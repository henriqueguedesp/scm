<?php

namespace SCM\routes;

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

$rotas = new RouteCollection();

$rotas->add('raiz', new Route('/', array('_controller' => 
    'SCM\controllers\controleIndex',
    '_method' => 'dashboard'))); 

$rotas->add('valorMedioKWH', new Route('/valorMedioKWH', array('_controller' => 
    'SCM\controllers\controleIndex',
    '_method' => 'valorMedioKWH'))); 

##LOGIN
$rotas->add('login', new Route('/login  ', array('_controller' => 
    'SCM\controllers\controleUsuario',
    '_method' => 'paginaLogin'))); 

$rotas->add('validaLogin', new Route('/validaLogin', array('_controller' => 
    'SCM\controllers\controleUsuario',
    '_method' => 'validaLogin'))); 

$rotas->add('removerUsuario', new Route('/removerUsuario', array('_controller' => 
    'SCM\controllers\controleUsuario',
    '_method' => 'removerUsuario'))); 


##CONTROLE PERFIL
$rotas->add('controlePerfil', new Route('/controlePerfil', array('_controller' => 
    'SCM\controllers\controlePerfil',
    '_method' => 'paginaPerfil'))); 

$rotas->add('controlePerfil', new Route('/controlePerfil', array('_controller' => 
    'SCM\controllers\controlePerfil',
    '_method' => 'paginaPerfil'))); 

$rotas->add('configuracoes', new Route('/configuracoes', array('_controller' => 
    'SCM\controllers\controleConfiguracoes',
    '_method' => 'configuracoes'))); 

$rotas->add('atualizarConfiguracoes', new Route('/atualizarConfiguracoes', array('_controller' => 
    'SCM\controllers\controleConfiguracoes',
    '_method' => 'atualizarConfiguracoes'))); 


#CONTROLE DE GERADORES

$rotas->add('controleGeradores', new Route('/controleGeradores', array('_controller' => 
    'SCM\controllers\controleGerador',
    '_method' => 'paginaControleGeradores'))); 

$rotas->add('cadastrarGerador', new Route('/cadastrarGerador', array('_controller' => 
    'SCM\controllers\controleGerador',
    '_method' => 'cadastrarGerador'))); 

$rotas->add('editarGerador', new Route('/editarGerador', array('_controller' => 
    'SCM\controllers\controleGerador',
    '_method' => 'editarGerador'))); 

$rotas->add('desativarGerador', new Route('/desativarGerador', array('_controller' => 
    'SCM\controllers\controleGerador',
    '_method' => 'desativarGerador'))); 

$rotas->add('ativarGerador', new Route('/ativarGerador', array('_controller' => 
    'SCM\controllers\controleGerador',
    '_method' => 'ativarGerador'))); 

#CONTROLE DE SECADORES

$rotas->add('controleSecadores', new Route('/controleSecadores', array('_controller' => 
    'SCM\controllers\controleSecador',
    '_method' => 'paginacontroleSecadores'))); 

$rotas->add('cadastrarSecador', new Route('/cadastrarSecador', array('_controller' => 
    'SCM\controllers\controleSecador',
    '_method' => 'cadastrarSecador'))); 

$rotas->add('editarSecador', new Route('/editarSecador', array('_controller' => 
    'SCM\controllers\controleSecador',
    '_method' => 'editarSecador'))); 

$rotas->add('desativarSecador', new Route('/desativarSecador', array('_controller' => 
    'SCM\controllers\controleSecador',
    '_method' => 'desativarSecador'))); 

$rotas->add('ativarSecador', new Route('/ativarSecador', array('_controller' => 
    'SCM\controllers\controleSecador',
    '_method' => 'ativarSecador'))); 

#APONTAMENTO


$rotas->add('apontamento', new Route('/apontamento', array('_controller' => 
    'SCM\controllers\controleApontamento',
    '_method' => 'paginaApontamento'))); 

# APONTAMENTO DIESEL
$rotas->add('apontamentoDiesel', new Route('/apontamentoDiesel', array('_controller' => 
    'SCM\controllers\controleApontamento',
    '_method' => 'paginaApontamentoDiesel'))); 

$rotas->add('apontamentoDieselJson', new Route('/apontamentoDieselJson', array('_controller' => 
    'SCM\controllers\controleApontamento',
    '_method' => 'apontamentoDieselJson'))); 

$rotas->add('cadastrarApontamentoDiesel', new Route('/cadastrarApontamentoDiesel', array('_controller' => 
    'SCM\controllers\controleApontamento',
    '_method' => 'cadastrarApontamentoDiesel'))); 

$rotas->add('editarApontamentoDiesel', new Route('/editarApontamentoDiesel', array('_controller' => 
    'SCM\controllers\controleApontamento',
    '_method' => 'editarApontamentoDiesel'))); 

$rotas->add('desativarApontamentoDiesel', new Route('/desativarApontamentoDiesel', array('_controller' => 
    'SCM\controllers\controleApontamento',
    '_method' => 'desativarApontamentoDiesel'))); 

##APONTAMENTO GERAÇÃO DE ENERGIA

$rotas->add('apontamentoGeracaoEnergia', new Route('/apontamentoGeracaoEnergia', array('_controller' => 
    'SCM\controllers\controleApontamento',
    '_method' => 'apontamentoGeracaoEnergia'))); 

$rotas->add('cadastrarApontamentoGeracaoEnergia', new Route('/cadastrarApontamentoGeracaoEnergia', array('_controller' => 
    'SCM\controllers\controleApontamento',
    '_method' => 'cadastrarApontamentoGeracaoEnergia'))); 

$rotas->add('apontamentoGeracaoEnergiaJson', new Route('/apontamentoGeracaoEnergiaJson', array('_controller' => 
    'SCM\controllers\controleApontamento',
    '_method' => 'apontamentoGeracaoEnergiaJson')));

$rotas->add('editarApontamentoGeracaoEnergia', new Route('/editarApontamentoGeracaoEnergia', array('_controller' => 
    'SCM\controllers\controleApontamento',
    '_method' => 'editarApontamentoGeracaoEnergia')));

$rotas->add('desativarApontamentoGeracaoEnergia', new Route('/desativarApontamentoGeracaoEnergia', array('_controller' => 
    'SCM\controllers\controleApontamento',
    '_method' => 'desativarApontamentoGeracaoEnergia')));

##APONTAMENTO CONSUMO GLP

$rotas->add('apontamentoConsumoGLP', new Route('/apontamentoConsumoGLP', array('_controller' => 
    'SCM\controllers\controleApontamento',
    '_method' => 'apontamentoConsumoGLP'))); 

$rotas->add('cadastrarApontamentoConsumoGLP', new Route('/cadastrarApontamentoConsumoGLP', array('_controller' => 
    'SCM\controllers\controleApontamento',
    '_method' => 'cadastrarApontamentoConsumoGLP'))); 

$rotas->add('apontamentoConsumoGLPJson', new Route('/apontamentoConsumoGLPJson', array('_controller' => 
    'SCM\controllers\controleApontamento',
    '_method' => 'apontamentoConsumoGLPJson'))); 

$rotas->add('editarApontamentoConsumoGLP', new Route('/editarApontamentoConsumoGLP', array('_controller' => 
    'SCM\controllers\controleApontamento',
    '_method' => 'editarApontamentoConsumoGLP'))); 

$rotas->add('desativarApontamentoConsumoGPL', new Route('/desativarApontamentoConsumoGPL', array('_controller' => 
    'SCM\controllers\controleApontamento',
    '_method' => 'desativarApontamentoConsumoGPL'))); 


##APONTAMENTO CONSUMO ENERGIA CEMIG

$rotas->add('apontamentoConsumoCemig', new Route('/apontamentoConsumoCemig', array('_controller' => 
    'SCM\controllers\controleApontamento',
    '_method' => 'apontamentoConsumoCemig'))); 

$rotas->add('cadastrarApontamentoConsumoCemig', new Route('/cadastrarApontamentoConsumoCemig', array('_controller' => 
    'SCM\controllers\controleApontamento',
    '_method' => 'cadastrarApontamentoConsumoCemig'))); 

$rotas->add('editarApontamentoConsumoCemig', new Route('/editarApontamentoConsumoCemig', array('_controller' => 
    'SCM\controllers\controleApontamento',
    '_method' => 'editarApontamentoConsumoCemig'))); 

$rotas->add('desativarApontamentoConsumoCemig', new Route('/desativarApontamentoConsumoCemig', array('_controller' => 
    'SCM\controllers\controleApontamento',
    '_method' => 'desativarApontamentoConsumoCemig'))); 

##ROTA PARA PESO INICIAL 
$rotas->add('atualizarConsumoInicialDiesel', new Route('/atualizarConsumoInicialDiesel', array('_controller' => 
    'SCM\controllers\controleApontamento',
    '_method' => 'atualizarConsumoInicialDiesel'))); 

$rotas->add('atualizarConsumoInicialEnergia', new Route('/atualizarConsumoInicialEnergia', array('_controller' => 
    'SCM\controllers\controleApontamento',
    '_method' => 'atualizarConsumoInicialEnergia'))); 

$rotas->add('atualizarConsumoInicialGLP', new Route('/atualizarConsumoInicialGLP', array('_controller' => 
    'SCM\controllers\controleApontamento',
    '_method' => 'atualizarConsumoInicialGLP'))); 

##RELATÓRIOS
$rotas->add('relatorios', new Route('/relatorios', array('_controller' => 
    'SCM\controllers\controleRelatorios',
    '_method' => 'paginaRelatorios'))); 

$rotas->add('glpConsumoPeriodoTotal', new Route('/glpConsumoPeriodoTotal', array('_controller' => 
    'SCM\controllers\controleRelatorios',
    '_method' => 'glpConsumoPeriodoTotal'))); 

$rotas->add('glpConsumoPeriodoTotalSecador', new Route('/glpConsumoPeriodoTotalSecador', array('_controller' => 
    'SCM\controllers\controleRelatorios',
    '_method' => 'glpConsumoPeriodoTotalSecador'))); 

$rotas->add('dielselConsumoPeriodoTotal', new Route('/dielselConsumoPeriodoTotal', array('_controller' => 
    'SCM\controllers\controleRelatorios',
    '_method' => 'dielselConsumoPeriodoTotal'))); 

$rotas->add('dieselConsumoPeriodoTotalGerador', new Route('/dieselConsumoPeriodoTotalGerador', array('_controller' => 
    'SCM\controllers\controleRelatorios',
    '_method' => 'dieselConsumoPeriodoTotalGerador'))); 

$rotas->add('energiaConsumoPeriodoTotal', new Route('/energiaConsumoPeriodoTotal', array('_controller' => 
    'SCM\controllers\controleRelatorios',
    '_method' => 'energiaConsumoPeriodoTotal'))); 

$rotas->add('energiaConsumoPeriodoTotalGerador', new Route('/energiaConsumoPeriodoTotalGerador', array('_controller' => 
    'SCM\controllers\controleRelatorios',
    '_method' => 'energiaConsumoPeriodoTotalGerador')));

$rotas->add('energiaConsumoPeriodoTotalGerador', new Route('/energiaConsumoPeriodoTotalGerador', array('_controller' => 
    'SCM\controllers\controleRelatorios',
    '_method' => 'energiaConsumoPeriodoTotalGerador')));

$rotas->add('energiaCemigPeriodoTotal', new Route('/energiaCemigPeriodoTotal', array('_controller' => 
    'SCM\controllers\controleRelatorios',
    '_method' => 'energiaCemigPeriodoTotal')));

$rotas->add('energiaCemigMaisGeradaPeriodoTotal', new Route('/energiaCemigMaisGeradaPeriodoTotal', array('_controller' => 
    'SCM\controllers\controleRelatorios',
    '_method' => 'energiaCemigMaisGeradaPeriodoTotal')));

$rotas->add('custoDespalhaPorPeriodo', new Route('/custoDespalhaPorPeriodo', array('_controller' => 
    'SCM\controllers\controleRelatorios',
    '_method' => 'custoDespalhaPorPeriodo')));

$rotas->add('testeSQL', new Route('/testeSQL', array('_controller' => 
    'SCM\controllers\controleRelatorios',
    '_method' => 'testeSQL'))); 

##LOCAIS CONSUMO DE ENERGIA

$rotas->add('locaisEnergia', new Route('/locaisEnergia', array('_controller' => 
    'SCM\controllers\controleLocalEnergia',
    '_method' => 'locaisEnergia'))); 

$rotas->add('casdastroLocal', new Route('/casdastroLocal', array('_controller' => 
    'SCM\controllers\controleLocalEnergia',
    '_method' => 'casdastroLocal'))); 

$rotas->add('editarLocal', new Route('/editarLocal', array('_controller' => 
    'SCM\controllers\controleLocalEnergia',
    '_method' => 'editarLocal'))); 

$rotas->add('desativarLocal', new Route('/desativarLocal', array('_controller' => 
    'SCM\controllers\controleLocalEnergia',
    '_method' => 'desativarLocal'))); 

$rotas->add('ativarLocal', new Route('/ativarLocal', array('_controller' => 
    'SCM\controllers\controleLocalEnergia',
    '_method' => 'ativarLocal'))); 

##APONTAMENTO CONSUMO ENERGIA LOCAIS

$rotas->add('apontamentoConsumoLocais', new Route('/apontamentoConsumoLocais', array('_controller' => 
    'SCM\controllers\controleConsumoLocal',
    '_method' => 'apontamentoConsumoLocais'))); 

$rotas->add('cadastrarApontamentoConsumoLocal', new Route('/cadastrarApontamentoConsumoLocal', array('_controller' => 
    'SCM\controllers\controleConsumoLocal',
    '_method' => 'cadastrarApontamentoConsumoLocal'))); 

$rotas->add('apontamentoConsumoEnergia', new Route('/apontamentoConsumoEnergia/{_param}', array('_controller' => 
    'SCM\controllers\controleConsumoLocal',
    '_method' => 'apontamentoConsumoEnergia'))); 

$rotas->add('desativarApontamentoConsumoEnergia', new Route('/desativarApontamentoConsumoEnergia', array('_controller' => 
    'SCM\controllers\controleConsumoLocal',
    '_method' => 'desativarApontamentoConsumoEnergia'))); 

#RELATÓRIOS DASHBOARD
$rotas->add('custoTotalLinha', new Route('/custoTotalLinha/{_param}', array('_controller' => 
    'SCM\util\relatorioDashboard',
    '_method' => 'custoTotalLinha'))); 

$rotas->add('custoTotalLinhas', new Route('/custoTotalLinhas', array('_controller' => 
    'SCM\util\relatorioDashboard',
    '_method' => 'custoTotalLinhas'))); 

$rotas->add('custoSilosArmazenagemPorMaterial', new Route('/custoSilosArmazenagemPorMaterial', array('_controller' => 
    'SCM\util\relatorioDashboard',
    '_method' => 'custoSilosArmazenagemPorMaterial'))); 

$rotas->add('custoSilosArmazenagem', new Route('/custoSilosArmazenagem', array('_controller' => 
    'SCM\util\relatorioDashboard',
    '_method' => 'custoSilosArmazenagem'))); 

$rotas->add('custoDescartePorMaterial', new Route('/custoDescartePorMaterial', array('_controller' => 
    'SCM\util\relatorioDashboard',
    '_method' => 'custoDescartePorMaterial'))); 

$rotas->add('custoDescarte', new Route('/custoDescarte', array('_controller' => 
    'SCM\util\relatorioDashboard',
    '_method' => 'custoDescarte'))); 

$rotas->add('custoEmpilhadeiras', new Route('/custoEmpilhadeiras', array('_controller' => 
    'SCM\util\relatorioDashboard',
    '_method' => 'custoEmpilhadeiras'))); 

$rotas->add('custoDespalha', new Route('/custoDespalha', array('_controller' => 
    'SCM\util\relatorioDashboard',
    '_method' => 'custoDespalha'))); 

$rotas->add('custoSistemaAR', new Route('/custoSistemaAR', array('_controller' => 
    'SCM\util\relatorioDashboard',
    '_method' => 'custoSistemaAR'))); 

$rotas->add('custoArmazens', new Route('/custoArmazens', array('_controller' => 
    'SCM\util\relatorioDashboard',
    '_method' => 'custoArmazens'))); 

$rotas->add('custoTotal', new Route('/custoTotal', array('_controller' => 
    'SCM\util\relatorioDashboard',
    '_method' => 'custoTotal'))); 

$rotas->add('custoEnergiaLinha01', new Route('/custoEnergiaLinha01', array('_controller' => 
    'SCM\util\relatorioDashboard',
    '_method' => 'custoEnergiaLinha01'))); 

$rotas->add('custoEnergiaLinha02', new Route('/custoEnergiaLinha02', array('_controller' => 
    'SCM\util\relatorioDashboard',
    '_method' => 'custoEnergiaLinha02'))); 

$rotas->add('custoEnergiaLinha03', new Route('/custoEnergiaLinha03', array('_controller' => 
    'SCM\util\relatorioDashboard',
    '_method' => 'custoEnergiaLinha03'))); 

$rotas->add('custoEnergiaLinha04', new Route('/custoEnergiaLinha04', array('_controller' => 
    'SCM\util\relatorioDashboard',
    '_method' => 'custoEnergiaLinha04'))); 

$rotas->add('custoEnergiaArmazen01', new Route('/custoEnergiaArmazen01', array('_controller' => 
    'SCM\util\relatorioDashboard',
    '_method' => 'custoEnergiaArmazen01'))); 

$rotas->add('custoEnergiaArmazen02', new Route('/custoEnergiaArmazen02', array('_controller' => 
    'SCM\util\relatorioDashboard',
    '_method' => 'custoEnergiaArmazen02'))); 

$rotas->add('custoEnergiaArmazen03', new Route('/custoEnergiaArmazen03', array('_controller' => 
    'SCM\util\relatorioDashboard',
    '_method' => 'custoEnergiaArmazen03'))); 

$rotas->add('custoEnergiaArmazen04', new Route('/custoEnergiaArmazen04', array('_controller' => 
    'SCM\util\relatorioDashboard',
    '_method' => 'custoEnergiaArmazen04'))); 

$rotas->add('custoEnergiaArmazen05', new Route('/custoEnergiaArmazen05', array('_controller' => 
    'SCM\util\relatorioDashboard',
    '_method' => 'custoEnergiaArmazen05'))); 

$rotas->add('custoEnergiaTotalArmazens', new Route('/custoEnergiaTotalArmazens', array('_controller' => 
    'SCM\util\relatorioDashboard',
    '_method' => 'custoEnergiaTotalArmazens'))); 

$rotas->add('custoEnergiaTotal', new Route('/custoEnergiaTotal', array('_controller' => 
    'SCM\util\relatorioDashboard',
    '_method' => 'custoEnergiaTotal'))); 

$rotas->add('custoSecador', new Route('/custoSecador/{_param}', array('_controller' => 
    'SCM\util\relatorioDashboard',
    '_method' => 'custoSecador'))); 

$rotas->add('custoTotalSecador', new Route('/custoTotalSecador', array('_controller' => 
    'SCM\util\relatorioDashboard',
    '_method' => 'custoTotalSecador'))); 

$rotas->add('custoTorreAbsoluto', new Route('/custoTorreAbsoluto', array('_controller' => 
    'SCM\util\relatorioDashboard',
    '_method' => 'custoTorreAbsoluto'))); 

$rotas->add('custoTSIAbsoluto', new Route('/custoTSIAbsoluto', array('_controller' => 
    'SCM\util\relatorioDashboard',
    '_method' => 'custoTSIAbsoluto'))); 

$rotas->add('custoTSI', new Route('/custoTSI', array('_controller' => 
    'SCM\util\relatorioDashboard',
    '_method' => 'custoTSI'))); 

$rotas->add('custoTorre', new Route('/custoTorre', array('_controller' => 
    'SCM\util\relatorioDashboard',
    '_method' => 'custoTorre'))); 

$rotas->add('custoEnergiaTorre', new Route('/custoEnergiaTorre', array('_controller' => 
    'SCM\util\relatorioDashboard',
    '_method' => 'custoEnergiaTorre'))); 

$rotas->add('custoEnergiaTSI', new Route('/custoEnergiaTSI', array('_controller' => 
    'SCM\util\relatorioDashboard',
    '_method' => 'custoEnergiaTSI'))); 

##APONTAMENTO PRODUCAO
$rotas->add('apontamentoProducao', new Route('/apontamentoProducao', array('_controller' => 
    'SCM\controllers\controleApontamento',
    '_method' => 'apontamentoProducao'))); 

$rotas->add('cadastrarApontamentoProducao', new Route('/cadastrarApontamentoProducao', array('_controller' => 
    'SCM\controllers\controleApontamento',
    '_method' => 'cadastrarApontamentoProducao'))); 

$rotas->add('apontamentoProducaoJson', new Route('/apontamentoProducaoJson', array('_controller' => 
    'SCM\controllers\controleApontamento',
    '_method' => 'apontamentoProducaoJson'))); 

$rotas->add('desativarApontamentoProducao', new Route('/desativarApontamentoProducao', array('_controller' => 
    'SCM\controllers\controleApontamento',
    '_method' => 'desativarApontamentoProducao'))); 

return $rotas;
