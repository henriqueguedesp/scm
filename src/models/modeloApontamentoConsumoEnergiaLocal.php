<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace SCM\models;

/**
 * Description of modeloApontamentoConsumoEnergiaLocal
 *
 * @author henrique.guedes
 */
use PDO;
use SCM\util\conexao;
use SCM\entity\consumoLocal;

class modeloApontamentoConsumoEnergiaLocal {

    public function cadastrar(consumoLocal $consumoLocal) {
        try {
            $conexao = conexao::getInstance();
            $sql = 'INSERT INTO SCMConsumoEnergia (idSCMLocalEnergia, idCriador, dataCriacao, dataInicio, dataTermino, 
                    consumoTotal, consumoMedio, status,totalDias)
                    values
                    (:idSCMLocalEnergia, :idCriador, now(),:dataInicio, :dataTermino, 
                    :consumoTotal,:consumoMedio, 1,:totalDias)';
            $conexao->beginTransaction();
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':idSCMLocalEnergia', $consumoLocal->getIdSCMLocalEnergia());
            $p_sql->bindValue(':idCriador', $consumoLocal->getIdCriador());
            $p_sql->bindValue(':dataInicio', $consumoLocal->getDataInicio());
            $p_sql->bindValue(':dataTermino', $consumoLocal->getDataTermino());
            $p_sql->bindValue(':consumoTotal', $consumoLocal->getConsumoTotal());
            $p_sql->bindValue(':consumoMedio', $consumoLocal->getConsumoMedio());
            $p_sql->bindValue(':totalDias', $consumoLocal->getTotalDias());
            $p_sql->execute();
            $id = $conexao->lastInsertId();
            for ($i = 0; $i <= $consumoLocal->getTotalDias(); $i++) {

                $sql = 'INSERT INTO SCMApontamentoConsumoEnergia
              (idSCMConsumoEnergia, idLocalConsumo, dia, mediaConsumo, status)
              values
              (:idSCMConsumoEnergia, :idLocalConsumo, :dia, :mediaConsumo, 1)';
                $p_sql = Conexao::getInstance()->prepare($sql);
                $p_sql->bindValue(':idSCMConsumoEnergia', $id);
                $p_sql->bindValue(':idLocalConsumo', $consumoLocal->getIdSCMLocalEnergia());
                $p_sql->bindValue(':dia', $consumoLocal->getDataInicio());
                $p_sql->bindValue(':mediaConsumo', $consumoLocal->getConsumoMedio());
                $p_sql->execute();
                $consumoLocal->setDataInicio(date('Y-m-d', strtotime("+1 days", strtotime($consumoLocal->getDataInicio()))));
            }
            $conexao->commit();
            return 1;
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function todosApontamentos($local) {
        try {
            /*
              $sql = 'SELECT
              g.local AS localizacao, CONCAT("Gerador: ",g.descricao)as title, CONCAT(DATE_FORMAT(c.dataInicial, "%Y-%m-%d")," 00:00:00") AS start, CONCAT(DATE_FORMAT(c.dataFinal, "%Y-%m-%d")," 23:59:59") AS end, c.valorConsumoInicial , c.valorConsumoFinal, c.dataLeitura, (c.valorConsumoFinal - c.valorConsumoInicial ) AS consumoTotal, c.idSCMGConsumo AS idApontamento, g.idSCMGerador as idGerador
              FROM SCMGerador  AS g
              LEFT JOIN SCMGConsumo AS c ON c.idSCMGerador = g.idSCMGerador
              WHERE c.status  = 1;;'; */
            $sql = 'SELECT 
                    C.idSCMConsumoEnergia as idApontamento,C.idSCMLocalEnergia as idLocal, C.dataInicio, C.dataTermino, 
                    C.consumoTotal,
                    CONCAT(DATE_FORMAT(CA.dia, "%Y-%m-%d")," 00:00:00") AS start, 
                    CONCAT(DATE_FORMAT(CA.dia, "%Y-%m-%d")," 23:59:59") AS end,
                    CA.mediaConsumo,
                    (SELECT local FROM SCMLocalEnergia WHERE idSCMLocalEnergia = C.idSCMLocalEnergia) AS localizacao,
                    (SELECT descricao FROM SCMLocalEnergia WHERE idSCMLocalEnergia = C.idSCMLocalEnergia) AS title
                    FROM SCMConsumoEnergia AS C
                    LEFT JOIN SCMApontamentoConsumoEnergia as CA on  C.idSCMConsumoEnergia = CA.idSCMConsumoEnergia
                    WHERE C.status = 1 AND CA.status = 1 
                    AND C.idSCMLocalEnergia = :idLocal;';
            $p_sql = conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':idLocal', $local);

            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_ASSOC);
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function consumoTotalEnergiaLocal($local, $dataInicio, $dataTermino) {
        try {

            $sql = 'SELECT 
                    sum(mediaConsumo) as  consumo
                    FROM SCMApontamentoConsumoEnergia 
                    WHERE dia between :dataInicio and :dataTermino and status = 1 
                    AND idLocalConsumo = :idLocal;';
            $p_sql = conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':idLocal', $local);
            $p_sql->bindValue(':dataInicio', $dataInicio);
            $p_sql->bindValue(':dataTermino', $dataTermino);

            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            return $ex;
        }
    }
  
    public function consumoEnergiaLocal($local, $dataInicio, $dataTermino) {
        try {

            $sql = 'SELECT 
                    mediaConsumo as  consumo
                    FROM SCMApontamentoConsumoEnergia 
                    WHERE dia between :dataInicio and :dataTermino and status = 1 
                    AND idLocalConsumo = :idLocal;';
            $p_sql = conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':idLocal', $local);
            $p_sql->bindValue(':dataInicio', $dataInicio);
            $p_sql->bindValue(':dataTermino', $dataTermino);

            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function desativar($idApontamento) {
        try {
            $conexao = Conexao::getInstance();

            $sql = 'UPDATE SCMConsumoEnergia 
                    SET status  = 0
                    WHERE idSCMConsumoEnergia = :id';
            $conexao->beginTransaction();
            $p_sql = conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $idApontamento);

            $p_sql->execute();

            $sql = 'UPDATE SCMApontamentoConsumoEnergia
                    SET status = 0
                    WHERE idSCMConsumoEnergia = :id';
            $p_sql = conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $idApontamento);
            $p_sql->execute();

            $conexao->commit();

            return 1;
        } catch (Exception $ex) {
            return $ex;
        }
    }

}
