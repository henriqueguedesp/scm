<?php

namespace SCM\models;

use PDO;
use SCM\util\conexao;
use SCM\entity\secador;

class modeloSecador {

    public function cadastrar(secador $secador) {
        try {
            $conexao = Conexao::getInstance();
            $sql = 'INSERT INTO SCMSecador (local, descricao,dataCriacao,status)
                    values
                    (:local, :descricao, now(),1)';
            $conexao->beginTransaction();
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':local', $secador->getLocal());
            $p_sql->bindValue(':descricao', $secador->getDescricao());
            $p_sql->execute();
            $conexao->commit();
            return 1;
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function todosSecadores() {
        try {
            $sql = 'SELECT * FROM SCMSecador';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function secadorEspecifico($id) {
        try {
            $sql = 'SELECT * FROM SCMSecador WHERE idSCMSecador = :idSCMSecador';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':idSCMSecador', $id);

            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function editar(secador $secador) {
        try {
            $sql = 'UPDATE SCMSecador  SET local = :local, descricao = :descricao WHERE idSCMSecador = :id';

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':descricao', $secador->getDescricao());
            $p_sql->bindValue(':local', $secador->getLocal());
            $p_sql->bindValue(':id', $secador->getIdSCMSecador());
            $p_sql->execute();
            return 1;
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function desativarSecador(secador $secador) {
        try {
            $sql = 'UPDATE SCMSecador  SET status = 0 WHERE idSCMSecador = :id';

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $secador->getIdSCMSecador());
            $p_sql->execute();
            return 1;
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function ativarSecador(secador $secador) {
        try {
            $sql = 'UPDATE SCMSecador  SET status = 1 WHERE idSCMSecador = :id';

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $secador->getIdSCMSecador());
            $p_sql->execute();
            return 1;
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function todosSecadoresAtivos() {
        try {
            $sql = 'SELECT * FROM SCMSecador WHERE status = 1';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            return $ex;
        }
    }

}
