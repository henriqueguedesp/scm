<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace SCM\models;

use PDO;
use SCM\util\conexaoSQL;

/**
 * Description of modeloProtheus
 *
 * @author henrique.guedes
 */
class modeloProtheus {

    public function valorTotalGastoPorCC($dataInicial, $dataFinal, $ccInicial, $ccFinal) {
        try {
            $sql = "SELECT 
                    SUM ((D1_TOTAL + D1_VALFRE + D1_VALIPI + D1_ICMSCOM)  - D1_VALDESC) AS TOTAL
                    FROM SD1010 D1
                    LEFT JOIN SB1010 B1 ON B1_FILIAL = SUBSTRING(D1_FILIAL,1,2) AND B1_COD = D1_COD
                    AND B1.D_E_L_E_T_ = ' '
                    LEFT JOIN SA2010 A2 ON A2_FILIAL = SUBSTRING(D1_FILIAL,1,2) AND A2_COD = D1_FORNECE AND A2_LOJA = D1_LOJA AND A2.D_E_L_E_T_ = ' '
                    LEFT JOIN CTT010 CT ON CTT_FILIAL = SUBSTRING(D1_FILIAL,1,2) AND CTT_CUSTO = D1_CC AND CT.D_E_L_E_T_ = ' '
                    LEFT JOIN SC7010 C7 ON C7_FILIAL = D1_FILIAL AND  D1_PEDIDO = C7_NUM AND C7.D_E_L_E_T_ = ' ' 
		    AND D1_FORNECE = C7_FORNECE AND D1_LOJA = C7_LOJA AND D1_COD = C7_PRODUTO AND D1_ITEMPC = C7_ITEM 
                    LEFT JOIN SF4010 F4 ON F4_FILIAL = SUBSTRING(D1_FILIAL,1,2) AND F4_CODIGO = D1_TES AND F4.D_E_L_E_T_ = ' ' 
                    LEFT JOIN SBM010 BM ON BM_FILIAL = D1_FILIAL AND BM_GRUPO = B1_GRUPO /*AND BM_MSBLQL = '2' AND BM.D_E_L_E_T_ = ' '*/
                    --LEFT JOIN SE2010 E2 ON E2_FILIAL = D1_FILIAL AND E2_NUM = D1_DOC AND E2_FORNECE = D1_FORNECE AND E2_LOJA = D1_LOJA AND E2.D_E_L_E_T_ = ' '
                    INNER JOIN SF1010 F1 ON F1_FILIAL = D1_FILIAL AND F1_DOC = D1_DOC AND F1_FORNECE = D1_FORNECE AND F1_LOJA = D1_LOJA AND F1.D_E_L_E_T_= ' '
                    LEFT JOIN SC1010 C1 ON C1_FILIAL = C7_FILIAL AND C7_NUMSC = C1_NUM AND C7_ITEMSC = C1_ITEM AND C1.D_E_L_E_T_ = ' '
                    WHERE D1.D_E_L_E_T_ = ' ' 
                    AND D1.D1_FILIAL = '0302' 
                    AND B1_GRUPO NOT IN ('DSC-','MPB-','PAB-','PAS-','PDS-','0041') 
                    AND D1_DTDIGIT BETWEEN :dataInicial AND :dataFinal 
                    AND D1_CC BETWEEN :ccInicial AND :ccFinal
                    AND C7.C7_CC is not null
                    AND (C1_SOLICIT != 'SUZANA.RUELA')
                    AND (C1_SOLICIT != 'RAIANE.SOUZA')
                    AND D1_COD  != '00100142'

                    ORDER BY 1";
            $p_sql = conexaoSQL::getInstance()->prepare($sql);
            $p_sql->bindValue(':dataInicial', $dataInicial);
            $p_sql->bindValue(':dataFinal', $dataFinal);
            $p_sql->bindValue(':ccInicial', $ccInicial);
            $p_sql->bindValue(':ccFinal', $ccFinal);
            $p_sql->execute();

            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function itensGastoPorCC($dataInicial, $dataFinal, $ccInicial, $ccFinal) {
        try {
            $sql = "SELECT 
                       B1_DESC AS NOME_PRODUTO,
					((D1_TOTAL + D1_VALFRE + D1_VALIPI + D1_ICMSCOM)  - D1_VALDESC) AS VALORTOTAL,
                    D1_QUANT AS QUANTIDADE

                    FROM SD1010 D1
                    LEFT JOIN SB1010 B1 ON B1_FILIAL = SUBSTRING(D1_FILIAL,1,2) AND B1_COD = D1_COD
                    AND B1.D_E_L_E_T_ = ' '
                    LEFT JOIN SA2010 A2 ON A2_FILIAL = SUBSTRING(D1_FILIAL,1,2) AND A2_COD = D1_FORNECE AND A2_LOJA = D1_LOJA AND A2.D_E_L_E_T_ = ' '
                    LEFT JOIN CTT010 CT ON CTT_FILIAL = SUBSTRING(D1_FILIAL,1,2) AND CTT_CUSTO = D1_CC AND CT.D_E_L_E_T_ = ' '
                    LEFT JOIN SC7010 C7 ON C7_FILIAL = D1_FILIAL AND  D1_PEDIDO = C7_NUM AND C7.D_E_L_E_T_ = ' ' 
		    AND D1_FORNECE = C7_FORNECE AND D1_LOJA = C7_LOJA AND D1_COD = C7_PRODUTO AND D1_ITEMPC = C7_ITEM 
                    LEFT JOIN SF4010 F4 ON F4_FILIAL = SUBSTRING(D1_FILIAL,1,2) AND F4_CODIGO = D1_TES AND F4.D_E_L_E_T_ = ' ' 
                    LEFT JOIN SBM010 BM ON BM_FILIAL = D1_FILIAL AND BM_GRUPO = B1_GRUPO /*AND BM_MSBLQL = '2' AND BM.D_E_L_E_T_ = ' '*/
                    --LEFT JOIN SE2010 E2 ON E2_FILIAL = D1_FILIAL AND E2_NUM = D1_DOC AND E2_FORNECE = D1_FORNECE AND E2_LOJA = D1_LOJA AND E2.D_E_L_E_T_ = ' '
                    INNER JOIN SF1010 F1 ON F1_FILIAL = D1_FILIAL AND F1_DOC = D1_DOC AND F1_FORNECE = D1_FORNECE AND F1_LOJA = D1_LOJA AND F1.D_E_L_E_T_= ' '
                    LEFT JOIN SC1010 C1 ON C1_FILIAL = C7_FILIAL AND C7_NUMSC = C1_NUM AND C7_ITEMSC = C1_ITEM AND C1.D_E_L_E_T_ = ' '
                    WHERE D1.D_E_L_E_T_ = ' ' 
                    AND D1.D1_FILIAL = '0302' 
                    AND B1_GRUPO NOT IN ('DSC-','MPB-','PAB-','PAS-','PDS-','0041') 
                    AND D1_DTDIGIT BETWEEN :dataInicial AND :dataFinal 
                    AND D1_CC BETWEEN :ccInicial AND :ccFinal
                    AND C7.C7_CC is not null
                    AND (C1_SOLICIT != 'SUZANA.RUELA')
                    AND (C1_SOLICIT != 'RAIANE.SOUZA')
                    AND D1_COD  != '00100142'
                    ORDER BY 1";
            $p_sql = conexaoSQL::getInstance()->prepare($sql);
            $p_sql->bindValue(':dataInicial', $dataInicial);
            $p_sql->bindValue(':dataFinal', $dataFinal);
            $p_sql->bindValue(':ccInicial', $ccInicial);
            $p_sql->bindValue(':ccFinal', $ccFinal);
            $p_sql->execute();

            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function totalMatRecPorLinha($dataInicial, $dataFinal, $linha) {
        try {
            $sql = "SELECT SUM(ZK_PESOLIQ)/1000 AS MATERIAL
                    FROM SZK010 
                    WHERE 
                    ZK_TICKIND = '1'  AND ZK_PLANREC = " . $linha . " AND ZK_DATAINI BETWEEN :dataInicial AND :dataFinal AND 
                    D_E_L_E_T_ = ' ';";

            $p_sql = conexaoSQL::getInstance()->prepare($sql);
            //$p_sql->bindValue(':lin', $linha);
            $p_sql->bindValue(':dataInicial', $dataInicial);
            $p_sql->bindValue(':dataFinal', $dataFinal);
            $p_sql->execute();

            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function totalMatRecTotal($dataInicial, $dataFinal) {
        try {
            $sql = "SELECT SUM(ZK_PESOLIQ)/1000 AS MATERIAL
                    FROM SZK010 
                    WHERE 
                    ZK_TICKIND = '1'  AND  ZK_DATAINI BETWEEN :dataInicial AND :dataFinal AND 
                    D_E_L_E_T_ = ' ';";

            $p_sql = conexaoSQL::getInstance()->prepare($sql);
            $p_sql->bindValue(':dataInicial', $dataInicial);
            $p_sql->bindValue(':dataFinal', $dataFinal);
            $p_sql->execute();

            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function totalGastoPorProduto($dataInicial, $dataFinal, $codigoProduto, $ccInicial, $ccFinal) {
        try {
            $sql = "SELECT
                    SUM((D1_TOTAL + D1_VALFRE + D1_VALIPI + D1_ICMSCOM)  - D1_VALDESC) AS VALORTOTAL,
                    SUM(D1_QUANT) AS QUANTIDADE
                    FROM SD1010 D1
                    LEFT JOIN SB1010 B1 ON B1_FILIAL = SUBSTRING(D1_FILIAL,1,2) AND B1_COD = D1_COD
                    AND B1.D_E_L_E_T_ = ' '
                    LEFT JOIN SA2010 A2 ON A2_FILIAL = SUBSTRING(D1_FILIAL,1,2) AND A2_COD = D1_FORNECE AND A2_LOJA = D1_LOJA AND A2.D_E_L_E_T_ = ' '
                    LEFT JOIN CTT010 CT ON CTT_FILIAL = SUBSTRING(D1_FILIAL,1,2) AND CTT_CUSTO = D1_CC AND CT.D_E_L_E_T_ = ' '
                    LEFT JOIN SC7010 C7 ON C7_FILIAL = D1_FILIAL AND  D1_PEDIDO = C7_NUM AND C7.D_E_L_E_T_ = ' ' 
                    AND D1_FORNECE = C7_FORNECE AND D1_LOJA = C7_LOJA AND D1_COD = C7_PRODUTO AND D1_ITEMPC = C7_ITEM 
                    LEFT JOIN SF4010 F4 ON F4_FILIAL = SUBSTRING(D1_FILIAL,1,2) AND F4_CODIGO = D1_TES AND F4.D_E_L_E_T_ = ' ' 
                    LEFT JOIN SBM010 BM ON BM_FILIAL = D1_FILIAL AND BM_GRUPO = B1_GRUPO /*AND BM_MSBLQL = '2' AND BM.D_E_L_E_T_ = ' '*/
                    --LEFT JOIN SE2010 E2 ON E2_FILIAL = D1_FILIAL AND E2_NUM = D1_DOC AND E2_FORNECE = D1_FORNECE AND E2_LOJA = D1_LOJA AND E2.D_E_L_E_T_ = ' '
                    INNER JOIN SF1010 F1 ON F1_FILIAL = D1_FILIAL AND F1_DOC = D1_DOC AND F1_FORNECE = D1_FORNECE AND F1_LOJA = D1_LOJA AND F1.D_E_L_E_T_= ' '
                    LEFT JOIN SC1010 C1 ON C1_FILIAL = C7_FILIAL AND C7_NUMSC = C1_NUM AND C7_ITEMSC = C1_ITEM AND C1.D_E_L_E_T_ = ' '
                    WHERE D1.D_E_L_E_T_ = ' '
                    AND D1.D1_FILIAL = '0302'
                    AND B1_GRUPO NOT IN ('DSC-','MPB-','PAB-','PAS-','PDS-','0041')
                    AND D1_DTDIGIT BETWEEN :dataInicial AND :dataFinal
                    AND D1_CC BETWEEN :ccInicial AND :ccFinal
                    AND D1_COD = :produto
                    AND C7.C7_CC is not null
                    AND (C1_SOLICIT != 'SUZANA.RUELA')
                    AND (C1_SOLICIT != 'RAIANE.SOUZA')
                    ORDER BY 1 ";

            $p_sql = conexaoSQL::getInstance()->prepare($sql);
            $p_sql->bindValue(':dataInicial', $dataInicial);
            $p_sql->bindValue(':dataFinal', $dataFinal);
            $p_sql->bindValue(':ccInicial', $ccInicial);
            $p_sql->bindValue(':ccFinal', $ccFinal);
            $p_sql->bindValue(':produto', $codigoProduto);
            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function totalGastoPorProdutoDashboard($dataInicial, $dataFinal, $codigoProduto, $ccInicial, $ccFinal) {
        try {
            $sql = "SELECT
                   B1_DESC AS NOME_PRODUTO,
					((D1_TOTAL + D1_VALFRE + D1_VALIPI + D1_ICMSCOM)  - D1_VALDESC) AS VALORTOTAL,
                    D1_QUANT AS QUANTIDADE
                    FROM SD1010 D1
                    LEFT JOIN SB1010 B1 ON B1_FILIAL = SUBSTRING(D1_FILIAL,1,2) AND B1_COD = D1_COD
                    AND B1.D_E_L_E_T_ = ' '
                    LEFT JOIN SA2010 A2 ON A2_FILIAL = SUBSTRING(D1_FILIAL,1,2) AND A2_COD = D1_FORNECE AND A2_LOJA = D1_LOJA AND A2.D_E_L_E_T_ = ' '
                    LEFT JOIN CTT010 CT ON CTT_FILIAL = SUBSTRING(D1_FILIAL,1,2) AND CTT_CUSTO = D1_CC AND CT.D_E_L_E_T_ = ' '
                    LEFT JOIN SC7010 C7 ON C7_FILIAL = D1_FILIAL AND  D1_PEDIDO = C7_NUM AND C7.D_E_L_E_T_ = ' ' 
                    AND D1_FORNECE = C7_FORNECE AND D1_LOJA = C7_LOJA AND D1_COD = C7_PRODUTO AND D1_ITEMPC = C7_ITEM 
                    LEFT JOIN SF4010 F4 ON F4_FILIAL = SUBSTRING(D1_FILIAL,1,2) AND F4_CODIGO = D1_TES AND F4.D_E_L_E_T_ = ' ' 
                    LEFT JOIN SBM010 BM ON BM_FILIAL = D1_FILIAL AND BM_GRUPO = B1_GRUPO /*AND BM_MSBLQL = '2' AND BM.D_E_L_E_T_ = ' '*/
                    --LEFT JOIN SE2010 E2 ON E2_FILIAL = D1_FILIAL AND E2_NUM = D1_DOC AND E2_FORNECE = D1_FORNECE AND E2_LOJA = D1_LOJA AND E2.D_E_L_E_T_ = ' '
                    INNER JOIN SF1010 F1 ON F1_FILIAL = D1_FILIAL AND F1_DOC = D1_DOC AND F1_FORNECE = D1_FORNECE AND F1_LOJA = D1_LOJA AND F1.D_E_L_E_T_= ' '
                    LEFT JOIN SC1010 C1 ON C1_FILIAL = C7_FILIAL AND C7_NUMSC = C1_NUM AND C7_ITEMSC = C1_ITEM AND C1.D_E_L_E_T_ = ' '
                    WHERE D1.D_E_L_E_T_ = ' '
                    AND D1.D1_FILIAL = '0302'
                    AND B1_GRUPO NOT IN ('DSC-','MPB-','PAB-','PAS-','PDS-','0041')
                    AND D1_DTDIGIT BETWEEN :dataInicial AND :dataFinal
                    AND D1_CC BETWEEN :ccInicial AND :ccFinal
                    AND D1_COD = :produto
                    AND C7.C7_CC is not null
                    AND (C1_SOLICIT != 'SUZANA.RUELA')
                    AND (C1_SOLICIT != 'RAIANE.SOUZA')
                    ORDER BY 1 ";

            $p_sql = conexaoSQL::getInstance()->prepare($sql);
            $p_sql->bindValue(':dataInicial', $dataInicial);
            $p_sql->bindValue(':dataFinal', $dataFinal);
            $p_sql->bindValue(':ccInicial', $ccInicial);
            $p_sql->bindValue(':ccFinal', $ccFinal);
            $p_sql->bindValue(':produto', $codigoProduto);
            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function totalGastoPorProdutoHistorico($codigoProduto, $ccInicial, $ccFinal) {
        try {
            $sql = "SELECT TOP 1
                    D1_QUANT AS QUANTIDADE,
                    ((D1_TOTAL + D1_VALFRE + D1_VALIPI + D1_ICMSCOM)  - D1_VALDESC) AS VALORTOTAL
                    FROM SD1010 D1
                    LEFT JOIN SB1010 B1 ON B1_FILIAL = SUBSTRING(D1_FILIAL,1,2) AND B1_COD = D1_COD
                    AND B1.D_E_L_E_T_ = ' '
                    LEFT JOIN SA2010 A2 ON A2_FILIAL = SUBSTRING(D1_FILIAL,1,2) AND A2_COD = D1_FORNECE AND A2_LOJA = D1_LOJA AND A2.D_E_L_E_T_ = ' '
                    LEFT JOIN CTT010 CT ON CTT_FILIAL = SUBSTRING(D1_FILIAL,1,2) AND CTT_CUSTO = D1_CC AND CT.D_E_L_E_T_ = ' '
                    LEFT JOIN SC7010 C7 ON C7_FILIAL = D1_FILIAL AND  D1_PEDIDO = C7_NUM AND C7.D_E_L_E_T_ = ' ' 
                    AND D1_FORNECE = C7_FORNECE AND D1_LOJA = C7_LOJA AND D1_COD = C7_PRODUTO AND D1_ITEMPC = C7_ITEM 
                    LEFT JOIN SF4010 F4 ON F4_FILIAL = SUBSTRING(D1_FILIAL,1,2) AND F4_CODIGO = D1_TES AND F4.D_E_L_E_T_ = ' ' 
                    LEFT JOIN SBM010 BM ON BM_FILIAL = D1_FILIAL AND BM_GRUPO = B1_GRUPO /*AND BM_MSBLQL = '2' AND BM.D_E_L_E_T_ = ' '*/
                    --LEFT JOIN SE2010 E2 ON E2_FILIAL = D1_FILIAL AND E2_NUM = D1_DOC AND E2_FORNECE = D1_FORNECE AND E2_LOJA = D1_LOJA AND E2.D_E_L_E_T_ = ' '
                    INNER JOIN SF1010 F1 ON F1_FILIAL = D1_FILIAL AND F1_DOC = D1_DOC AND F1_FORNECE = D1_FORNECE AND F1_LOJA = D1_LOJA AND F1.D_E_L_E_T_= ' '
                    LEFT JOIN SC1010 C1 ON C1_FILIAL = C7_FILIAL AND C7_NUMSC = C1_NUM AND C7_ITEMSC = C1_ITEM AND C1.D_E_L_E_T_ = ' '
                    WHERE D1.D_E_L_E_T_ = ' '
                    AND D1.D1_FILIAL = '0302'
                    AND B1_GRUPO NOT IN ('DSC-','MPB-','PAB-','PAS-','PDS-','0041')
                    AND D1_CC BETWEEN :ccInicial AND :ccFinal
                    AND D1_COD = :produto
                    AND C7.C7_CC is not null
                    AND (C1_SOLICIT != 'SUZANA.RUELA')
                    AND (C1_SOLICIT != 'RAIANE.SOUZA')
                    ORDER BY  D1_DTDIGIT DESC  ";

            $p_sql = conexaoSQL::getInstance()->prepare($sql);
            $p_sql->bindValue(':ccInicial', $ccInicial);
            $p_sql->bindValue(':ccFinal', $ccFinal);
            $p_sql->bindValue(':produto', $codigoProduto);
            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            return $ex;
        }
    }

}
