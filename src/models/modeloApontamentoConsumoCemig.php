<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace SCM\models;

use PDO;
use SCM\util\conexao;
use SCM\entity\consumoCemig;

/**
 * Description of modeloApontamentoConsumoCemig
 *
 * @author henrique.guedes
 */
class modeloApontamentoConsumoCemig {

    public function cadastrar(consumoCemig $consumo) {
        try {
            $conexao = Conexao::getInstance();
            $sql = 'INSERT INTO SCMEnergia (idLeitor, dataLeitura, mesReferencia, consumo, valorPago, status)
                    values
                    (:idLeitor, now(), :mesReferencia, :consumo,:valorPago, 1);';
            $conexao->beginTransaction();
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':idLeitor', $consumo->getIdLeitor());
            $p_sql->bindValue(':mesReferencia', $consumo->getMesReferencia());
            $p_sql->bindValue(':consumo', $consumo->getConsumo());
            $p_sql->bindValue(':valorPago', $consumo->getValorPago());
            $p_sql->execute();
            $conexao->commit();
            return 1;
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function todosApontamentos() {
        try {
            $sql = 'select u.usuario, e.idSCMEnergia AS idApontamento, e.consumo, e.valorPago, e.mesReferencia,  e.dataLeitura FROM 
                    SCMEnergia as e
                    LEFT JOIN usuario AS u ON u.idUsuario = e.idLeitor 
                    WHERE e.status = 1 ;';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_ASSOC);
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function verificaConta(consumoCemig $consumo) {
        try {
            $sql = 'SELECT * FROM SCMEnergia 
                    WHERE mesReferencia = :mes and status = 1;';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':mes', $consumo->getMesReferencia());

            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_ASSOC);
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function verificaContaID(consumoCemig $consumo) {
        try {
            $sql = 'select * FROM SCMEnergia 
                    WHERE mesReferencia = :mes and status = 1 AND idSCMEnergia != :id;';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':mes', $consumo->getMesReferencia());
            $p_sql->bindValue(':id', $consumo->getIdSCMEnergia());

            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_ASSOC);
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function editar(consumoCemig $consumo) {
        try {
            $sql = 'UPDATE SCMEnergia 
                        SET  mesReferencia = :mes, consumo = :consumo, valorPago = :valorPago, dataLeitura = now()
                        WHERE idSCMEnergia = :id ';
            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->bindValue(':mes', $consumo->getMesReferencia());
            $p_sql->bindValue(':consumo', $consumo->getConsumo());
            $p_sql->bindValue(':valorPago', $consumo->getValorPago());
            $p_sql->bindValue(':id', $consumo->getIdSCMEnergia());
            $p_sql->execute();
            return 1;
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function desabilitar(consumoCemig $consumo) {
        try {
            $sql = 'UPDATE SCMEnergia 
                        SET  status = 0
                        WHERE idSCMEnergia = :id ';
            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->bindValue(':id', $consumo->getIdSCMEnergia());
            $p_sql->execute();
            return 1;
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function energiaCemigPeriodoTotal($dataInicial, $dataFinal) {
        try {
            $sql = 'SELECT  
                    * 
                    from SCMEnergia 
                    WHERE 
                    status = 1 AND mesReferencia  BETWEEN :dataInicial AND :dataFinal ORDER BY mesReferencia ASC;';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':dataInicial', $dataInicial);
            $p_sql->bindValue(':dataFinal', $dataFinal);

            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function energiaCemigPeriodoTotalSomatorio($dataInicial, $dataFinal) {
        try {
            $sql = 'SELECT  
                                        round(sum(valorPago),2) as totalCemig
                    from SCMEnergia 
                    WHERE 
                    status = 1 AND mesReferencia  BETWEEN :dataInicial AND :dataFinal ORDER BY mesReferencia ASC;';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':dataInicial', $dataInicial);
            $p_sql->bindValue(':dataFinal', $dataFinal);

            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function valorKWHCemig($dataInicial, $dataFinal) {
        try {
            $sql = 'SELECT  
                    (sum(consumo)/
                    sum(valorPago)) as valorKWHCemig
                    from SCMEnergia 
                    WHERE 
                    status = 1 AND mesReferencia  BETWEEN :dataInicial AND :dataFinal ';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':dataInicial', $dataInicial);
            $p_sql->bindValue(':dataFinal', $dataFinal);

            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            return $ex;
        }
    }
    public function consumoTotalCemig($dataInicial, $dataFinal) {
        try {
            $sql = 'SELECT  
                    sum(consumo) as consumo
                   
                    from SCMEnergia 
                    WHERE 
                    status = 1 AND mesReferencia  BETWEEN :dataInicial AND :dataFinal ';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':dataInicial', $dataInicial);
            $p_sql->bindValue(':dataFinal', $dataFinal);

            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            return $ex;
        }
    }
    public function consumoCemig($dataInicial, $dataFinal) {
        try {
            $sql = 'SELECT  
                    consumo as consumo
                   
                    from SCMEnergia 
                    WHERE 
                    status = 1 AND mesReferencia  BETWEEN :dataInicial AND :dataFinal ';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':dataInicial', $dataInicial);
            $p_sql->bindValue(':dataFinal', $dataFinal);

            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            return $ex;
        }
    }

}
