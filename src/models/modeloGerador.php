<?php

namespace SCM\models;

use PDO;
use SCM\entity\gerador;
use SCM\util\conexao;

class modeloGerador {

    public function cadastrar(gerador $gerador) {
        try {
            $conexao = Conexao::getInstance();
            $sql = 'INSERT INTO SCMGerador (local, descricao,dataCriacao,status)
                    values
                    (:local, :descricao, now(),1)';
            $conexao->beginTransaction();
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':local', $gerador->getLocal());
            $p_sql->bindValue(':descricao', $gerador->getDescricao());
            $p_sql->execute();
            $conexao->commit();
            return 1;
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function todosGeradores() {
        try {
            $sql = 'SELECT * FROM SCMGerador';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function editar(gerador $gerador) {
        try {
            $sql = 'UPDATE SCMGerador  SET local = :local, descricao = :descricao WHERE idSCMGerador = :id';

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':descricao', $gerador->getDescricao());
            $p_sql->bindValue(':local', $gerador->getLocal());
            $p_sql->bindValue(':id', $gerador->getIdSCMGerador());
            $p_sql->execute();
            return 1;
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function desativarGerador(gerador $gerador) {
        try {
            $sql = 'UPDATE SCMGerador  SET status = 0 WHERE idSCMGerador = :id';

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $gerador->getIdSCMGerador());
            $p_sql->execute();
            return 1;
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function ativarGerador(gerador $gerador) {
        try {
            $sql = 'UPDATE SCMGerador  SET status = 1 WHERE idSCMGerador = :id';

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $gerador->getIdSCMGerador());
            $p_sql->execute();
            return 1;
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function todosGeradoresAtivados() {
        try {
            $sql = 'SELECT * FROM SCMGerador WHERE status = 1';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function geradorEspecifico($id) {
        try {
            $sql = 'SELECT * FROM SCMGerador WHERE idSCMGerador = :id';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $id);

            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            return $ex;
        }
    }

}
