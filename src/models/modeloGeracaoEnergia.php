<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace SCM\models;

use PDO;
use SCM\util\conexao;
use SCM\entity\geracaoEnergia;

class modeloGeracaoEnergia {

    public function cadastrar(geracaoEnergia $geracao) {
        try {
            $conexao = Conexao::getInstance();
            $sql = 'INSERT INTO SCMGEnergia (idSCMGerador, idLeitor, dataLeitura, dataInicial, dataFinal, valorProduzidoInicial,valorProduzidoFinal, status, diasApontamento, mediaProduzido)
                    values
                    (:idSCMGerador, :idLeitor, now(), :dataInicial, :dataFinal, :valorProduzidoInicial,:valorProduzidoFinal, 1,:diasApontamento, :mediaProduzido)';
            $conexao->beginTransaction();
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':idSCMGerador', $geracao->getIdSCMGerador());
            $p_sql->bindValue(':idLeitor', $geracao->getIdLeitor());
            $p_sql->bindValue(':dataInicial', $geracao->getDataInicial());
            $p_sql->bindValue(':dataFinal', $geracao->getDataFinal());
            $p_sql->bindValue(':valorProduzidoInicial', $geracao->getValorProduzidoInicial());
            $p_sql->bindValue(':valorProduzidoFinal', $geracao->getValorProduzidoFinal());
            $p_sql->bindValue(':diasApontamento', $geracao->getDiasApontamento());
            $p_sql->bindValue(':mediaProduzido', $geracao->getMediaProduzido());
            $p_sql->execute();


            $id = $conexao->lastInsertId();

            for ($i = 0; $i <= $geracao->getDiasApontamento(); $i++) {

                $sql = 'INSERT INTO SCMGEnergiaApontamento
                    (idSCMGEnergia, idSCMGerador, dia, mediaProduzido, status) 
                    values
                    (:idSCMGEnergia, :idSCMGerador, :dia, :mediaProduzido, 1)';
                $p_sql = Conexao::getInstance()->prepare($sql);
                $p_sql->bindValue(':idSCMGEnergia', $id);
                $p_sql->bindValue(':idSCMGerador', $geracao->getIdSCMGerador());
                $p_sql->bindValue(':dia', $geracao->getDataInicial());
                $p_sql->bindValue(':mediaProduzido', $geracao->getMediaProduzido());
                $p_sql->execute();
                $geracao->setDataInicial(date('Y-m-d', strtotime("+1 days", strtotime($geracao->getDataInicial()))));
            }


            $conexao->commit();
            return 1;
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function todosApontamentos() {
        try {
            /*
              $sql = 'SELECT
              g.local AS localizacao, CONCAT("Gerador: ",g.descricao) as title, CONCAT(DATE_FORMAT(e.dataInicial, "%Y-%m-%d")," 00:00:00") AS start, CONCAT(DATE_FORMAT(e.dataFinal, "%Y-%m-%d")," 23:59:59") AS end, e.valorProduzidoInicial , e.valorProduzidoFinal, e.dataLeitura, (e.valorProduzidoFinal - e.valorProduzidoInicial ) AS valorProduzido, e.idSCMGEnergia AS idApontamento, g.idSCMGerador as idGerador
              FROM SCMGerador  AS g
              LEFT JOIN SCMGEnergia AS e ON g.idSCMGerador = e.idSCMGerador
              WHERE e.status  = 1;'; */
            $sql = 'SELECT 
                    C.idSCMGEnergia as idApontamento,C.idSCMGerador as idGerador, C.dataInicial, C.dataFinal, C.valorProduzidoInicial,C.valorProduzidoFinal, (C.valorProduzidoFinal - C.valorProduzidoInicial) AS valorProduzido,
                    CONCAT(DATE_FORMAT(CA.dia, "%Y-%m-%d")," 00:00:00") AS start, CONCAT(DATE_FORMAT(CA.dia, "%Y-%m-%d")," 23:59:59") AS end, CA.mediaProduzido,
                    (SELECT local FROM SCMGerador WHERE idSCMGerador = idGerador) AS localizacao, (SELECT descricao FROM SCMGerador WHERE idSCMGerador = idGerador) AS title
                    FROM SCMGEnergia AS C
                    LEFT JOIN SCMGEnergiaApontamento as CA on  C.idSCMGEnergia = CA.idSCMGEnergia
                    WHERE C.status = 1 AND CA.status = 1;';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_ASSOC);
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function editar(geracaoEnergia $geracao) {
        try {
            $conexao = Conexao::getInstance();
            $sql = 'UPDATE SCMGEnergia 
                    SET idSCMGerador = :idSCMGerador, idLeitor = :idLeitor, dataLeitura = now(), valorProduzidoInicial = :valorProduzidoInicial, valorProduzidoFinal = :valorProduzidoFinal,
                    dataInicial = :dataInicial, dataFinal = :dataFinal
                    WHERE 
                    idSCMGEnergia  = :id;';
            $conexao->beginTransaction();
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $geracao->getIdSCMGEnergia());
            $p_sql->bindValue(':idSCMGerador', $geracao->getIdSCMGerador());
            $p_sql->bindValue(':idLeitor', $geracao->getIdLeitor());
            $p_sql->bindValue(':dataInicial', $geracao->getDataInicial());
            $p_sql->bindValue(':dataFinal', $geracao->getDataFinal());
            $p_sql->bindValue(':valorProduzidoInicial', $geracao->getValorProduzidoInicial());
            $p_sql->bindValue(':valorProduzidoFinal', $geracao->getValorProduzidoFinal());
            $p_sql->execute();
            $conexao->commit();
            return 1;
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function desativar(geracaoEnergia $geracao) {
        try {

            $conexao = Conexao::getInstance();

            $sql = 'UPDATE SCMGEnergia 
                    SET status = 0
                    WHERE 
                    idSCMGEnergia  = :id;';
            $conexao->beginTransaction();
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $geracao->getIdSCMGEnergia());

            $p_sql->execute();

            $sql = 'UPDATE SCMGEnergiaApontamento
                    SET status = 0
                    WHERE idSCMGEnergia = :id';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $geracao->getIdSCMGEnergia());
            $p_sql->execute();

            $conexao->commit();

            return 1;
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function atualizarConsumoInicial($id) {
        try {
            $sql = 'SELECT valorProduzidoFinal, DATE_ADD(dataFinal , INTERVAL 1 DAY) as dataInicial, DATE_ADD(dataFinal , INTERVAL 7 DAY) AS dataFinal
                     FROM SCMGEnergia
                     WHERE idSCMGerador  = :id AND status = 1 ORDER BY dataFinal DESC LIMIT 1;';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $id);

            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function energiaConsumoPeriodoTotal($dataInicial, $dataFinal) {
        try {
            $sql = 'select date_format(dia, "%d/%m/%Y") as dia, round(sum(mediaProduzido),2) as consumo from SCMGEnergiaApontamento
                    WHERE dia between :dataInicial AND :dataFinal AND status = 1
                    GROUP BY dia ORDER BY dia;';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':dataInicial', $dataInicial);
            $p_sql->bindValue(':dataFinal', $dataFinal);

            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function energiaConsumoPeriodoTotalGerador($dataInicial, $dataFinal, $gerador) {
        try {
            $sql = 'select date_format(dia, "%d/%m/%Y") as dia, round(sum(mediaProduzido),2) as consumo from SCMGEnergiaApontamento
                    WHERE dia between :dataInicial AND :dataFinal AND status = 1 AND idSCMGerador = :id
                    GROUP BY dia ORDER BY dia;';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':dataInicial', $dataInicial);
            $p_sql->bindValue(':dataFinal', $dataFinal);
            $p_sql->bindValue(':id', $gerador);

            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            return $ex;
        }
    }
    public function totalEnergiaGerada($dataInicial, $dataFinal) {
        try {
            $sql = 'select  round(sum(mediaProduzido),2) as consumo from SCMGEnergiaApontamento
                    WHERE dia between :dataInicial AND :dataFinal AND status = 1 ';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':dataInicial', $dataInicial);
            $p_sql->bindValue(':dataFinal', $dataFinal);

            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            return $ex;
        }
    }
    public function energiaGerada($dataInicial, $dataFinal) {
        try {
            $sql = 'select  round(mediaProduzido,2) as consumo from SCMGEnergiaApontamento
                    WHERE dia between :dataInicial AND :dataFinal AND status = 1 ';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':dataInicial', $dataInicial);
            $p_sql->bindValue(':dataFinal', $dataFinal);

            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            return $ex;
        }
    }

    

}
