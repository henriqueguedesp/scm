<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace SCM\models;

/**
 * Description of modeloApontamentoProducao
 *
 * @author henrique.guedes
 */
use PDO;
use SCM\util\conexao;
use SCM\entity\producao;

class modeloApontamentoProducao {

    //put your code here
    public function cadastrar(producao $producao) {
        try {
            $conexao = Conexao::getInstance();
            $sql = 'INSERT INTO SCMProducao (idCriador,dataApontamento,tipo, dataInicial, dataFinal, totalProduzido,status,diasApontamento,mediaProducao)
                    values
                    (:idCriador, now(), :local,:dataInicial ,  :dataFinal, :totalProduzido, 1,:diasApontamento, :mediaProducao)';
            $conexao->beginTransaction();
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':idCriador', $producao->getIdCriador());
            $p_sql->bindValue(':dataInicial', $producao->getDataInicial());
            $p_sql->bindValue(':dataFinal', $producao->getDataFinal());
            $p_sql->bindValue(':totalProduzido', $producao->getTotalProduzido());
            $p_sql->bindValue(':diasApontamento', $producao->getDiasApontamento());
            $p_sql->bindValue(':mediaProducao', $producao->getMediaProducao());
            $p_sql->bindValue(':local', $producao->getLocal());
            $p_sql->execute();
            $id = $conexao->lastInsertId();

            for ($i = 0; $i <= $producao->getDiasApontamento(); $i++) {

                $sql = 'INSERT INTO SCMProducaoApontamento
                    (idSCMProducao,dia, mediaProduzido, status) 
                    values
                    (:idSCMProducao, :dia, :mediaProduzido, 1)';
                $p_sql = Conexao::getInstance()->prepare($sql);
                $p_sql->bindValue(':idSCMProducao', $id);
                $p_sql->bindValue(':dia', $producao->getDataInicial());
                $p_sql->bindValue(':mediaProduzido', $producao->getMediaProducao());
                $p_sql->execute();
                $producao->setDataInicial(date('Y-m-d', strtotime("+1 days", strtotime($producao->getDataInicial()))));
            }


            $conexao->commit();
            return 1;
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function todosApontamentos() {
        try {

            $sql = 'SELECT 
                    P.idSCMProducao as idApontamento,
                    P.dataInicial, P.dataFinal, 
                    IF(P.tipo = 0, "Torre de Tratamento", "TSI") AS title,
                    P.totalProduzido AS totalProduzido,
                    CONCAT(DATE_FORMAT(PA.dia, "%Y-%m-%d")," 00:00:00") AS start,
                    CONCAT(DATE_FORMAT(PA.dia, "%Y-%m-%d")," 23:59:59") AS end, 
                    PA.mediaProduzido
                    
                    FROM SCMProducao AS P
                    LEFT JOIN SCMProducaoApontamento as PA on  PA.idSCMProducao= P.idSCMProducao
                    WHERE P.status = 1 AND PA.status = 1;';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_ASSOC);
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function apontamentosEspecificios($tipo, $dataInicial, $dataFinal) {
        try {

            $sql = '
SELECT 
                    sum(PA.mediaProduzido) as total 
                    FROM SCMProducao AS P
                    LEFT JOIN SCMProducaoApontamento as PA on  PA.idSCMProducao= P.idSCMProducao
                    WHERE P.status = 1 AND PA.status = 1
                    AND PA.dia between :dataInicial AND :dataFinal
                    AND P.tipo = :tipo;';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':tipo', $tipo);
            $p_sql->bindValue(':dataInicial', $dataInicial);
            $p_sql->bindValue(':dataFinal', $dataFinal);

            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            return $ex;
        }
    }
    public function apontamentosEspecificiosLinha($tipo, $dataInicial, $dataFinal) {
        try {

            $sql = '
SELECT 
                    PA.mediaProduzido as total,
                    PA.dia
                    FROM SCMProducao AS P
                    LEFT JOIN SCMProducaoApontamento as PA on  PA.idSCMProducao= P.idSCMProducao
                    WHERE P.status = 1 AND PA.status = 1
                    AND PA.dia between :dataInicial AND :dataFinal
                    AND P.tipo = :tipo;';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':tipo', $tipo);
            $p_sql->bindValue(':dataInicial', $dataInicial);
            $p_sql->bindValue(':dataFinal', $dataFinal);

            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function desativar(producao $producao) {
        try {

            $conexao = Conexao::getInstance();

            $sql = 'UPDATE SCMProducao 
                    SET status  = 0
                    WHERE idSCMProducao = :id';
            $conexao->beginTransaction();
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $producao->getIdSCMProducao());

            $p_sql->execute();

            $sql = 'UPDATE SCMProducaoApontamento
                    SET status = 0
                    WHERE idSCMProducao = :id';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $producao->getIdSCMProducao());
            $p_sql->execute();

            $conexao->commit();

            return 1;
        } catch (Exception $ex) {
            return $ex;
        }
    }

}
