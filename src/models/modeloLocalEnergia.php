<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace SCM\models;

/**
 * Description of modeloLocalEnergia
 *
 * @author henrique.guedes
 */

use PDO;
use SCM\util\conexao;
use SCM\entity\localEnergia;

class modeloLocalEnergia {
    
     public function cadastrar(localEnergia $localEnergia) {
        try {
            $conexao = Conexao::getInstance();
            $sql = 'INSERT INTO SCMLocalEnergia (idCriador, local, descricao,dataCriacao,status)
                    values
                    (:idCriador, :local, :descricao, now(),1)';
            $conexao->beginTransaction();
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':local', $localEnergia->getLocal());
            $p_sql->bindValue(':descricao', $localEnergia->getDescricao());
            $p_sql->bindValue(':idCriador', $localEnergia->getIdCriador());
            $p_sql->execute();
            $conexao->commit();
            return 1;
        } catch (Exception $ex) {
            return $ex;
        }
    }
    
     public function todosLocais() {
        try {
            $sql = 'SELECT * FROM SCMLocalEnergia';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            return $ex;
        }
    }
 public function editar(localEnergia $localEnergia) {
        try {
            $sql = 'UPDATE SCMLocalEnergia  SET local = :local, descricao = :descricao WHERE idSCMLocalEnergia = :id';

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':descricao', $localEnergia->getDescricao());
            $p_sql->bindValue(':local', $localEnergia->getLocal());
            $p_sql->bindValue(':id', $localEnergia->getIdLocalEnergia());
            $p_sql->execute();
            return 1;
        } catch (Exception $ex) {
            return $ex;
        }
    }
    public function desativarLocal(localEnergia $localEnergia) {
        try {
            $sql = 'UPDATE SCMLocalEnergia  SET status = 0 WHERE idSCMLocalEnergia = :id';

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $localEnergia->getIdLocalEnergia());
            $p_sql->execute();
            return 1;
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function ativarLocal(localEnergia $localEnergia) {
        try {
            $sql = 'UPDATE SCMLocalEnergia  SET status = 1 WHERE idSCMLocalEnergia = :id';

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $localEnergia->getIdLocalEnergia());
            $p_sql->execute();
            return 1;
        } catch (Exception $ex) {
            return $ex;
        }
    }   
    
     public function todosLocaisAtivados() {
        try {
            $sql = 'SELECT * FROM SCMLocalEnergia WHERE status = 1';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            return $ex;
        }
    }
    
    public function localEspecifico($id) {
        try {
            $sql = 'SELECT * FROM SCMLocalEnergia WHERE idSCMLocalEnergia = :id';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $id);

            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            return $ex;
        }
    }

}
