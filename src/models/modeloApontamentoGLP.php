<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace SCM\models;

use SCM\util\conexao;
use PDO;
USE SCM\entity\consumoGLP;

/**
 * Description of modeloApontamentoGLP
 *
 * @author henrique.guedes
 */
class modeloApontamentoGLP {

    public function cadastrar(consumoGLP $consumo) {
        try {
            $conexao = Conexao::getInstance();
            $sql = 'INSERT INTO SCMSConsumo (idSCMSecador, idLeitor, dataLeitura, dataInicial, dataFinal, valorConsumoInicial,valorConsumoFinal, status, diasApontamento, mediaConsumo) 
                    values
                    (:idSCMSecador, :idLeitor, now(), :dataInicial, :dataFinal, :valorConsumoInicial,:valorConsumoFinal, 1, :diasApontamento, :mediaConsumo)';
            $conexao->beginTransaction();
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':idSCMSecador', $consumo->getIdSCMSecador());
            $p_sql->bindValue(':idLeitor', $consumo->getIdLeitor());
            $p_sql->bindValue(':dataInicial', $consumo->getDataInicial());
            $p_sql->bindValue(':dataFinal', $consumo->getDataFinal());
            $p_sql->bindValue(':valorConsumoInicial', $consumo->getValorConsumoInicial());
            $p_sql->bindValue(':valorConsumoFinal', $consumo->getValoreConsumoFinal());
            $p_sql->bindValue(':diasApontamento', $consumo->getDiasApontamento());
            $p_sql->bindValue(':mediaConsumo', $consumo->getMediaConsumo());

            $p_sql->execute();
            $id = $conexao->lastInsertId();

            for ($i = 0; $i <= $consumo->getDiasApontamento(); $i++) {

                $sql = 'INSERT INTO SCMSConsumoApontamento
                    (idSCMSConsumo, idSCMSecador, dia, mediaConsumo, status) 
                    values
                    (:idSCMSConsumo, :idSCMSecador, :dia, :mediaConsumo, 1)';
                $p_sql = Conexao::getInstance()->prepare($sql);
                $p_sql->bindValue(':idSCMSConsumo', $id);
                $p_sql->bindValue(':idSCMSecador', $consumo->getIdSCMSecador());
                $p_sql->bindValue(':dia', $consumo->getDataInicial());
                $p_sql->bindValue(':mediaConsumo', $consumo->getMediaConsumo());
                $p_sql->execute();
                $consumo->setDataInicial(date('Y-m-d', strtotime("+1 days", strtotime($consumo->getDataInicial()))));
            }

            $conexao->commit();
            return 1;
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function todosApontamentos() {
        try {/*
          $sql = 'SELECT
          s.local AS localizacao, CONCAT("Secador : ",s.descricao) as title, CONCAT(DATE_FORMAT(c.dataInicial, "%Y-%m-%d")," 00:00:00") AS start, CONCAT(DATE_FORMAT(c.dataFinal, "%Y-%m-%d")," 23:59:59") AS end, c.valorConsumoInicial , c.valorConsumoFinal, c.dataLeitura, (c.valorConsumoFinal - c.valorConsumoInicial ) AS valorConsumo, c.idSCMSConsumo AS idApontamento, s.idSCMSecador as idSecador
          FROM SCMSecador  AS s
          LEFT JOIN SCMSConsumo AS c ON c.idSCMSecador = s.idSCMSecador
          WHERE c.status  = 1;'; */
            $sql = 'SELECT 
C.idSCMSConsumo as idApontamento,C.idSCMSecador as idSecador, C.dataInicial, C.dataFinal, C.valorConsumoInicial,C.valorConsumoFinal, (C.valorConsumoFinal - C.valorConsumoInicial) AS valorConsumo,
CONCAT(DATE_FORMAT(CA.dia, "%Y-%m-%d")," 00:00:00") AS start, CONCAT(DATE_FORMAT(CA.dia, "%Y-%m-%d")," 23:59:59") AS end, CA.mediaConsumo,
(SELECT local FROM SCMSecador WHERE idSCMSecador = idSecador) AS localizacao, (SELECT descricao FROM SCMSecador WHERE idSCMSecador = idSecador) AS title
FROM SCMSConsumo AS C
LEFT JOIN SCMSConsumoApontamento as CA on  C.idSCMSConsumo = CA.idSCMSConsumo
WHERE C.status = 1 AND CA.status = 1;';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_ASSOC);
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function editar(consumoGLP $consumo) {
        try {
            $sql = 'UPDATE SCMSConsumo 
                    SET idSCMSecador = :idSCMSecador, idLeitor = :idLeitor, dataLeitura = now(), valorConsumoInicial = :valorConsumoInicial ,valorConsumoFinal = :valorConsumoFinal,
                    dataInicial = :dataInicial, dataFinal = :dataFinal, diasApontamento = :diasApontamento, mediaConsumo = :mediaConsumo
                    WHERE idSCMSConsumo = :id';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':idSCMSecador', $consumo->getIdSCMSecador());
            $p_sql->bindValue(':idLeitor', $consumo->getIdLeitor());
            $p_sql->bindValue(':dataInicial', $consumo->getDataInicial());
            $p_sql->bindValue(':dataFinal', $consumo->getDataFinal());
            $p_sql->bindValue(':valorConsumoInicial', $consumo->getValorConsumoInicial());
            $p_sql->bindValue(':valorConsumoFinal', $consumo->getValoreConsumoFinal());
            $p_sql->bindValue(':id', $consumo->getIdSCMSConsumo());
            $p_sql->bindValue(':diasApontamento', $consumo->getDiasApontamento());
            $p_sql->bindValue(':mediaConsumo', $consumo->getMediaConsumo());
            $p_sql->execute();
            return 1;
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function desativar(consumoGLP $consumo) {
        try {
            $conexao = Conexao::getInstance();

            $sql = 'UPDATE SCMSConsumo 
                    SET status = 0
                    WHERE idSCMSConsumo = :id';
            $conexao->beginTransaction();
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $consumo->getIdSCMSConsumo());

            $p_sql->execute();

            $sql = 'UPDATE SCMSConsumoApontamento
                    SET status = 0
                    WHERE idSCMSConsumo = :id';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $consumo->getIdSCMSConsumo());
            $p_sql->execute();

            $conexao->commit();

            return 1;
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function atualizarConsumoInicial($id) {
        try {
            $sql = 'SELECT valorConsumoFinal, DATE_ADD(dataFinal , INTERVAL 1 DAY) as dataInicial, DATE_ADD(dataFinal , INTERVAL 7 DAY) AS dataFinal
                     FROM SCMSConsumo
                     WHERE idSCMSecador= :id AND status = 1 ORDER BY dataFinal DESC LIMIT 1;';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $id);

            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function glpConsumoPeriodoTotal($dataInicial, $dataFinal) {
        try {
            $sql = 'select date_format(dia, "%d/%m/%Y") as dia, ROUND(sum(mediaConsumo),2) as consumo from SCMSConsumoApontamento
                    WHERE dia between :dataInicial AND :dataFinal AND status = 1
                    GROUP BY dia ORDER BY dia;';
            $p_sql = Conexao::getInstance()->prepare($sql);
                        $p_sql->bindValue(':dataInicial', $dataInicial);
                        $p_sql->bindValue(':dataFinal', $dataFinal);

            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            return $ex;
        }
    }
    public function glpConsumoPeriodoTotalSecador($dataInicial, $dataFinal,$secador) {
        try {
            $sql = 'select date_format(dia, "%d/%m/%Y") as dia, round(sum(mediaConsumo),2) as consumo, (select descricao from SCMSecador WHERE idSCMSecador  = SCMSConsumoApontamento.idSCMSecador) as secador
                    from SCMSConsumoApontamento
                    WHERE dia between :dataInicial AND :dataFinal AND status = 1  AND idSCMSecador = :idSecador
                    GROUP BY dia ORDER BY dia;';
            $p_sql = Conexao::getInstance()->prepare($sql);
                        $p_sql->bindValue(':dataInicial', $dataInicial);
                        $p_sql->bindValue(':dataFinal', $dataFinal);
                        $p_sql->bindValue(':idSecador', $secador);

            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            return $ex;
        }
    }

}
