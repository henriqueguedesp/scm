<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace SCM\models;

use PDO;
use SCM\util\conexao;

/**
 * Description of modeloConfiguracoes
 *
 * @author henrique.guedes
 */
class modeloConfiguracoes {

    public function atualizarConfiguracoes($dataInicioSafra, $dataTerminoSafra) {
        try {
            $conexao = conexao::getInstance();
            $sql = 'INSERT INTO SCMDemaisInformacoes (dataInicioSafra, dataTerminoSafra, dataAlteracao)
                    values
                    (:dataInicioSafra, :dataTerminoSafra, now());';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':dataInicioSafra', $dataInicioSafra);
            $p_sql->bindValue(':dataTerminoSafra', $dataTerminoSafra);

            $p_sql->execute();
            return 1;
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function safraAtual() {
        try {
            $conexao = conexao::getInstance();
            $sql = 'SELECT  
                    * 
                    FROM SCMDemaisInformacoes
                    ORDER BY dataAlteracao DESC LIMIT 1;';
            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
            return 1;
        } catch (Exception $ex) {
            return $ex;
        }
    }

}
