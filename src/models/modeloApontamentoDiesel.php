<?php

namespace SCM\models;

use PDO;
use SCM\util\conexao;
use SCM\entity\consumoDiesel;

class modeloApontamentoDiesel {

    public function cadastrar(consumoDiesel $consumo) {
        try {
            $conexao = Conexao::getInstance();
            $sql = 'INSERT INTO SCMGConsumo (idSCMGerador, idLeitor, dataLeitura, dataInicial, dataFinal, valorConsumoInicial,valorConsumoFinal, status,diasApontamento,mediaConsumo)
                    values
                    (:idSCMGerador, :idLeitor, now(),:dataInicial, :dataFinal, :valorConsumoInicial,:valorConsumoFinal, 1,:diasApontamento, :mediaConsumo)';
            $conexao->beginTransaction();
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':idSCMGerador', $consumo->getIdSCMGerador());
            $p_sql->bindValue(':idLeitor', $consumo->getIdLeitor());
            $p_sql->bindValue(':dataInicial', $consumo->getDataInicial());
            $p_sql->bindValue(':dataFinal', $consumo->getDataFinal());
            $p_sql->bindValue(':valorConsumoInicial', $consumo->getValorConsumoInicial());
            $p_sql->bindValue(':valorConsumoFinal', $consumo->getValorConsumoFinal());
            $p_sql->bindValue(':diasApontamento', $consumo->getDiasApontamento());
            $p_sql->bindValue(':mediaConsumo', $consumo->getMediaConsumo());
            $p_sql->execute();
            $id = $conexao->lastInsertId();

            for ($i = 0; $i <= $consumo->getDiasApontamento(); $i++) {

                $sql = 'INSERT INTO SCMGConsumoApontamento
                    (idSCMGConsumo, idSCMGerador, dia, mediaConsumo, status) 
                    values
                    (:idSCMGConsumo, :idSCMGerador, :dia, :mediaConsumo, 1)';
                $p_sql = Conexao::getInstance()->prepare($sql);
                $p_sql->bindValue(':idSCMGConsumo', $id);
                $p_sql->bindValue(':idSCMGerador', $consumo->getIdSCMGerador());
                $p_sql->bindValue(':dia', $consumo->getDataInicial());
                $p_sql->bindValue(':mediaConsumo', $consumo->getMediaConsumo());
                $p_sql->execute();
                $consumo->setDataInicial(date('Y-m-d', strtotime("+1 days", strtotime($consumo->getDataInicial()))));
            }



            $conexao->commit();
            return 1;
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function todosApontamentos() {
        try {
            /*
              $sql = 'SELECT
              g.local AS localizacao, CONCAT("Gerador: ",g.descricao)as title, CONCAT(DATE_FORMAT(c.dataInicial, "%Y-%m-%d")," 00:00:00") AS start, CONCAT(DATE_FORMAT(c.dataFinal, "%Y-%m-%d")," 23:59:59") AS end, c.valorConsumoInicial , c.valorConsumoFinal, c.dataLeitura, (c.valorConsumoFinal - c.valorConsumoInicial ) AS consumoTotal, c.idSCMGConsumo AS idApontamento, g.idSCMGerador as idGerador
              FROM SCMGerador  AS g
              LEFT JOIN SCMGConsumo AS c ON c.idSCMGerador = g.idSCMGerador
              WHERE c.status  = 1;;'; */
            $sql = 'SELECT 
                    C.idSCMGConsumo as idApontamento,C.idSCMGerador as idGerador, C.dataInicial, C.dataFinal, C.valorConsumoInicial,C.valorConsumoFinal, (C.valorConsumoFinal - C.valorConsumoInicial) AS valorConsumo,
                    CONCAT(DATE_FORMAT(CA.dia, "%Y-%m-%d")," 00:00:00") AS start, CONCAT(DATE_FORMAT(CA.dia, "%Y-%m-%d")," 23:59:59") AS end, CA.mediaConsumo,
                    (SELECT local FROM SCMGerador WHERE idSCMGerador = idGerador) AS localizacao, (SELECT descricao FROM SCMGerador WHERE idSCMGerador = idGerador) AS title
                    FROM SCMGConsumo AS C
                    LEFT JOIN SCMGConsumoApontamento as CA on  C.idSCMGConsumo = CA.idSCMGConsumo
                    WHERE C.status = 1 AND CA.status = 1;';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_ASSOC);
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function editar(consumoDiesel $consumo) {
        try {
            $sql = 'UPDATE SCMGConsumo 
                    SET idSCMGerador = :idSCMGerador, idLeitor = :idLeitor, dataLeitura = now(), valorConsumoInicial = :valorConsumoInicial ,valorConsumoFinal = :valorConsumoFinal,
                    dataInicial = :dataInicial, dataFinal = :dataFinal
                    WHERE idSCMGConsumo = :id';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':idSCMGerador', $consumo->getIdSCMGerador());
            $p_sql->bindValue(':idLeitor', $consumo->getIdLeitor());
            $p_sql->bindValue(':dataInicial', $consumo->getDataInicial());
            $p_sql->bindValue(':dataFinal', $consumo->getDataFinal());
            $p_sql->bindValue(':valorConsumoInicial', $consumo->getValorConsumoInicial());
            $p_sql->bindValue(':valorConsumoFinal', $consumo->getValorConsumoFinal());
            $p_sql->bindValue(':id', $consumo->getIdSCMGConsumo());
            $p_sql->execute();
            return 1;
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function desativar(consumoDiesel $consumo) {
        try {
            /*$sql = 'UPDATE SCMGConsumo 
                    SET status  = 0
                    WHERE idSCMGConsumo = :id';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $consumo->getIdSCMGConsumo());
            $p_sql->execute();
            */
            
             $conexao = Conexao::getInstance();

           $sql = 'UPDATE SCMGConsumo 
                    SET status  = 0
                    WHERE idSCMGConsumo = :id';
            $conexao->beginTransaction();
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $consumo->getIdSCMGConsumo());

            $p_sql->execute();

            $sql = 'UPDATE SCMGConsumoApontamento
                    SET status = 0
                    WHERE idSCMGConsumo = :id';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $consumo->getIdSCMGConsumo());
            $p_sql->execute();

            $conexao->commit();
            
            return 1;
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function atualizarConsumoInicial($id) {
        try {
            $sql = 'SELECT valorConsumoFinal, DATE_ADD(dataFinal , INTERVAL 1 DAY) as dataInicial, DATE_ADD(dataFinal , INTERVAL 7 DAY) AS dataFinal
                     FROM SCMGConsumo 
                     WHERE idSCMGerador  = 1 AND status = 1 ORDER BY dataFinal DESC LIMIT 1;';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $id);

            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function dielselConsumoPeriodoTotal($dataInicial, $dataFinal) {
        try {
            $sql = 'select date_format(dia, "%d/%m/%Y") as dia, round(sum(mediaConsumo),2) as consumo from SCMGConsumoApontamento
                    WHERE dia between :dataInicial AND :dataFinal AND status = 1
                    GROUP BY dia ORDER BY dia;';
            $p_sql = Conexao::getInstance()->prepare($sql);
                        $p_sql->bindValue(':dataInicial', $dataInicial);
                        $p_sql->bindValue(':dataFinal', $dataFinal);

            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            return $ex;
        }
    }
    public function dielselConsumoPeriodoTotalFinal($dataInicial, $dataFinal) {
        try {
            $sql = 'select round(sum(mediaConsumo),2) as consumo from SCMGConsumoApontamento
                    WHERE dia between :dataInicial AND :dataFinal AND status = 1
                    ';
            $p_sql = Conexao::getInstance()->prepare($sql);
                        $p_sql->bindValue(':dataInicial', $dataInicial);
                        $p_sql->bindValue(':dataFinal', $dataFinal);

            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            return $ex;
        }
    }
    public function dieselConsumoPeriodoTotalGerador($dataInicial, $dataFinal,$gerador) {
        try {
            $sql = 'select date_format(dia, "%d/%m/%Y") as dia, round(sum(mediaConsumo),2) as consumo from SCMGConsumoApontamento
                    WHERE dia between :dataInicial AND :dataFinal AND status = 1 AND idSCMGerador = :gerador
                    GROUP BY dia ORDER BY dia;';
            $p_sql = Conexao::getInstance()->prepare($sql);
                        $p_sql->bindValue(':dataInicial', $dataInicial);
                        $p_sql->bindValue(':dataFinal', $dataFinal);
                        $p_sql->bindValue(':gerador', $gerador);

            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            return $ex;
        }
    }
    
}
