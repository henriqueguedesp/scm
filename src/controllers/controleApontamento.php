<?php

namespace SCM\controllers;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use SCM\util\sessao;
use SCM\models\modeloGerador;
use SCM\models\modeloSecador;
use SCM\models\modeloApontamentoDiesel;
use SCM\models\modeloGeracaoEnergia;
use SCM\models\modeloApontamentoGLP;
use SCM\models\modeloApontamentoConsumoCemig;
use SCM\entity\consumoDiesel;
use SCM\entity\geracaoEnergia;
use SCM\entity\consumoGLP;
use SCM\entity\consumoCemig;
use SCM\entity\producao;
use SCM\models\modeloApontamentoProducao;

class controleApontamento {

    private $twig;
    private $request;
    private $sessao;
    private $raiz = '/scm/public_html/';

    function __construct(Response $response, \Twig_Environment $twig, \Symfony\Component\HttpFoundation\Request $request, sessao $sessao) {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

    public function redireciona($destino) {
        $redirect = new RedirectResponse($destino);
        $redirect->send();
    }

    public function paginaApontamento() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            return $this->response->setContent($this->twig->render('apontamento/apontamento.html.twig', array('user' => $usuario)));
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }

    ##APONTAMENTO DE DIESEL

    public function paginaApontamentoDiesel() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $modelo = new modeloGerador();
            $geradores = $modelo->todosGeradoresAtivados();
            return $this->response->setContent($this->twig->render('apontamento/apontamentoDiesel.html.twig', array('user' => $usuario, 'geradores' => $geradores)));
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }

    public function cadastrarApontamentoDiesel() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $consumoDiesel = new consumoDiesel();
            $consumoDiesel->setIdSCMGerador($this->request->get('idGerador'));
            $consumoDiesel->setValorConsumoInicial($this->request->get('consumoInicial'));
            $consumoDiesel->setValorConsumoFinal($this->request->get('consumoFinal'));
            $consumoDiesel->setDataInicial($this->request->get('dataInicial'));
            $consumoDiesel->setDataFinal($this->request->get('dataFinal'));
            $consumoDiesel->setIdLeitor($usuario->idUsuario);
            if ($consumoDiesel->getValorConsumoInicial() > $consumoDiesel->getValorConsumoFinal()) {
                echo 10;
            } else {
                $diferenca = strtotime($consumoDiesel->getDataFinal()) - strtotime($consumoDiesel->getDataInicial());
                $consumoDiesel->setDiasApontamento(floor($diferenca / (60 * 60 * 24)));
                $consumoDiesel->setMediaConsumo(($consumoDiesel->getValorConsumoFinal() - $consumoDiesel->getValorConsumoInicial()) / ($consumoDiesel->getDiasApontamento() + 1));

                $modelo = new modeloApontamentoDiesel();
                $retorno = $modelo->cadastrar($consumoDiesel);
                if ($retorno == 1) {
                    echo 1;
                } else {
                    echo $retorno;
                }
            }
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }

    public function apontamentoDieselJson() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {

            $modelo = new modeloApontamentoDiesel();
            $apontamentos = $modelo->todosApontamentos();

            $retorno = json_encode($apontamentos);
            echo $retorno;
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }

    public function desativarApontamentoDiesel() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $consumoDiesel = new consumoDiesel();
            $consumoDiesel->setIdSCMGConsumo($this->request->get('id'));
            $modelo = new modeloApontamentoDiesel();
            $retorno = $modelo->desativar($consumoDiesel);
            if ($retorno == 1) {
                echo 1;
            } else {
                echo $retorno;
            }
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }

    ##APONTAMENTO DE GERAÇÃO DE ENERGIA

    public function apontamentoGeracaoEnergia() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $modelo = new modeloGerador();
            $geradores = $modelo->todosGeradoresAtivados();
            return $this->response->setContent($this->twig->render('apontamento/apontamentoGeracaoEnergia.html.twig', array('user' => $usuario, 'geradores' => $geradores)));
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }

    public function cadastrarApontamentoGeracaoEnergia() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {

            $geracaoEnergia = new geracaoEnergia();
            $geracaoEnergia->setIdSCMGerador($this->request->get('idGerador'));
            $geracaoEnergia->setDataInicial($this->request->get('dataInicial'));
            $geracaoEnergia->setDataFinal($this->request->get('dataFinal'));
            $geracaoEnergia->setValorProduzidoInicial($this->request->get('valorInicial'));
            $geracaoEnergia->setValorProduzidoFinal($this->request->get('valorFinal'));
            $geracaoEnergia->setIdLeitor($usuario->idUsuario);
            if ($geracaoEnergia->getValorProduzidoInicial() > $geracaoEnergia->getValorProduzidoFinal()) {
                echo 10;
            } else {

                $diferenca = strtotime($geracaoEnergia->getDataFinal()) - strtotime($geracaoEnergia->getDataInicial());
                $geracaoEnergia->setDiasApontamento(floor($diferenca / (60 * 60 * 24)));
                $geracaoEnergia->setMediaProduzido(($geracaoEnergia->getValorProduzidoFinal() - $geracaoEnergia->getValorProduzidoInicial()) / ($geracaoEnergia->getDiasApontamento() + 1));

                $modelo = new modeloGeracaoEnergia();
                $retorno = $modelo->cadastrar($geracaoEnergia);
                if ($retorno == 1) {
                    echo 1;
                } else {

                    echo $retorno;
                }
            }
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }

    public function apontamentoGeracaoEnergiaJson() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {

            $modelo = new modeloGeracaoEnergia();
            $apontamentos = $modelo->todosApontamentos();

            $retorno = json_encode($apontamentos);
            echo $retorno;
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }

    public function desativarApontamentoGeracaoEnergia() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $geracaoEnergia = new geracaoEnergia();
            $geracaoEnergia->setIdSCMGEnergia($this->request->get('id'));

            $modelo = new modeloGeracaoEnergia();
            $retorno = $modelo->desativar($geracaoEnergia);
            if ($retorno == 1) {
                echo 1;
            } else {
                echo $retorno;
            }
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }

    ##APONTAMENTO CONSUMO GLP

    public function apontamentoConsumoGLP() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $modelo = new modeloSecador();
            $secadores = $modelo->todosSecadoresAtivos();
            return $this->response->setContent($this->twig->render('apontamento/apontamentoConsumoGLP.html.twig', array('user' => $usuario, 'secadores' => $secadores)));
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }

    public function cadastrarApontamentoConsumoGLP() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $consumoGLP = new consumoGLP();
            $consumoGLP->setIdSCMSecador($this->request->get('idSecador'));
            $consumoGLP->setDataInicial($this->request->get('dataInicial'));
            $consumoGLP->setDataFinal($this->request->get('dataFinal'));
            $consumoGLP->setValorConsumoInicial($this->request->get('consumoInicial'));
            $consumoGLP->setValoreConsumoFinal($this->request->get('consumoFinal'));
            $consumoGLP->setIdLeitor($usuario->idUsuario);

            if ($consumoGLP->getValorConsumoInicial() > $consumoGLP->getValoreConsumoFinal()) {
                echo 10;
            } else {
                $diferenca = strtotime($consumoGLP->getDataFinal()) - strtotime($consumoGLP->getDataInicial());
                $consumoGLP->setDiasApontamento(floor($diferenca / (60 * 60 * 24)));
                $consumoGLP->setMediaConsumo(($consumoGLP->getValoreConsumoFinal() - $consumoGLP->getValorConsumoInicial()) / ($consumoGLP->getDiasApontamento() + 1));



                $modelo = new modeloApontamentoGLP();

                $retorno = $modelo->cadastrar($consumoGLP);
                if ($retorno == 1) {
                    echo 1;
                } else {
                    echo $retorno;
                }
            }
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }

    public function apontamentoConsumoGLPJson() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {

            $modelo = new modeloApontamentoGLP();
            $apontamentos = $modelo->todosApontamentos();

            $retorno = json_encode($apontamentos);
            echo $retorno;
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }

    public function editarApontamentoConsumoGLP() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $consumoGLP = new consumoGLP();
            $consumoGLP->setIdSCMSecador($this->request->get('idSecador'));
            $consumoGLP->setDataInicial($this->request->get('dataInicial'));
            $consumoGLP->setDataFinal($this->request->get('dataFinal'));
            $consumoGLP->setValorConsumoInicial($this->request->get('valorInicial'));
            $consumoGLP->setValoreConsumoFinal($this->request->get('valorFinal'));
            $consumoGLP->setIdLeitor($usuario->idUsuario);
            $consumoGLP->setIdSCMSConsumo($this->request->get('idApontamento'));

            if ($consumoGLP->getValorConsumoInicial() > $consumoGLP->getValoreConsumoFinal()) {
                echo 10;
            } else {
                $diferenca = strtotime($consumoGLP->getDataFinal()) - strtotime($consumoGLP->getDataInicial());
                $consumoGLP->setDiasApontamento(floor($diferenca / (60 * 60 * 24)));
                $consumoGLP->setMediaConsumo(($consumoGLP->getValoreConsumoFinal() - $consumoGLP->getValorConsumoInicial()) / $consumoGLP->getDiasApontamento());
                $modelo = new modeloApontamentoGLP();

                $retorno = $modelo->editar($consumoGLP);
                if ($retorno == 1) {
                    echo 1;
                } else {
                    echo $retorno;
                }
            }
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }

    public function desativarApontamentoConsumoGPL() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $consumoGLP = new consumoGLP();
            $consumoGLP->setIdLeitor($usuario->idUsuario);
            $consumoGLP->setIdSCMSConsumo($this->request->get('id'));


            $modelo = new modeloApontamentoGLP();

            $retorno = $modelo->desativar($consumoGLP);
            if ($retorno == 1) {
                echo 1;
            } else {
                echo $retorno;
            }
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }

##APONTAMENTO CONSUMO CEMIG

    public function apontamentoConsumoCemig() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $modelo = new modeloApontamentoConsumoCemig();
            $apontamentos = $modelo->todosApontamentos();

            return $this->response->setContent($this->twig->render('apontamento/apontamentoConsumoCemig.html.twig', array('user' => $usuario, 'apontamentos' => $apontamentos)));
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }

    public function cadastrarApontamentoConsumoCemig() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $consumo = new consumoCemig();
            $consumo->setConsumo($this->request->get('consumo'));
            $consumo->setMesReferencia($this->request->get('mesReferencia'));
            $consumo->setValorPago($this->request->get('valorPago'));
            $consumo->setIdLeitor($usuario->idUsuario);


            $modelo = new modeloApontamentoConsumoCemig();


            $retorno = $modelo->verificaConta($consumo);

            if ($retorno) {
                echo 11;
            } else {
                $retorno = $modelo->cadastrar($consumo);
                if ($retorno == 1) {
                    echo 1;
                } else {
                    echo $retorno;
                }
            }
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }

    public function editarApontamentoConsumoCemig() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $consumo = new consumoCemig();
            $consumo->setConsumo($this->request->get('consumo'));
            $consumo->setMesReferencia($this->request->get('mesReferencia'));
            $consumo->setValorPago($this->request->get('valorPago'));
            $consumo->setIdLeitor($usuario->idUsuario);
            $consumo->setIdSCMEnergia($this->request->get('idApontamento'));

            $modelo = new modeloApontamentoConsumoCemig();


            $retorno = $modelo->verificaContaID($consumo);
            if ($retorno) {
                echo 11;
            } else {
                $retorno = $modelo->editar($consumo);
                if ($retorno == 1) {
                    echo 1;
                } else {
                    echo $retorno;
                }
            }
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }

    public function desativarApontamentoConsumoCemig() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $consumo = new consumoCemig();

            $consumo->setIdSCMEnergia($this->request->get('id'));
            $modelo = new modeloApontamentoConsumoCemig();
            $retorno = $modelo->desabilitar($consumo);
            if ($retorno == 1) {
                echo 1;
            } else {
                echo $retorno;
            }
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }

    public function atualizarConsumoInicialDiesel() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $id = $this->request->get('id');
            $modelo = new modeloApontamentoDiesel();
            $dados = $modelo->atualizarConsumoInicial($id);
            if ($dados) {
                $data = json_encode($dados);
                echo $data;
            } else {
                echo 'null';
            }
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }

    public function atualizarConsumoInicialEnergia() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $id = $this->request->get('id');
            $modelo = new modeloGeracaoEnergia();
            $dados = $modelo->atualizarConsumoInicial($id);
            if ($dados) {
                $data = json_encode($dados);
                echo $data;
            } else {
                echo 'null';
            }
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }

    public function atualizarConsumoInicialGLP() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $id = $this->request->get('id');
            $modelo = new modeloApontamentoGLP();
            $dados = $modelo->atualizarConsumoInicial($id);
            if ($dados) {
                $data = json_encode($dados);
                echo $data;
            } else {
                echo 'null';
            }
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }

##APONTAMENTO PRODUCAÇÃO

    public function apontamentoProducao() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $modelo = new modeloGerador();
            $geradores = $modelo->todosGeradoresAtivados();
            return $this->response->setContent($this->twig->render('apontamento/apontamentoProducao.html.twig', array('user' => $usuario, 'geradores' => $geradores)));
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }

    public function cadastrarApontamentoProducao() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $producao = new producao();
            $producao->setTotalProduzido($this->request->get('producaoTotal'));
            $producao->setDataInicial($this->request->get('dataInicial'));
            $producao->setDataFinal($this->request->get('dataFinal'));
            $producao->setLocal($this->request->get('local'));
            $producao->setIdCriador($usuario->idUsuario);

            $diferenca = strtotime($producao->getDataFinal()) - strtotime($producao->getDataInicial());
            $producao->setDiasApontamento(floor($diferenca / (60 * 60 * 24)));
            $producao->setMediaProducao($producao->getTotalProduzido() / ($producao->getDiasApontamento() + 1));

            $modelo = new modeloApontamentoProducao();
            $retorno = $modelo->cadastrar($producao);
            if ($retorno == 1) {
                echo 1;
            } else {
                echo $retorno;
            }
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }
    
     public function apontamentoProducaoJson() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {

            $modelo = new modeloApontamentoProducao();
            $apontamentos = $modelo->todosApontamentos();

            $retorno = json_encode($apontamentos);
            echo $retorno;
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }

        public function desativarApontamentoProducao() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $producao = new producao();
            $producao->setIdSCMProducao($this->request->get('id'));
            $modelo = new modeloApontamentoProducao();
            $retorno = $modelo->desativar($producao);
            if ($retorno == 1) {
                echo 1;
            } else {
                echo $retorno;
            }
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }
}
