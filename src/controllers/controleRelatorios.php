<?php

namespace SCM\controllers;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use SCM\util\sessao;
use SCM\models\modeloApontamentoGLP;
use SCM\models\modeloSecador;
use SCM\models\modeloGerador;
use SCM\models\modeloApontamentoDiesel;
use SCM\models\modeloGeracaoEnergia;
use SCM\models\modeloProtheus;
use SCM\models\modeloApontamentoConsumoCemig;

class controleRelatorios {

    private $twig;
    private $request;
    private $sessao;
    private $raiz = '/scm/public_html/';

    function __construct(Response $response, \Twig_Environment $twig, \Symfony\Component\HttpFoundation\Request $request, sessao $sessao) {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

    public function redireciona($destino) {
        $redirect = new RedirectResponse($destino);
        $redirect->send();
    }

    public function paginaRelatorios() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $modelo = new modeloSecador();
            $secadores = $modelo->todosSecadoresAtivos();
            $modelo = new modeloGerador();
            $geradores = $modelo->todosGeradoresAtivados();
            return $this->response->setContent($this->twig->render('relatorio/relatorios.html.twig', array('user' => $usuario, 'secadores' => $secadores, 'geradores' => $geradores)));
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }

    public function glpConsumoPeriodoTotal() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $dataInicial = $this->request->get('form01DataInicial');
            $dataFinal = $this->request->get('form01DataFinal');

            $modelo = new modeloApontamentoGLP();
            $dados = $modelo->glpConsumoPeriodoTotal($dataInicial, $dataFinal);


            $dataIni = str_replace("-", "", $dataInicial);
            $dataFin = str_replace("-", "", $dataFinal);

            $modelo = new modeloProtheus();
            $retorno = $modelo->totalGastoPorProduto($dataIni, $dataFin, '00100142', '1110401', '1110404');
            $valorGLP = 0;
            if ($retorno->VALORTOTAL) {
                $valorGLP = $retorno->VALORTOTAL / $retorno->QUANTIDADE;
            } else {
                $retorno = $modelo->totalGastoPorProdutoHistorico('00100142', '1110401', '1110404');
                $valorGLP = $retorno->VALORTOTAL / $retorno->QUANTIDADE;
            }

            $valorGLP = round($valorGLP, 2);

            $total = 0;

            header('Cache-Control: no-cache, must-revalidate');
            header('Pragma: no-cache');
            header('Content-Type: application/x-msexcel');
            header("Content-Disposition: attachment; filename = Relatório consumo por período_Consumo GLP.xls");
            echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
            echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
            echo "<table border='1'>";

            echo "<tr>";
            echo "<td><b>Usuário</b></td>";
            echo "<td>" . $usuario->usuario . "</td>";
            echo "</tr>";

            echo "<tr>";
            echo "<td><b>Descrição</b></td>";
            echo '<td>Relatório informa consumo de GLP de todos os secadores por data.</td>';
            echo "</tr>";

            echo "<tr>";
            echo "<td><b>Secador</b></td>";
            echo "<td> Todos secadores</td>";
            echo "</tr>";

            echo "<tr>";
            echo "<td><b>Valor kg gás (R$)</b></td>";
            echo "<td>" . number_format($valorGLP, 2, ",", ".") . "</td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td><b>Data inicial</b></td>";
            echo "<td>" . $dataInicial . "</td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td><b>Data final</b></td>";
            echo "<td>" . $dataFinal . "</td>";
            echo "</tr>";

            echo "<table border='0'>";

            echo "<table border='1'>";
            echo "<tr>";
            echo "<td><center><b>Dia</b></center></td>";
            echo "<td><center><b>Consumo (kg)</b></center></td>";
            echo "</tr>";
            foreach ($dados as $dado) {
                echo "<tr>";
                echo "<td>" . $dado->dia . "</td>";
                echo "<td>" . number_format($dado->consumo, 2, ",", ".") . "</td>";
                echo "</tr>";
                $total = $total + $dado->consumo;
            }
            echo "<tr>";
            echo "<td><b>Consumo total</b></td>";
            echo "<td><b>" . number_format($total, 2, ",", ".") . "</b></td>";
            echo "</tr>";

            $valorGLP = $valorGLP * $total;
            echo "<tr>";
            echo "<td><b>Custo total (R$)</b></td>";
            echo "<td> <b>" . number_format($valorGLP, 2, ",", ".") . "</b></td>";
            echo "</tr>";
            echo "</table>";
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }

    public function glpConsumoPeriodoTotalSecador() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $dataInicial = $this->request->get('form02DataInicial');
            $dataFinal = $this->request->get('form02DataFinal');
            $secador = $this->request->get('form02Secador');

            $modelo = new modeloSecador();
            $des = $modelo->secadorEspecifico($secador);

            $modelo = new modeloApontamentoGLP();
            $dados = $modelo->glpConsumoPeriodoTotalSecador($dataInicial, $dataFinal, $secador);


            $dataIni = str_replace("-", "", $dataInicial);
            $dataFin = str_replace("-", "", $dataFinal);

            $modelo = new modeloProtheus();
            $retorno = $modelo->totalGastoPorProduto($dataIni, $dataFin, '00100142', '1110401', '1110404');
            $valorGLP = 0;
            if ($retorno->VALORTOTAL) {
                $valorGLP = $retorno->VALORTOTAL / $retorno->QUANTIDADE;
            } else {
                $retorno = $modelo->totalGastoPorProdutoHistorico('00100142', '1110401', '1110404');
                $valorGLP = $retorno->VALORTOTAL / $retorno->QUANTIDADE;
            }

            $valorGLP = round($valorGLP, 2);


            $total = 0;

            header('Cache-Control: no-cache, must-revalidate');
            header('Pragma: no-cache');
            header('Content-Type: application/x-msexcel');
            header("Content-Disposition: attachment; filename = Relatório consumo por período_Consumo GLP.xls");
            echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
            echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
            echo "<table border='1'>";

            echo "<tr>";
            echo "<td><b>Usuário</b></td>";
            echo "<td>" . $usuario->usuario . "</td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td><b>Descrição</b></td>";
            echo "<td>Relatório informa consumo de GLP de secador especifíco por data.</td>";
            echo "</tr>";

            echo "<tr>";
            echo "<td><b>Secador</b></td>";
            echo "<td>" . $des->descricao . "</td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td><b>Valor kg gás (R$)</b></td>";
            echo "<td>" . number_format($valorGLP, 2, ",", ".") . "</td>";
            echo "</tr>";
            echo "<td><b>Data inicial</b></td>";
            echo "<td>" . $dataInicial . "</td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td><b>Data final</b></td>";
            echo "<td>" . $dataFinal . "</td>";
            echo "</tr>";

            echo "<table border='0'>";

            echo "<tr></tr>";
            echo "<table border='1'>";
            echo "<tr>";
            echo "<td><center><b>Dia</b></center></td>";
            echo "<td><center><b>Consumo (kg)</b></center></td>";
            echo "</tr>";
            foreach ($dados as $dado) {
                echo "<tr>";
                echo "<td>" . $dado->dia . "</td>";
                echo "<td>" . number_format($dado->consumo, 2, ",", ".") . "</td>";
                echo "</tr>";
                $total = $total + $dado->consumo;
            }
            echo "<tr>";
            echo "<td><b>Consumo total</b></td>";
            echo "<td><b>" . number_format($total, 2, ",", ".") . "</b></td>";
            echo "</tr>";
            $valorGLP = $valorGLP * $total;
            echo "<tr>";
            echo "<td><b>Custo total (R$)</b></td>";
            echo "<td> <b>" . number_format($valorGLP, 2, ",", ".") . "</b></td>";
            echo "</table>";
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }

    public function dielselConsumoPeriodoTotal() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $dataInicial = $this->request->get('form02DataInicial');
            $dataFinal = $this->request->get('form02DataFinal');

            $modelo = new modeloApontamentoDiesel();
            $dados = $modelo->dielselConsumoPeriodoTotal($dataInicial, $dataFinal);

            $dataIni = str_replace("-", "", $dataInicial);
            $dataFin = str_replace("-", "", $dataFinal);

            $modelo = new modeloProtheus();
            $retorno = $modelo->totalGastoPorProduto($dataIni, $dataFin, '00100158', '1119501', '1119501');
            $valorDiesel = 0;
            if ($retorno->VALORTOTAL) {
                $valorDiesel = $retorno->VALORTOTAL / $retorno->QUANTIDADE;
            } else {
                $retorno = $modelo->totalGastoPorProdutoHistorico('00100158', '1119501', '1119501');
                $valorDiesel = $retorno->VALORTOTAL / $retorno->QUANTIDADE;
            }
            $valorDiesel = round($valorDiesel, 2);

            $total = 0;

            header('Cache-Control: no-cache, must-revalidate');
            header('Pragma: no-cache');
            header('Content-Type: application/x-msexcel');
            header("Content-Disposition: attachment; filename = Relatório consumo por período_Diesel.xls");
            echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
            echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
            echo "<table border='1'>";

            echo "<tr>";
            echo "<td><b>Usuário</b></td>";
            echo "<td>" . $usuario->usuario . "</td>";
            echo "</tr>";

            echo "<tr>";
            echo "<td><b>Descrição</b></td>";
            echo '<td>Relatório informa consumo de diesel de todos os geradores por data.</td>';
            echo "</tr>";

            echo "<tr>";
            echo "<td><b>Gerador</b></td>";
            echo "<td> Todos geradores</td>";
            echo "</tr>";

            echo "<tr>";
            echo "<td><b>Valor litro do diesel (R$)</b></td>";
            echo "<td>" . number_format($valorDiesel, 2, ",", ".") . "</td>";
            echo "</tr>";

            echo "<tr>";
            echo "<td><b>Data inicial</b></td>";
            echo "<td>" . $dataInicial . "</td>";
            echo "</tr>";

            echo "<tr>";
            echo "<td><b>Data final</b></td>";
            echo "<td>" . $dataFinal . "</td>";
            echo "</tr>";

            echo "<table border='0'>";

            echo "<table border='1'>";
            echo "<tr>";
            echo "<td><center><b>Dia</b></center></td>";
            echo "<td><center><b>Consumo (l)</b></center></td>";
            echo "</tr>";
            foreach ($dados as $dado) {
                echo "<tr>";
                echo "<td>" . $dado->dia . "</td>";
                echo "<td>" . number_format($dado->consumo, 2, ",", ".") . "</td>";
                echo "</tr>";
                $total = $total + $dado->consumo;
            }
            echo "<tr>";
            echo "<td><b>Consumo total</b></td>";
            echo "<td><b>" . number_format($total, 2, ",", ".") . "</b></td>";
            echo "</tr>";
            echo "<tr>";
            $valorDiesel = $valorDiesel * $total;

            echo "<td><b>Custo total (R$)</b></td>";

            echo "<td><b>" . number_format($valorDiesel, 2, ",", ".") . "</b></td>";
            echo "</tr>";
            echo "</table>";
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }

    public function dieselConsumoPeriodoTotalGerador() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $dataInicial = $this->request->get('form04DataInicial');
            $dataFinal = $this->request->get('form04DataFinal');
            $gerador = $this->request->get('form04Gerador');

            $modelo = new modeloGerador();
            $desc = $modelo->geradorEspecifico($gerador);
            $modelo = new modeloApontamentoDiesel();
            $dados = $modelo->dieselConsumoPeriodoTotalGerador($dataInicial, $dataFinal, $gerador);

            $dataIni = str_replace("-", "", $dataInicial);
            $dataFin = str_replace("-", "", $dataFinal);

            $modelo = new modeloProtheus();
            $retorno = $modelo->totalGastoPorProduto($dataIni, $dataFin, '00100158', '1119501', '1119501');
            $valorDiesel = 0;
            if ($retorno->VALORTOTAL) {
                $valorDiesel = $retorno->VALORTOTAL / $retorno->QUANTIDADE;
            } else {
                $retorno = $modelo->totalGastoPorProdutoHistorico('00100158', '1119501', '1119501');
                $valorDiesel = $retorno->VALORTOTAL / $retorno->QUANTIDADE;
            }

            $valorDiesel = round($valorDiesel, 2);

            $total = 0;

            header('Cache-Control: no-cache, must-revalidate');
            header('Pragma: no-cache');
            header('Content-Type: application/x-msexcel');
            header("Content-Disposition: attachment; filename = Relatório consumo por período e por gerador_Diesel.xls");
            echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
            echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
            echo "<table border='1'>";

            echo "<tr>";
            echo "<td><b>Usuário</b></td>";
            echo "<td>" . $usuario->usuario . "</td>";
            echo "</tr>";

            echo "<tr>";
            echo "<td><b>Descrição</b></td>";
            echo '<td>Relatório informa consumo de diesel por gerador e por data.</td>';
            echo "</tr>";

            echo "<tr>";
            echo "<td><b>Gerador</b></td>";
            echo "<td>" . $desc->descricao . "</td>";
            echo "</tr>";

            echo "<tr>";
            echo "<td><b>Valor litro do diesel (R$)</b></td>";
            echo "<td>" . number_format($valorDiesel, 2, ",", ".") . "</td>";
            echo "</tr>";

            echo "<tr>";
            echo "<td><b>Data inicial</b></td>";
            echo "<td>" . $dataInicial . "</td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td><b>Data final</b></td>";
            echo "<td>" . $dataFinal . "</td>";
            echo "</tr>";

            echo "<table border='0'>";

            echo "<table border='1'>";
            echo "<tr>";
            echo "<td><center><b>Dia</b></center></td>";
            echo "<td><center><b>Consumo (l)</b></center></td>";
            echo "</tr>";
            foreach ($dados as $dado) {
                echo "<tr>";
                echo "<td>" . $dado->dia . "</td>";
                echo "<td>" . number_format($dado->consumo, 2, ",", ".") . "</td>";
                echo "</tr>";
                $total = $total + $dado->consumo;
            }
            echo "<tr>";
            echo "<td><b>Consumo total</b></td>";
            echo "<td><b>" . number_format($total, 2, ",", ".") . "</b></td>";
            echo "</tr>";
            $valorDiesel = $valorDiesel * $total;

            echo "<td><b>Custo total (R$)</b></td>";

            echo "<td><b>" . number_format($valorDiesel, 2, ",", ".") . "</b></td>";
            echo "</tr>";
            echo "</table>";
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }

    public function energiaConsumoPeriodoTotal() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $dataInicial = $this->request->get('form05DataInicial');
            $dataFinal = $this->request->get('form05DataFinal');

            $modelo = new modeloGeracaoEnergia();
            $dados = $modelo->energiaConsumoPeriodoTotal($dataInicial, $dataFinal);




            $total = 0;

            header('Cache-Control: no-cache, must-revalidate');
            header('Pragma: no-cache');
            header('Content-Type: application/x-msexcel');
            header("Content-Disposition: attachment; filename = Relatório geração energia por período.xls");
            echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
            echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
            echo "<table border='1'>";

            echo "<tr>";
            echo "<td><b>Usuário</b></td>";
            echo "<td>" . $usuario->usuario . "</td>";
            echo "</tr>";

            echo "<tr>";
            echo "<td><b>Descrição</b></td>";
            echo '<td>Relatório informa a energia gerada por todos os geradores e por data.</td>';
            echo "</tr>";

            echo "<tr>";
            echo "<td><b>Gerador</b></td>";
            echo "<td> Todos geradores</td>";
            echo "</tr>";

            echo "<tr>";
            echo "<td><b>Data inicial</b></td>";
            echo "<td>" . $dataInicial . "</td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td><b>Data final</b></td>";
            echo "<td>" . $dataFinal . "</td>";
            echo "</tr>";

            echo "<table border='0'>";

            echo "<table border='1'>";
            echo "<tr>";
            echo "<td><center><b>Dia</b></center></td>";
            echo "<td><center><b>Energia produzida (kWh)</b></center></td>";
            echo "</tr>";
            foreach ($dados as $dado) {
                echo "<tr>";
                echo "<td>" . $dado->dia . "</td>";
                echo "<td>" . number_format($dado->consumo, 2, ",", ".") . "</td>";
                echo "</tr>";
                $total = $total + $dado->consumo;
            }
            echo "<tr>";
            echo "<td><b>Energia total</b></td>";
            echo "<td><b>" . number_format($total, 2, ",", ".") . "</b></td>";
            echo "</tr>";
            echo "</table>";
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }

    public function energiaConsumoPeriodoTotalGerador() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $dataInicial = $this->request->get('form06DataInicial');
            $dataFinal = $this->request->get('form06DataFinal');
            $gerador = $this->request->get('form06Gerador');

            $modelo = new modeloGerador();
            $desc = $modelo->geradorEspecifico($gerador);
            $modelo = new modeloGeracaoEnergia();
            $dados = $modelo->energiaConsumoPeriodoTotalGerador($dataInicial, $dataFinal, $gerador);

            $total = 0;

            header('Cache-Control: no-cache, must-revalidate');
            header('Pragma: no-cache');
            header('Content-Type: application/x-msexcel');
            header("Content-Disposition: attachment; filename = Relatório geração energia por gerador e por período.xls");
            echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
            echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
            echo "<table border='1'>";

            echo "<tr>";
            echo "<td><b>Usuário</b></td>";
            echo "<td>" . $usuario->usuario . "</td>";
            echo "</tr>";

            echo "<tr>";
            echo "<td><b>Descrição</b></td>";
            echo '<td>Relatório informa a energia gerada por todos os geradores e por data.</td>';
            echo "</tr>";

            echo "<tr>";
            echo "<td><b>Gerador</b></td>";
            echo "<td>" . $desc->descricao . "</td>";
            echo "</tr>";

            echo "<tr>";
            echo "<td><b>Data inicial</b></td>";
            echo "<td>" . $dataInicial . "</td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td><b>Data final</b></td>";
            echo "<td>" . $dataFinal . "</td>";
            echo "</tr>";

            echo "<table border='0'>";

            echo "<table border='1'>";
            echo "<tr>";
            echo "<td><center><b>Dia</b></center></td>";
            echo "<td><center><b>Energia produzida (kWh)</b></center></td>";
            echo "</tr>";
            foreach ($dados as $dado) {
                echo "<tr>";
                echo "<td>" . $dado->dia . "</td>";
                echo "<td>" . number_format($dado->consumo, 2, ",", ".") . "</td>";
                echo "</tr>";
                $total = $total + $dado->consumo;
            }
            echo "<tr>";
            echo "<td><b>Energia total</b></td>";
            echo "<td><b>" . number_format($total, 2, ",", ".") . "</b></td>";
            echo "</tr>";
            echo "</table>";
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }

    public function energiaCemigPeriodoTotal() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            setlocale(LC_MONETARY, "pt_BR", "ptb");

            $dataInicial = $this->request->get('form07DataInicial');
            $dataFinal = $this->request->get('form07DataFinal');


            $modelo = new modeloApontamentoConsumoCemig();
            $dados = $modelo->energiaCemigPeriodoTotal($dataInicial, $dataFinal);


            $total = 0;
            $valor = 0;

            header('Cache-Control: no-cache, must-revalidate');
            header('Pragma: no-cache');
            header('Content-Type: application/x-msexcel');
            header("Content-Disposition: attachment; filename = Relatório consumo energia CEMIG e por período.xls");
            echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
            echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
            echo "<table border='1'>";

            echo "<tr>";
            echo "<td><b>Usuário</b></td>";
            echo "<td>" . $usuario->usuario . "</td>";
            echo "</tr>";

            echo "<tr>";
            echo "<td><b>Descrição</b></td>";
            echo '<td>Relatório informa o consumo de energia da CEMIG por data.</td>';
            echo "</tr>";

            echo "<tr>";
            echo "<td><b>Data inicial</b></td>";
            echo "<td>" . $dataInicial . "</td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td><b>Data final</b></td>";
            echo "<td>" . $dataFinal . "</td>";
            echo "</tr>";

            echo "<table border='0'>";
            echo "<table border='1'>";
            echo "<tr>";
            echo "<td><center><b>Mês</b></center></td>";
            echo "<td><center><b>Consumo (kWh)</b></center></td>";
            echo "<td><center><b>Valor pago (R$)</b></center></td>";
            echo "</tr>";
            foreach ($dados as $dado) {
                echo "<tr>";
                echo "<td>" . $dado->mesReferencia . "</td>";
                echo "<td>" . number_format($dado->consumo, 2, ",", ".") . "</td>";
                echo "<td>" . number_format($dado->valorPago, 2, ",", ".") . "</td>";
                echo "</tr>";
                $total = $total + $dado->consumo;
                $valor = $valor + $dado->valorPago;
            }
            echo "<tr>";
            echo "<td><b>Totais</b></td>";
            echo "<td><b>" . number_format($total, 2, ",", ".") . "</b></td>";

            echo "<td><b>" . number_format($valor, 2, ",", ".") . "</b></td>";
            echo "</tr>";
            echo "</table>";
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }

    public function energiaCemigMaisGeradaPeriodoTotal() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {

            $dataInicial = $this->request->get('form08DataInicial');
            $dataFinal = $this->request->get('form08DataFinal');


            $modelo = new modeloGeracaoEnergia();
            $dadosGeracao = $modelo->energiaConsumoPeriodoTotal($dataInicial, $dataFinal);

            $dataInicial = date("Y-m", strtotime($dataInicial));
            $dataFinal = date("Y-m", strtotime($dataFinal));

            $modelo = new modeloApontamentoConsumoCemig();
            $dados = $modelo->energiaCemigPeriodoTotal($dataInicial, $dataFinal);

            $tots = 0;
            foreach ($dados as $dado) {
                $tots = $tots + $dado->consumo;
            }
            foreach ($dadosGeracao as $dado) {
                $tots = $tots + $dado->consumo;
            }



            header('Cache-Control: no-cache, must-revalidate');
            header('Pragma: no-cache');
            header('Content-Type: application/x-msexcel');
            header("Content-Disposition: attachment; filename = Relatório consumo energia CEMIG mais energia gerada por período.xls");
            echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
            echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
            echo "<table  border='1' >";

            echo "<tr>";
            echo "<td><b>Usuário</b></td>";
            echo "<td>" . $usuario->usuario . "</td>";
            echo "</tr>";

            echo "<tr>";
            echo "<td><b>Descrição</b></td>";
            echo '<td>Relatório informa o consumo de energia da CEMIG mais total de energia gerada por data.</td>';
            echo "</tr>";

            echo "<tr>";
            echo "<td><b>Data inicial</b></td>";
            echo "<td>" . $dataInicial . "</td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td><b>Data final</b></td>";
            echo "<td>" . $dataFinal . "</td>";
            echo "</tr>";
            echo "</table>";

            echo "<table  border='0' >";

            echo "<tr style='border-width: 0px'>";
            echo "</table>";

            echo "<table  border='1' >";

            echo "</tr>";

            echo "<tr style='border-width: 1px'>";
            echo "<td colspan='2' ><center><b>CEMIG</b></center></td>";
            echo "<td colspan='2' ><center><b>Energia Gerada</b></center></td>";
            echo "<td colspan='2' ><center><b>Energia total</b></center></td>";
            echo "</tr>";

            echo "<tr>";
            echo "<td><center><b>Mês</b></center></td>";
            echo "<td><center><b>Consumo (kWh)</b></center></td>";
            echo "<td><center><b>Dia</b></center></td>";
            echo "<td><center><b>Energia Produzida</b></center></td>";
            echo "<td><center><b>Total de energia consumida</b></center></td>";
            echo "<td><b>" . number_format($tots, 2, ",", ".") . "</b></td>";

            echo "</tr>";

            $tamanho01 = count($dados);
            $tamanho02 = count($dadosGeracao);

            $i01 = 0;
            $i02 = 0;
            $total01 = 0;
            $total02 = 0;

            $control = false;
            $iControl = false;
            $tControl = false;
            while ($control == false) {

                if ($i01 < $tamanho01) {
                    echo "<tr>";
                    echo "<td >" . $dados[$i01]->mesReferencia . "</td>";
                    echo "<td '>" . number_format($dados[$i01]->consumo, 2, ",", ".") . "</td>";
                    $total01 = $total01 + $dados[$i01]->consumo;
                    $i01++;
                } else {
                    if (!$iControl) {
                        echo "<tr >";
                        echo "<td><b>Consumo total</b></td>";
                        echo "<td><b>" . number_format($total01, 2, ",", ".") . "</b></td>";
                        $iControl = true;
                    } else {

                        echo "<tr>";
                        echo "<td style='border-width: 0px'></td>";
                        echo "<td style='border-width: 0px'></td>";
                    }
                }

                if ($i02 < $tamanho02) {

                    echo "<td  >" . $dadosGeracao[$i02]->dia . "</td>";
                    echo "<td '>" . number_format($dadosGeracao[$i02]->consumo, 2, ",", ".") . "</td>";
                    echo "</tr>";
                    $total02 = $total02 + $dadosGeracao[$i02]->consumo;
                    $i02++;
                } else {
                    if (!$tControl) {
                        echo "<td><b>Consumo total</b></td>";
                        echo "<td><b>" . number_format($total02, 2, ",", ".") . "</b></td>";
                        echo "</tr>";
                        $tControl = true;
                    } else {

                        echo "<td style='border-width: 0px'></td>";
                        echo "<td style='border-width: 0px'></td>";
                        echo "</tr>";
                    }
                }
                if (($iControl == true) && ($tControl == true)) {
                    if (($i01 == $tamanho01 && $i02 == $tamanho02)) {
                        $control = true;
                    }
                }
            }


            echo "</table>";
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }

    public function testeSQL() {
        $modelo = new modeloProtheus();
        $linha = '4';
        $valorGasto = $modelo->valorTotalGastoPorCC("20190101", "20191231", "1110204", "1110204");
        $materialRecebido = $modelo->totalMatRecPorLinha("20190101", "20191231", $linha);
        print_r($valorGasto);
        print_r('<br>');
        print_r($materialRecebido);
        print_r('<br>');
        $val = $materialRecebido->MATERIAL / $valorGasto->TOTAL;
        print_r(round($val, 2));
        print_r('<br>');
        print_r($linha);
    }

    public function custoDespalhaPorPeriodo() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $dataInicial = $this->request->get('form09DataInicial');
            $dataFinal = $this->request->get('form09DataFinal');

            $dataIni = $dataInicial;
            $dataFin = $dataFinal;
            $dataInicial = str_replace("-", "", $dataInicial);
            $dataFinal = str_replace("-", "", $dataFinal);

            $custo01 = $this->calculaCustoTonelada($dataInicial, $dataFinal, '1110201', 1);
            $custo02 = $this->calculaCustoTonelada($dataInicial, $dataFinal, '1110202', 2);
            $custo03 = $this->calculaCustoTonelada($dataInicial, $dataFinal, '1110203', 3);
            $custo04 = $this->calculaCustoTonelada($dataInicial, $dataFinal, '1110204', 4);
            $modelo = new modeloProtheus();

            $material01 = $modelo->totalMatRecPorLinha($dataInicial, $dataFinal, 1);
            $material02 = $modelo->totalMatRecPorLinha($dataInicial, $dataFinal, 2);
            $material03 = $modelo->totalMatRecPorLinha($dataInicial, $dataFinal, 3);
            $material04 = $modelo->totalMatRecPorLinha($dataInicial, $dataFinal, 4);

            $valorGasto01 = $modelo->valorTotalGastoPorCC($dataInicial, $dataFinal, '1110201', '1110201');
            $valorGasto02 = $modelo->valorTotalGastoPorCC($dataInicial, $dataFinal, '1110202', '1110202');
            $valorGasto03 = $modelo->valorTotalGastoPorCC($dataInicial, $dataFinal, '1110203', '1110203');
            $valorGasto04 = $modelo->valorTotalGastoPorCC($dataInicial, $dataFinal, '1110204', '1110204');


            header('Cache-Control: no-cache, must-revalidate');
            header('Pragma: no-cache');
            header('Content-Type: application/x-msexcel');
            header("Content-Disposition: attachment; filename = Relatório custo despalha por período.xls");
            echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
            echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
            echo "<table border='1'>";

            echo "<tr>";
            echo "<td><b>Usuário</b></td>";
            echo "<td>" . $usuario->usuario . "</td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td><b>Descrição</b></td>";
            echo "<td>Relatório informa o custo da despalha por data.</td>";
            echo "</tr>";
            echo "<td><b>Data inicial</b></td>";
            echo "<td>" . $dataIni . "</td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td><b>Data final</b></td>";
            echo "<td>" . $dataFin . "</td>";
            echo "</tr>";

            echo "<table border='0'>";

            echo "<tr></tr>";
            echo "<table border='1'>";
            echo "<tr>";
            echo "<td colspan='2' ><center><b>Despalha 01</b></center></td>";
            echo "<td colspan='2' ><center><b>Despalha 02</b></center></td>";
            echo "<td colspan='2' ><center><b>Despalha 03</b></center></td>";
            echo "<td colspan='2' ><center><b>Despalha 04</b></center></td>";
            echo "</tr>";



            echo "<tr>";
            echo "<td> Total de material recebido (ton)</td>";
            echo "<td>" . number_format($material01->MATERIAL, 2, ",", ".") . "</td>";
            echo "<td> Total de material recebido (ton)</td>";
            echo "<td>" . number_format($material02->MATERIAL, 2, ",", ".") . "</td>";
            echo "<td> Total de material recebido (ton)</td>";
            echo "<td>" . number_format($material03->MATERIAL, 2, ",", ".") . "</td>";
            echo "<td> Total de material recebido (ton)</td>";
            echo "<td>" . number_format($material04->MATERIAL, 2, ",", ".") . "</td>";
            echo "</tr>";

            echo "<tr>";
            echo "<td> Total de custos (R$)</td>";
            echo "<td>" . number_format($valorGasto01->TOTAL, 2, ",", ".") . "</td>";
            echo "<td> Total de custos (R$)</td>";
            echo "<td>" . number_format($valorGasto02->TOTAL, 2, ",", ".") . "</td>";
            echo "<td> Total de custos (R$)</td>";
            echo "<td>" . number_format($valorGasto03->TOTAL, 2, ",", ".") . "</td>";
            echo "<td> Total de custos (R$)</td>";
            echo "<td>" . number_format($valorGasto04->TOTAL, 2, ",", ".") . "</td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td> Custo por tonelada</td>";
            echo "<td>" . number_format($custo01, 2, ",", ".") . "</td>";
            echo "<td> Custo por tonelada</td>";
            echo "<td>" . number_format($custo02, 2, ",", ".") . "</td>";
            echo "<td> Custo por tonelada</td>";
            echo "<td>" . number_format($custo03, 2, ",", ".") . "</td>";
            echo "<td> Custo por tonelada</td>";
            echo "<td>" . number_format($custo04, 2, ",", ".") . "</td>";
            echo "</tr>";
            echo "</table>";
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }

    public function calculaCustoTonelada($dataInicial, $dataFinal, $cc, $linha) {
        $modelo = new modeloProtheus();

        $valorGasto = $modelo->valorTotalGastoPorCC($dataInicial, $dataFinal, $cc, $cc);
        $materialRecebido = $modelo->totalMatRecPorLinha($dataInicial, $dataFinal, $linha);
        $val = $materialRecebido->MATERIAL / $valorGasto->TOTAL;
        return round($val, 2);
    }

    ##FUNÇÕES A SER CONSULTADAS

    public function consumoGLPPorSecador($secador, $dataInicial, $dataFinal, $ccInicial, $ccFinal) {
//função retorna valor gasto com GLP por determinado perído e por determinado secador; 
        $modelo = new modeloSecador();
        $des = $modelo->secadorEspecifico($secador);

        $modelo = new modeloApontamentoGLP();
        $dados = $modelo->glpConsumoPeriodoTotalSecador($dataInicial, $dataFinal, $secador);

        $dataIni = str_replace("-", "", $dataInicial);
        $dataFin = str_replace("-", "", $dataFinal);

        $modelo = new modeloProtheus();
        $retorno = $modelo->totalGastoPorProduto($dataIni, $dataFin, '00100142', $ccInicial, $ccFinal);
        $valorGLP = 0;
        if ($retorno->VALORTOTAL) {
            $valorGLP = $retorno->VALORTOTAL / $retorno->QUANTIDADE;
        } else {
            $retorno = $modelo->totalGastoPorProdutoHistorico('00100142', $ccInicial, $ccFinal);
            $valorGLP = $retorno->VALORTOTAL / $retorno->QUANTIDADE;
        }
        $valorGLP = round($valorGLP, 2);
        $total = 0;
        foreach ($dados as $dado) {
            $total = $total + $dado->consumo;
        }
        $valorGLP = $valorGLP * $total;

        return $valorGLP;
    }
    
    public function totalPagoDieselPeriodo($dataInicial, $dataFinal) {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $modelo = new modeloApontamentoDiesel();
            $dados = $modelo->dielselConsumoPeriodoTotal($dataInicial, $dataFinal);

            $dataIni = str_replace("-", "", $dataInicial);
            $dataFin = str_replace("-", "", $dataFinal);

            $modelo = new modeloProtheus();
            $retorno = $modelo->totalGastoPorProduto($dataIni, $dataFin, '00100158', '1119501', '1119501');
            $valorDiesel = 0;
            if ($retorno->VALORTOTAL) {
                $valorDiesel = $retorno->VALORTOTAL / $retorno->QUANTIDADE;
            } else {
                $retorno = $modelo->totalGastoPorProdutoHistorico('00100158', '1119501', '1119501');
                $valorDiesel = $retorno->VALORTOTAL / $retorno->QUANTIDADE;
            }
            $valorDiesel = round($valorDiesel, 2);

            $total = 0;

            foreach ($dados as $dado) {

                $total = $total + $dado->consumo;
            }

            $valorDiesel = $valorDiesel * $total;
            #retorna Valor total do Diesel
            return $valorDiesel;
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }

}
