<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace SCM\controllers;

/**
 * Description of controleConsumoLocal
 *
 * @author henrique.guedes
 */
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use SCM\util\sessao;
use SCM\entity\consumoLocal;
use SCM\models\modeloLocalEnergia;
use SCM\models\modeloApontamentoConsumoEnergiaLocal;

class controleConsumoLocal {

    private $twig;
    private $request;
    private $sessao;
    private $raiz = '/scm/public_html/';

    function __construct(Response $response, \Twig_Environment $twig, \Symfony\Component\HttpFoundation\Request $request, sessao $sessao) {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

    public function redireciona($destino) {
        $redirect = new RedirectResponse($destino);
        $redirect->send();
    }

    public function apontamentoConsumoLocais() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $modelo = new modeloLocalEnergia();
            $locais = $modelo->todosLocaisAtivados();
            return $this->response->setContent($this->twig->render('apontamento/apontamentoConsumoLocal.html.twig', array('user' => $usuario, 'locais' => $locais)));
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }

    public function cadastrarApontamentoConsumoLocal() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $consumoLocal = new consumoLocal();

            $consumoLocal->setIdSCMLocalEnergia($this->request->get('idLocal'));
            $consumoLocal->setIdCriador($usuario->idUsuario);
            $consumoLocal->setConsumoTotal($this->request->get('consumo'));
            $consumoLocal->setDataInicio($this->request->get('dataInicial'));
            $consumoLocal->setDataTermino($this->request->get('dataFinal'));

            $diferenca = strtotime($consumoLocal->getDataTermino()) - strtotime($consumoLocal->getDataInicio());
            $consumoLocal->setTotalDias(floor($diferenca / (60 * 60 * 24)));
            $consumoLocal->setConsumoMedio(($consumoLocal->getConsumoTotal()) / ($consumoLocal->getTotalDias() + 1));
            //  print_r($consumoLocal);

            $modelo = new modeloApontamentoConsumoEnergiaLocal();
            $retorno = $modelo->cadastrar($consumoLocal);
            if ($retorno == 1) {
                echo 1;
            } else {
                echo $retorno;
            }
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }

    public function apontamentoConsumoEnergia($idLocalConsumo) {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {

            $modelo = new modeloApontamentoConsumoEnergiaLocal();
            $apontamentos = $modelo->todosApontamentos($idLocalConsumo);

            $retorno = json_encode($apontamentos);
            echo $retorno;
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }

    public function desativarApontamentoConsumoEnergia() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $id = $this->request->get('id');
            $modelo = new modeloApontamentoConsumoEnergiaLocal();
            $retorno = $modelo->desativar($id);
            if ($retorno == 1) {
                echo 1;
            } else {
                echo $retorno;
            }
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }

}
