<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace SCM\controllers;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use SCM\util\sessao;
use SCM\models\modeloConfiguracoes;

class controleConfiguracoes {

    private $response;
    private $twig;
    private $request;
    private $sessao;
    private $raiz = '/scm/public_html/';

    function __construct(Response $response, \Twig_Environment $twig, \Symfony\Component\HttpFoundation\Request $request, sessao $sessao) {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

    public function configuracoes() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $modelo = new modeloConfiguracoes();
$dados = $modelo->safraAtual();
            return $this->response->setContent($this->twig->render('configuracoes/configuracoes.html.twig', array('user' => $usuario,'dados'=>$dados)));
        } else {
            $this->redireciona($this->raiz . 'login');
        }
    }

    public function atualizarConfiguracoes() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $dataInicioSafra = $this->request->get('dataInicioSafra');
            $dataTerminoSafra = $this->request->get('dataTerminoSafra');
            $modelo = new modeloConfiguracoes();
            $retorno = $modelo->atualizarConfiguracoes($dataInicioSafra, $dataTerminoSafra);
            if ($retorno == 1) {
                echo 1;
            } else {
                echo $retorno;
            }
        } else {
            $this->redireciona($this->raiz . 'login');
        }
    }

     public function redireciona($destino) {
        $redirect = new RedirectResponse($destino);
        $redirect->send();
    }
}
