<?php

namespace SCM\controllers;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use SCM\util\sessao;
use SCM\models\modeloUsuario;
use SCM\models\modeloModulo;
use SCM\entity\usuario;

class controleUsuario {

    private $response;
    private $twig;
    private $request;
    private $sessao;
    private $raiz = '/scm/public_html/';

    function __construct(Response $response, \Twig_Environment $twig, \Symfony\Component\HttpFoundation\Request $request, sessao $sessao) {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

    public function redireciona($destino) {
        $redirect = new RedirectResponse($destino);
        $redirect->send();
    }

    public function validaLogin() {
        $modulo =$this->request->get('modulo');
        $usuario = $this->request->get('usuario');
        $senha = $this->request->get('senha');
        $modelo = new modeloUsuario();
        //$retorno = $modelo->validaLogin($usuario, $senha);
        $validaUsuario = $modelo->validaUsuario($usuario);
        if ($validaUsuario) {
            $usuarioAtivo = $modelo->usuarioAtivo($usuario);
            if ($usuarioAtivo) {
                $valida = $modelo->validaSenha($usuario, $senha);
                if ($valida) {
                    $modelo = new modeloModulo();
                    $moduloAtivo = $modelo->verificaAtivado($modulo);
                    if ($moduloAtivo) {

                        $modelo = new modeloUsuario();
                        $acesso = $modelo->validaLogin($usuario, $senha, $modulo);
                        if ($acesso) {
                            $this->sessao->add("userSCM", $acesso);
                            echo 1;
                        } else {
                            echo 14;
                        }
                    } else {
                        echo 13;
                    }
                } else {
                    echo 12;
                }
            } else {
                echo 11;
            }
        } else {
            echo 10;
        }
    }

    public function removerUsuario() {
        if ($this->sessao->get("userSCM")) {
            $this->sessao->remove('userSCM');
            $this->sessao->delete('userSCM');
            $this->redireciona($this->raiz);
        } else {
            $this->redireciona($this->raiz."login");
        }
    }
    
      public function paginaLogin() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            
            $this->redireciona('/scm/public_html/');
        } else {
            //return $this->response->setContent($this->twig->render('login/paginaLogin.html.twig'));
            $this->redireciona('/sim/');
        }
    }

}
