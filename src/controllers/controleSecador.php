<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace SCM\controllers;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use SCM\util\sessao;
use SCM\entity\secador;
use SCM\models\modeloSecador;

class controleSecador {

    private $twig;
    private $request;
    private $sessao;
    private $raiz = '/scm/public_html/';

    function __construct(Response $response, \Twig_Environment $twig, \Symfony\Component\HttpFoundation\Request $request, sessao $sessao) {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

    public function redireciona($destino) {
        $redirect = new RedirectResponse($destino);
        $redirect->send();
    }

    public function paginacontroleSecadores() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $modelo = new modeloSecador();
            $secadores = $modelo->todosSecadores();
            return $this->response->setContent($this->twig->render('secador/secador.html.twig', array('user' => $usuario, 'secadores'=>$secadores)));
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }
    
    public function cadastrarSecador(){
          $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $secador = new secador();
            $secador->setLocal($this->request->get('local'));
            $secador->setDescricao($this->request->get('descricao'));
            $modelo =  new modeloSecador();
            $retorno = $modelo->cadastrar($secador);
            if($retorno == 1){
                echo 1;
            }else{
                echo $retorno;
            }
        } else {
            $this->redireciona('/scm/public_html/login');
        }
        
    }
    
    public function editarSecador(){
          $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $secador = new secador();
            $secador->setLocal($this->request->get('local'));
            $secador->setDescricao($this->request->get('descricao'));
            $secador->setIdSCMSecador($this->request->get('id'));
            $modelo =  new modeloSecador();
            $retorno = $modelo->editar($secador);
            if($retorno == 1){
                echo 1;
            }else{
                echo $retorno;
            }
        } else {
            $this->redireciona('/scm/public_html/login');
        }
        
    }
    
    public function desativarSecador(){
          $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $secador = new secador();
            $secador->setIdSCMSecador($this->request->get('id'));
            $modelo =  new modeloSecador();
            $retorno = $modelo->desativarSecador($secador);
            if($retorno == 1){
                echo 1;
            }else{
                echo $retorno;
            }
        } else {
            $this->redireciona('/scm/public_html/login');
        }
        
    }
    
    public function ativarSecador(){
          $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $secador = new secador();
            $secador->setIdSCMSecador($this->request->get('id'));
            $modelo =  new modeloSecador();
            $retorno = $modelo->ativarSecador($secador);
            if($retorno == 1){
                echo 1;
            }else{
                echo $retorno;
            }
        } else {
            $this->redireciona('/scm/public_html/login');
        }
        
    }

}
