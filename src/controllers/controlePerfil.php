<?php

namespace SCM\controllers;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use SCM\util\sessao;
use SCM\models\modeloPerfil;
use SCM\entity\usuario;

class ControlePerfil {

    private $response;
    private $twig;
    private $request;
    private $sessao;

    function __construct(Response $response, \Twig_Environment $twig, \Symfony\Component\HttpFoundation\Request $request, Sessao $sessao) {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

    public function atualizarPerfil() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $user = new usuario();
            $modelo = new modeloPerfil();
            $user->setEmail($this->request->get('email'));
            $user->setFuncao($this->request->get('funcao'));
            $user->setIdUsuario($this->request->get('id'));
            $user->setNome($this->request->get('nome'));
            $user->setSenha($this->request->get('senha'));
            $user->setUsuario($this->request->get('usuario'));
            $user->setIdUsuario($usuario->idUsuario);

            $verificacao = $modelo->verificaPerfil($user->getIdUsuario(), $user->getUsuario());

            if ($verificacao) {
                echo 1;
            } else {
                $modelo->atualizar($user);
                $usuario->nome = $user->getNome();
                $usuario->usuario = $user->getUsuario();
                $usuario->funcao = $user->getFuncao();
                $this->sessao->alter('userSCM', $usuario);

                echo 0;
            }
        } else {
            $this->redireciona('/scm/public_html/');
        }
    }

    public function paginaPerfil() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $modelo = new modeloPerfil();
            $dados = $modelo->buscaPerfil($usuario->idUsuario);
//return $this->response->setContent($this->twig->render('Dashboard.html.twig', array('dados' => $dados, 'user' => $usuario)));
            return $this->response->setContent($this->twig->render('perfil/perfil.html.twig', array('user' => $usuario, 'dados' => $dados)));
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }
      public function redireciona($destino) {
        $redirect = new RedirectResponse($destino);
        $redirect->send();
    }
    

}
