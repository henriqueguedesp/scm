<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace SCM\controllers;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use SCM\util\sessao;
use SCM\entity\gerador;
use SCM\models\modeloGerador;

class controleGerador {

    private $twig;
    private $request;
    private $sessao;
    private $raiz = '/scm/public_html/';

    function __construct(Response $response, \Twig_Environment $twig, \Symfony\Component\HttpFoundation\Request $request, sessao $sessao) {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

    public function paginaControleGeradores() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $modelo = new modeloGerador();
            $geradores =  $modelo->todosGeradores();
            return $this->response->setContent($this->twig->render('gerador/gerador.html.twig', array('user' => $usuario, 'geradores'=> $geradores)));
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }

    public function cadastrarGerador() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $gerador = new gerador();
            $gerador->setLocal($this->request->get('local'));
            $gerador->setDescricao($this->request->get('descricao'));
            $modelo = new modeloGerador();
            $retorno = $modelo->cadastrar($gerador);
            if ($retorno == 1) {
                echo 1;
            } else {
                echo $retorno;
            }
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }
    
    public function editarGerador() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $gerador = new gerador();
            $gerador->setLocal($this->request->get('local'));
            $gerador->setDescricao($this->request->get('descricao'));
            $gerador->setIdSCMGerador($this->request->get('id'));
            $modelo = new modeloGerador();
            $retorno = $modelo->editar($gerador);
            if ($retorno == 1) {
                echo 1;
            } else {
                echo $retorno;
            }
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }
    
    public function desativarGerador() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $gerador = new gerador();
            $gerador->setIdSCMGerador($this->request->get('id'));
            $modelo = new modeloGerador();
            $retorno = $modelo->desativarGerador($gerador);
            if ($retorno == 1) {
                echo 1;
            } else {
                echo $retorno;
            }
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }
    public function ativarGerador() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $gerador = new gerador();
            $gerador->setIdSCMGerador($this->request->get('id'));
            $modelo = new modeloGerador();
            $retorno = $modelo->ativarGerador($gerador);
            if ($retorno == 1) {
                echo 1;
            } else {
                echo $retorno;
            }
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }

    public function redireciona($destino) {
        $redirect = new RedirectResponse($destino);
        $redirect->send();
    }

}
