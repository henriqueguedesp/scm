<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace SCM\controllers;

/**
 * Description of controleLocalEnergia
 *
 * @author henrique.guedes
 */
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use SCM\util\sessao;
use SCM\entity\localEnergia;
use SCM\models\modeloLocalEnergia;



class controleLocalEnergia {

    private $twig;
    private $request;
    private $sessao;
    private $raiz = '/scm/public_html/';

    function __construct(Response $response, \Twig_Environment $twig, \Symfony\Component\HttpFoundation\Request $request, sessao $sessao) {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

    public function redireciona($destino) {
        $redirect = new RedirectResponse($destino);
        $redirect->send();
    }

    ##LOCAIS CONSUMO DE ENERGIA

    public function locaisEnergia() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $modelo = new modeloLocalEnergia();
            $locais = $modelo->todosLocais();
            return $this->response->setContent($this->twig->render('localEnergia/locaisEnergia.html.twig', array('user' => $usuario,'locais'=>$locais)));
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }

    public function casdastroLocal() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $localEnergia = new localEnergia();
            $localEnergia->setLocal($this->request->get('local'));
            $localEnergia->setDescricao($this->request->get('descricao'));
            $localEnergia->setIdCriador($usuario->idUsuario);
            $modelo = new modeloLocalEnergia();
            $retorno = $modelo->cadastrar($localEnergia);
            if ($retorno == 1) {
                echo 1;
            } else {
                echo $retorno;
            }
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }
    
    public function editarLocal() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $localEnergia = new localEnergia();
            $localEnergia->setLocal($this->request->get('local'));
            $localEnergia->setDescricao($this->request->get('descricao'));
            $localEnergia->setIdLocalEnergia($this->request->get('id'));
            $modelo = new modeloLocalEnergia();
            $retorno = $modelo->editar($localEnergia);
            if ($retorno == 1) {
                echo 1;
            } else {
                echo $retorno;
            }
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }
    public function desativarLocal() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $localEnergia = new localEnergia();
            $localEnergia->setIdLocalEnergia($this->request->get('id'));
            $modelo = new modeloLocalEnergia();
            $retorno = $modelo->desativarLocal($localEnergia);
            if ($retorno == 1) {
                echo 1;
            } else {
                echo $retorno;
            }
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }
    
    public function ativarLocal() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $localEnergia = new localEnergia();
            $localEnergia->setIdLocalEnergia($this->request->get('id'));
            $modelo = new modeloLocalEnergia();
            $retorno = $modelo->ativarLocal($localEnergia);
            if ($retorno == 1) {
                echo 1;
            } else {
                echo $retorno;
            }
        } else {
            $this->redireciona('/scm/public_html/login');
        }
    }
   
    
   
}
