<?php

namespace SCM\controllers;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use SCM\util\sessao;
use SCM\models\modeloProtheus;
use SCM\models\modeloConfiguracoes;
use SCM\models\modeloApontamentoConsumoCemig;
use SCM\models\modeloSecador;
use SCM\models\modeloApontamentoGLP;
use SCM\models\modeloApontamentoDiesel;
use SCM\util\energia;
use SCM\models\modeloApontamentoConsumoEnergiaLocal;
use SCM\models\modeloApontamentoProducao;

class controleIndex {

    private $response;
    private $twig;
    private $request;
    private $sessao;
    private $raiz = '/scm/public_html/';

    function __construct(Response $response, \Twig_Environment $twig, \Symfony\Component\HttpFoundation\Request $request, sessao $sessao) {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

    public function dashboard() {
        $usuario = $this->sessao->get('userSCM');
        if ($usuario) {
            $custoTotal = 0;
            $modelo = new modeloConfiguracoes();
            $dados = $modelo->safraAtual();
            $dataInicioSafraDB = $dados->dataInicioSafra;
            $dataTerminoSafraDB = $dados->dataTerminoSafra;
            $dados->dataInicioSafra = str_replace("-", "", $dados->dataInicioSafra);
            $dados->dataTerminoSafra = str_replace("-", "", $dados->dataTerminoSafra);
            #CUSTO SECADORES
            $custoGLPSecador01 = ($this->consumoGLPPorSecador(1, $dataInicioSafraDB, $dataTerminoSafraDB, '1110401', '1110401')) / $this->totalMaterialRecPorLinha($dados->dataInicioSafra, $dados->dataTerminoSafra, 1);
            $custoGLPSecador02 = ($this->consumoGLPPorSecador(2, $dataInicioSafraDB, $dataTerminoSafraDB, '1110402', '1110402')) / $this->totalMaterialRecPorLinha($dados->dataInicioSafra, $dados->dataTerminoSafra, 2);
            $custoGLPSecador03 = ($this->consumoGLPPorSecador(3, $dataInicioSafraDB, $dataTerminoSafraDB, '1110403', '1110403')) / $this->totalMaterialRecPorLinha($dados->dataInicioSafra, $dados->dataTerminoSafra, 3);
            $custoGLPSecador04 = ($this->consumoGLPPorSecador(4, $dataInicioSafraDB, $dataTerminoSafraDB, '1110404', '1110404')) / $this->totalMaterialRecPorLinha($dados->dataInicioSafra, $dados->dataTerminoSafra, 4);
//            $custoGLPSecadorTotal = $custoGLPSecador01 + $custoGLPSecador02 + $custoGLPSecador03 + $custoGLPSecador04;
            $custoGLPSecadorTotal = ($this->consumoGLPPorSecador(1, $dataInicioSafraDB, $dataTerminoSafraDB, '1110401', '1110401')) + ($this->consumoGLPPorSecador(2, $dataInicioSafraDB, $dataTerminoSafraDB, '1110402', '1110402')) + ($this->consumoGLPPorSecador(3, $dataInicioSafraDB, $dataTerminoSafraDB, '1110403', '1110403')) + ($this->consumoGLPPorSecador(4, $dataInicioSafraDB, $dataTerminoSafraDB, '1110404', '1110404'));
            $custoGLPSecador01 = number_format($custoGLPSecador01, 2, ",", ".");
            $custoGLPSecador02 = number_format($custoGLPSecador02, 2, ",", ".");
            $custoGLPSecador03 = number_format($custoGLPSecador03, 2, ",", ".");
            $custoGLPSecador04 = number_format($custoGLPSecador04, 2, ",", ".");
            $custoGLPSecadorTotal = number_format($custoGLPSecadorTotal, 2, ",", ".");

            $custoGLPSecador = array('custoGLPSecador01' => $custoGLPSecador01, 'custoGLPSecador02' => $custoGLPSecador02, 'custoGLPSecador03' => $custoGLPSecador03,
                'custoGLPSecador04' => $custoGLPSecador04, 'custoGLPSecadorTotal' => $custoGLPSecadorTotal);

            $modelo = new modeloProtheus();
            $materialRecebido = $modelo->totalMatRecTotal($dados->dataInicioSafra, $dados->dataTerminoSafra);

            #DEBULHA
            $custoCCDebulha = $modelo->valorTotalGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1110501', '1110501');
            $custoCCDebulha = number_format($custoCCDebulha->TOTAL, 2, ",", ".");


            #CUSTOS DAS LINHAS DE RECEBIMENTO
            $custo01 = $this->calculaCusto($dados->dataInicioSafra, $dados->dataTerminoSafra, '1110101', 1);
            $custo02 = $this->calculaCusto($dados->dataInicioSafra, $dados->dataTerminoSafra, '1110102', 2);
            $custo03 = $this->calculaCusto($dados->dataInicioSafra, $dados->dataTerminoSafra, '1110103', 3);
            $custo04 = $this->calculaCusto($dados->dataInicioSafra, $dados->dataTerminoSafra, '1110104', 4);
            $custoLinha01 = $custo01;
            $custoLinha02 = $custo02;
            $custoLinha03 = $custo03;
            $custoLinha04 = $custo04;
            #CUSTOS DAS DESPALHAS
            $custoDespalha01 = $this->calculaCusto($dados->dataInicioSafra, $dados->dataTerminoSafra, '1110201', 1);
            $custoDespalha02 = $this->calculaCusto($dados->dataInicioSafra, $dados->dataTerminoSafra, '1110202', 2);
            $custoDespalha03 = $this->calculaCusto($dados->dataInicioSafra, $dados->dataTerminoSafra, '1110203', 3);
            $custoDespalha04 = $this->calculaCusto($dados->dataInicioSafra, $dados->dataTerminoSafra, '1110204', 4);
            #CUSTOS DOS SECADORES
            $custoSecador01 = $this->calculaCusto($dados->dataInicioSafra, $dados->dataTerminoSafra, '1110401', 1);
            $custoSecador02 = $this->calculaCusto($dados->dataInicioSafra, $dados->dataTerminoSafra, '1110402', 2);
            $custoSecador03 = $this->calculaCusto($dados->dataInicioSafra, $dados->dataTerminoSafra, '1110403', 3);
            $custoSecador04 = $this->calculaCusto($dados->dataInicioSafra, $dados->dataTerminoSafra, '1110404', 4);
            #COLOCANDO TUDO NO ARRAY PARA FACILITAR A PASSAGEM DOS DADOS PARA A VIEW

            $custoLinhaDespalhaSecador01 = ($custoLinha01 + $custoDespalha01 + $custoSecador01);
            $custoLinhaDespalhaSecador02 = ($custoLinha02 + $custoDespalha02 + $custoSecador02);
            $custoLinhaDespalhaSecador03 = ($custoLinha03 + $custoDespalha03 + $custoSecador03);
            $custoLinhaDespalhaSecador04 = ($custoLinha04 + $custoDespalha04 + $custoSecador04);


            $custoLinhaDespalhaSecadorAbsoluto = $custoLinhaDespalhaSecador01 + $custoLinhaDespalhaSecador02 + $custoLinhaDespalhaSecador03 + $custoLinhaDespalhaSecador04;
            $custoLinhaDespalhaSecador01 = round(($custoLinha01 + $custoDespalha01 + $custoSecador01) / $this->totalMaterialRecPorLinha($dados->dataInicioSafra, $dados->dataTerminoSafra, 1), 2);
            $custoLinhaDespalhaSecador02 = round(($custoLinha02 + $custoDespalha02 + $custoSecador02) / $this->totalMaterialRecPorLinha($dados->dataInicioSafra, $dados->dataTerminoSafra, 2), 2);
            $custoLinhaDespalhaSecador03 = round(($custoLinha03 + $custoDespalha03 + $custoSecador03) / $this->totalMaterialRecPorLinha($dados->dataInicioSafra, $dados->dataTerminoSafra, 3), 2);
            $custoLinhaDespalhaSecador04 = round(($custoLinha04 + $custoDespalha04 + $custoSecador04) / $this->totalMaterialRecPorLinha($dados->dataInicioSafra, $dados->dataTerminoSafra, 4), 2);
            $aux = number_format($custoLinhaDespalhaSecadorAbsoluto, 2, ",", ".");

//            $custoLinhaDespalhaSecadorAbsoluto = $custoLinhaDespalhaSecador01 + $custoLinhaDespalhaSecador02 + $custoLinhaDespalhaSecador03 + $custoLinhaDespalhaSecador04;
            $custoLinhaDespalhaSecador01 = str_replace('.', ',', $custoLinhaDespalhaSecador01);
            $custoLinhaDespalhaSecador02 = str_replace('.', ',', $custoLinhaDespalhaSecador02);
            $custoLinhaDespalhaSecador03 = str_replace('.', ',', $custoLinhaDespalhaSecador03);
            $custoLinhaDespalhaSecador04 = str_replace('.', ',', $custoLinhaDespalhaSecador04);
            $custoLinhaDespalhaSecadorAbsoluto = str_replace('.', ',', $custoLinhaDespalhaSecadorAbsoluto);
            $custoLinhaDespalhaSecador = array('custoLinhaDespalhaSecador01' => $custoLinhaDespalhaSecador01, 'custoLinhaDespalhaSecador02' => $custoLinhaDespalhaSecador02,
                'custoLinhaDespalhaSecador03' => $custoLinhaDespalhaSecador03, 'custoLinhaDespalhaSecador04' => $custoLinhaDespalhaSecador04, 'custoLinhaDespalhaSecadorAbsoluto' => $custoLinhaDespalhaSecadorAbsoluto);

            #CUSTO DESPALHA
            $custoCCDespalha = $modelo->valorTotalGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1110501', '1110501');
            $custoDespalhaMaterialRecebido = $custoCCDespalha->TOTAL / $materialRecebido->MATERIAL;
            $custoDespalhaMaterialRecebido = number_format($custoDespalhaMaterialRecebido, 2, ",", ".");
            $custoCCDespalha = number_format($custoCCDespalha->TOTAL, 2, ",", ".");
            $custosDespalha = array('custoDespalhaMaterialRecebido' => $custoDespalhaMaterialRecebido, 'custoCCDespalha' => $custoCCDespalha);

            #CUSTO SILOS DE ARMAZENAGEM
            $custoCCSilosArmazenagem = $modelo->valorTotalGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1110601', '1110601');
            $custoSilosArmazenagemMaterialRecebido = $custoCCSilosArmazenagem->TOTAL / $materialRecebido->MATERIAL;
            $custoSilosArmazenagemMaterialRecebido = number_format($custoSilosArmazenagemMaterialRecebido, 2, ",", ".");
            $custoCCSilosArmazenagem = number_format($custoCCSilosArmazenagem->TOTAL, 2, ",", ".");
            $custoSilosArmazenagem = array('custoCCSilosArmazenagem' => $custoCCSilosArmazenagem, 'custoSilosArmazenagemMaterialRecebido' => $custoSilosArmazenagemMaterialRecebido);

            #CUSTO DESCARTE
            //inclui CC do descarte 01 e descarte 02
            $custoCCDescarte = $modelo->valorTotalGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1110301', '1110302');
            $custoCCDescarte = $custoCCDescarte->TOTAL;
            $custoDescarteMaterialRecebido = $custoCCDescarte / $materialRecebido->MATERIAL;
            $custoDescarteMaterialRecebido = number_format($custoDescarteMaterialRecebido, 2, ",", ".");
            $custoCCDescarte = number_format($custoCCDescarte, 2, ",", ".");
            $custoDescarte = array('custoCCDescarte' => $custoCCDescarte, 'custoDescarteMaterialRecebido' => $custoDescarteMaterialRecebido);

            #CUSTO ARMAZÉNS + UNIDADE CONDESADORAS
            $custoCCArmazens = $modelo->valorTotalGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1130101', '1130108');
            $custoCCArmazens = $custoCCArmazens->TOTAL;
            $custoCCUnidadeCondensadoras = $modelo->valorTotalGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1130202', '1130202');
            $custoCCUnidadeCondensadoras = $custoCCUnidadeCondensadoras->TOTAL;
            $custoCCArmazensUnidadeCondensadoras = $custoCCArmazens + $custoCCUnidadeCondensadoras;
            $custoCCArmazensUnidadeCondensadoras = number_format($custoCCArmazensUnidadeCondensadoras, 2, ",", ".");

            #CUSTO EMPILHADEIRAS
            $custoCCEmpilhadeiras = $modelo->valorTotalGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1130201', '1130201');
            $custoCCEmpilhadeiras = $custoCCEmpilhadeiras->TOTAL;
            $custoCCEmpilhadeiras = number_format($custoCCEmpilhadeiras, 2, ",", ".");

            #CUSTO COMPRESSOR TORRE + SISTEMA DE AR COMPRIMIDO TORRE + CASA DE COMPRESSORES
            $custoCCCasaCompressorTorre = $modelo->valorTotalGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1120701', '1120701');
            $custoCCCasaCompressorTorre = $custoCCCasaCompressorTorre->TOTAL;
            $custoCCSistemaArComprimidoTorre = $modelo->valorTotalGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1129901', '1129901');
            $custoCCSistemaArComprimidoTorre = $custoCCSistemaArComprimidoTorre->TOTAL;
            $custoCCCasaCompressor = $modelo->valorTotalGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1111001', '1111002');
            $custoCCCasaCompressor = $custoCCCasaCompressor->TOTAL;
            $custoCasaCompressorCasaCompressorTorreSistemaArComprimidoTorre = $custoCCCasaCompressorTorre + $custoCCSistemaArComprimidoTorre + $custoCCCasaCompressor;
            $custoCasaCompressorCasaCompressorTorreSistemaArComprimidoTorre = number_format($custoCasaCompressorCasaCompressorTorreSistemaArComprimidoTorre, 2, ",", ".");

            //CUSTO TOTAL
            $ct01 = str_replace('.', '', $custoLinhaDespalhaSecadorAbsoluto);
            $ct01 = str_replace(',', '.', $ct01);
            $ct02 = str_replace('.', '', $custoCCDespalha);
            $ct02 = str_replace(',', '.', $ct02);
            $ct03 = str_replace('.', '', $custoCCSilosArmazenagem);
            $ct03 = str_replace(',', '.', $ct03);
            $ct04 = str_replace('.', '', $custoCCDescarte);
            $ct04 = str_replace(',', '.', $ct04);
            $ct05 = str_replace('.', '', $custoCCArmazensUnidadeCondensadoras);
            $ct05 = str_replace(',', '.', $ct05);
            $ct06 = str_replace('.', '', $custoCCEmpilhadeiras);
            $ct06 = str_replace(',', '.', $ct06);
            $ct07 = str_replace('.', '', $custoCasaCompressorCasaCompressorTorreSistemaArComprimidoTorre);
            $ct07 = str_replace(',', '.', $ct07);
            $custoTotal = $ct01 + $ct02 + $ct03 + $ct04 + $ct05 + $ct06 + $ct07;
            $custoTotal = number_format($custoTotal, 2, ",", ".");

            $custoLinhaDespalhaSecadorAbsoluto = $aux;


            ##custo energia por tonelada recebida
//            $modelo = new modeloProtheus();
            
            $retorno = $modelo->totalGastoPorProduto($dados->dataInicioSafra, $dados->dataTerminoSafra, '00100158', '1119501', '1119501');
            $modelo = new modeloApontamentoDiesel();
            $dadosConsumo = $modelo->dielselConsumoPeriodoTotalFinal($dataInicioSafraDB, $dataTerminoSafraDB);
            $valorDiesel = 0;
            if ($retorno->VALORTOTAL) {
                $valorDiesel = $retorno->VALORTOTAL / $retorno->QUANTIDADE;
            } else {
                $retorno = $modelo->totalGastoPorProdutoHistorico('00100158', '1119501', '1119501');
                $valorDiesel = $retorno->VALORTOTAL / $retorno->QUANTIDADE;
            }
            //$materialRecebido = $modelo->totalMatRecTotal($dados->dataInicioSafra, $dados->dataTerminoSafra);
            $modelo = new modeloApontamentoConsumoCemig();
            $dataInicial = date("Y-m", strtotime($dados->dataInicioSafra));
            $dataFinal = date("Y-m", strtotime($dados->dataTerminoSafra));
            $cemig = $modelo->energiaCemigPeriodoTotalSomatorio($dataInicial, $dataFinal);
            $custoEnergiaToneladaRecebida = (($valorDiesel*$dadosConsumo->consumo) + $cemig->totalCemig) / $materialRecebido->MATERIAL;

            ##custo GLP por toneladaRecebida
            $modelo = new modeloProtheus();
            $retorno = $modelo->totalGastoPorProduto($dados->dataInicioSafra, $dados->dataTerminoSafra, '00100142', '11104001', '1110404');
            $valorGLP = 0;
            $totalConsumo = 0;
            $modelo = new modeloApontamentoGLP();
            $dadosConsumo = $modelo->glpConsumoPeriodoTotal($dataInicioSafraDB, $dataTerminoSafraDB);
            foreach ($dadosConsumo as $dado) {
                $totalConsumo = $totalConsumo + $dado->consumo;
            }
            if ($retorno->VALORTOTAL) {
                $valorGLP = $retorno->VALORTOTAL / $retorno->QUANTIDADE;
            } else {
                $retorno = $modelo->totalGastoPorProdutoHistorico('00100142', '11104001', '1110404');
                $valorGLP = $retorno->VALORTOTAL / $retorno->QUANTIDADE;
            }
            $custoGPLPorToneladaRecebida = ($valorGLP * $totalConsumo) / $materialRecebido->MATERIAL;


##ENERGIA LINHAS
            $modelo = new modeloApontamentoConsumoEnergiaLocal();
            $informacoesKHW = new energia();
            $valorMedioKWH = $informacoesKHW->valorMedioKWH();

            $custoEnergiaLinha01 = $modelo->consumoTotalEnergiaLocal(1, $dados->dataInicioSafra, $dados->dataTerminoSafra);
            $custoEnergiaLinha02 = $modelo->consumoTotalEnergiaLocal(2, $dados->dataInicioSafra, $dados->dataTerminoSafra);
            $custoEnergiaLinha03 = $modelo->consumoTotalEnergiaLocal(3, $dados->dataInicioSafra, $dados->dataTerminoSafra);
            $custoEnergiaLinha04 = $modelo->consumoTotalEnergiaLocal(4, $dados->dataInicioSafra, $dados->dataTerminoSafra);

            $custoEnergiaLinha01 = ($custoEnergiaLinha01->consumo * $valorMedioKWH) / $this->totalMaterialRecPorLinha($dados->dataInicioSafra, $dados->dataTerminoSafra, 1);
            $custoEnergiaLinha02 = ($custoEnergiaLinha02->consumo * $valorMedioKWH) / $this->totalMaterialRecPorLinha($dados->dataInicioSafra, $dados->dataTerminoSafra, 2);
            $custoEnergiaLinha03 = ($custoEnergiaLinha03->consumo * $valorMedioKWH) / $this->totalMaterialRecPorLinha($dados->dataInicioSafra, $dados->dataTerminoSafra, 3);
            $custoEnergiaLinha04 = ($custoEnergiaLinha04->consumo * $valorMedioKWH) / $this->totalMaterialRecPorLinha($dados->dataInicioSafra, $dados->dataTerminoSafra, 4);
            $custoTotalLinhas = $custoEnergiaLinha01 + $custoEnergiaLinha02 + $custoEnergiaLinha03 + $custoEnergiaLinha04;
            $custoEnergiaLinha01 = number_format($custoEnergiaLinha01, 2, ",", ".");
            $custoEnergiaLinha02 = number_format($custoEnergiaLinha02, 2, ",", ".");
            $custoEnergiaLinha03 = number_format($custoEnergiaLinha03, 2, ",", ".");
            $custoEnergiaLinha04 = number_format($custoEnergiaLinha04, 2, ",", ".");
            
$custoEnergiaToneladaRecebida = $custoTotalLinhas;

            $custoEnergiaLinhas = array('custoEnergiaLinha01' => $custoEnergiaLinha01, 'custoEnergiaLinha02' => $custoEnergiaLinha02,
                'custoEnergiaLinha03' => $custoEnergiaLinha03, 'custoEnergiaLinha04' => $custoEnergiaLinha04);

            //CUSTOS ARMAZÉNS
            $modelo = new modeloApontamentoConsumoEnergiaLocal();
            $informacoesKHW = new energia();
            $valorMedioKWH = $informacoesKHW->valorMedioKWH();

            $custoArmazen01 = $modelo->consumoTotalEnergiaLocal(10, $dados->dataInicioSafra, $dados->dataTerminoSafra);
            $custoArmazen02 = $modelo->consumoTotalEnergiaLocal(6, $dados->dataInicioSafra, $dados->dataTerminoSafra);
            $custoArmazen03 = $modelo->consumoTotalEnergiaLocal(7, $dados->dataInicioSafra, $dados->dataTerminoSafra);
            $custoArmazen04 = $modelo->consumoTotalEnergiaLocal(8, $dados->dataInicioSafra, $dados->dataTerminoSafra);
            $custoArmazen05 = $modelo->consumoTotalEnergiaLocal(9, $dados->dataInicioSafra, $dados->dataTerminoSafra);
            $custoArmazen01 = $custoArmazen01->consumo * $valorMedioKWH;
            $custoArmazen02 = $custoArmazen02->consumo * $valorMedioKWH;
            $custoArmazen03 = $custoArmazen03->consumo * $valorMedioKWH;
            $custoArmazen04 = $custoArmazen04->consumo * $valorMedioKWH;
            $custoArmazen05 = $custoArmazen05->consumo * $valorMedioKWH;
            $custoEnergiaTotalArmazens = $custoArmazen01 + $custoArmazen02 + $custoArmazen03 + $custoArmazen04 + $custoArmazen05;

            $custoArmazen01 = number_format($custoArmazen01, 2, ",", ".");
            $custoArmazen02 = number_format($custoArmazen02, 2, ",", ".");
            $custoArmazen03 = number_format($custoArmazen03, 2, ",", ".");
            $custoArmazen04 = number_format($custoArmazen04, 2, ",", ".");
            $custoArmazen05 = number_format($custoArmazen05, 2, ",", ".");
            $custoEnergiaTotalArmazens = number_format($custoEnergiaTotalArmazens, 2, ",", ".");

            $custoEnergiaArmazens = array('custoArmazen01' => $custoArmazen01, 'custoArmazen02' => $custoArmazen02, 'custoArmazen03' => $custoArmazen03,
                'custoArmazen04' => $custoArmazen04, 'custoArmazen05' => $custoArmazen05, 'custoEnergiaTotalArmazens' => $custoEnergiaTotalArmazens);




            ##consumo total energia
            $energia = new energia();
            $valorMedioKWH = $informacoesKHW->valorMedioKWH();

            $energiaGerada = $energia->totalEnergiaGerada();
            $energiaCemig = $energia->totalEnergiaCemig();
            $energiaTotal = $energiaGerada + $energiaCemig;
            $energiaTotal = number_format(($energiaTotal * $valorMedioKWH), 2, ",", ".");

            ##TORRE DE CLASSIFICAÇÃO
            $modelo = new modeloProtheus();
            $custoTorreClassificacaoAbsoluto = $modelo->valorTotalGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1120201', '1120401');
            $modelo = new modeloApontamentoProducao();
            $producaoTorre = $modelo->apontamentosEspecificios(0, $dataInicioSafraDB, $dataTerminoSafraDB);
            $producaoTSI = $modelo->apontamentosEspecificios(1, $dataInicioSafraDB, $dataTerminoSafraDB);
            $producaoTorre = $producaoTorre->total;
            $producaoTSI = $producaoTSI->total;
            $custoTorreClassificacao = $custoTorreClassificacaoAbsoluto->TOTAL / $producaoTorre;


            //CUSTOS ENERGIA TORRE TRATAMENTO
            $modelo = new modeloApontamentoConsumoEnergiaLocal();
            $informacoesKHW = new energia();
            $valorMedioKWH = $informacoesKHW->valorMedioKWH();
            $custoEnergiaTorre = $modelo->consumoTotalEnergiaLocal(5, $dados->dataInicioSafra, $dados->dataTerminoSafra);
            $custoEnergiaTorre = $custoEnergiaTorre->consumo * $valorMedioKWH;
            $custoEnergiaTorre = $custoEnergiaTorre / $producaoTorre;
            $custoEnergiaTorre = number_format($custoEnergiaTorre, 2, ",", ".");
            $custoTorreClassificacao = number_format($custoTorreClassificacao, 2, ",", ".");
            $custoTorreClassificacaoAbsoluto = number_format($custoTorreClassificacaoAbsoluto->TOTAL, 2, ",", ".");

            ##Tsi
            $modelo = new modeloProtheus();
            $custoTSIAbsoluto = $modelo->valorTotalGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1121201', '1121201');
            $custoTSI = $custoTSIAbsoluto->TOTAL / $producaoTSI;
            //CUSTOS ENERGIA TORRE TRATAMENTO
            $modelo = new modeloApontamentoConsumoEnergiaLocal();
            $informacoesKHW = new energia();
            $valorMedioKWH = $informacoesKHW->valorMedioKWH();
            $custoEnergiaTSI = $modelo->consumoTotalEnergiaLocal(11, $dados->dataInicioSafra, $dados->dataTerminoSafra);
            $custoEnergiaTSI = $custoEnergiaTSI->consumo * $valorMedioKWH;
            $custoEnergiaTSI = $custoEnergiaTSI / $producaoTSI;
            $custoEnergiaTSI = number_format($custoEnergiaTSI, 2, ",", ".");
            $custoTSI = number_format($custoTSI, 2, ",", ".");
            $custoTSIAbsoluto = number_format($custoTSIAbsoluto->TOTAL, 2, ",", ".");

            return $this->response->setContent($this->twig->render('dashboard/dashboard.html.twig', array(
                                'user' => $usuario,
                                'custo01' => $custo01, 'custo02' => $custo02, 'custo03' => $custo03, 'custo04' => $custo04,
                                'custoEnergiaToneladaRecebida' => number_format($custoEnergiaToneladaRecebida, 2, ",", "."),
                                'custoGLPPorToneladaRecebida' => number_format($custoGPLPorToneladaRecebida, 2, ",", "."),
                                'totalMaterialRececbido' => number_format($materialRecebido->MATERIAL, 2, ",", "."),
                                'custoLinhaDespalhaSecador' => $custoLinhaDespalhaSecador,
                                'custosDespalha' => $custosDespalha,
                                'custoSilosArmazenagem' => $custoSilosArmazenagem,
                                'custoDescarte' => $custoDescarte,
                                'custoCCArmazensUnidadeCondensadoras' => $custoCCArmazensUnidadeCondensadoras,
                                'custoCCEmpilhadeiras' => $custoCCEmpilhadeiras,
                                'custoCasaCompressorCasaCompressorTorreSistemaArComprimidoTorre' => $custoCasaCompressorCasaCompressorTorreSistemaArComprimidoTorre,
                                'custoTotal' => $custoTotal,
                                'custoGLPSecador' => $custoGLPSecador,
                                'custoEnergiaLinhas' => $custoEnergiaLinhas,
                                'custoEnergiaArmazens' => $custoEnergiaArmazens,
                                'energiaTotal' => $energiaTotal,
                                'custoTorreClassificacaoAbsoluto' => $custoTorreClassificacaoAbsoluto,
                                'custoTorreClassificacao' => $custoTorreClassificacao,
                                'custoTSIAbsoluto' => $custoTSIAbsoluto,
                                'custoTSI' => $custoTSI,
                                'custoEnergiaTorre' => $custoEnergiaTorre,
                                'custoEnergiaTSI' => $custoEnergiaTSI,
                                'custoCCDebulha' => $custoCCDebulha
                                    )
            ));
            // print_r($custoLinhaDespalhaSecador);
        } else {
            $this->redireciona($this->raiz . 'login');
        }
    }

    public function calculaCusto($dataInicial, $dataFinal, $cc, $linha) {
        $modelo = new modeloProtheus();

        $valorGasto = $modelo->valorTotalGastoPorCC($dataInicial, $dataFinal, $cc, $cc);
        $val = $valorGasto->TOTAL;
        return round($val, 2);
    }

    public function calculaCustoTonelada($dataInicial, $dataFinal, $cc, $linha) {
        $modelo = new modeloProtheus();

        $valorGasto = $modelo->valorTotalGastoPorCC($dataInicial, $dataFinal, $cc, $cc);
        $materialRecebido = $modelo->totalMatRecPorLinha($dataInicial, $dataFinal, $linha);
        $val = $materialRecebido->MATERIAL / $valorGasto->TOTAL;
        return round($val, 2);
    }

    public function totalMaterialRecPorLinha($dataInicial, $dataFinal, $linha) {
        $modelo = new modeloProtheus();

        $materialRecebido = $modelo->totalMatRecPorLinha($dataInicial, $dataFinal, $linha);
        return $materialRecebido->MATERIAL;
    }

    public function redireciona($destino) {
        $redirect = new RedirectResponse($destino);
        $redirect->send();
    }

    public function consumoGLPPorSecador($secador, $dataInicial, $dataFinal, $ccInicial, $ccFinal) {

        //função retorna valor gasto com GLP por determinado perído e por determinado secador; 
        $modelo = new modeloSecador();
        $des = $modelo->secadorEspecifico($secador);

        $modelo = new modeloApontamentoGLP();
        $dados = $modelo->glpConsumoPeriodoTotalSecador($dataInicial, $dataFinal, $secador);

        $dataIni = str_replace("-", "", $dataInicial);
        $dataFin = str_replace("-", "", $dataFinal);

        $modelo = new modeloProtheus();
        $retorno = $modelo->totalGastoPorProduto($dataIni, $dataFin, '00100142', $ccInicial, $ccFinal);
        $valorGLP = 0;
        if ($retorno->VALORTOTAL) {
            $valorGLP = $retorno->VALORTOTAL / $retorno->QUANTIDADE;
        } else {
            $retorno = $modelo->totalGastoPorProdutoHistorico('00100142', $ccInicial, $ccFinal);
            $valorGLP = $retorno->VALORTOTAL / $retorno->QUANTIDADE;
        }
        $valorGLP = round($valorGLP, 2);
        $total = 0;
        foreach ($dados as $dado) {
            $total = $total + $dado->consumo;
        }
        $valorGLP = $valorGLP * $total;

        return $valorGLP;
    }

    public function valorMedioKWH() {
        $energia = new energia();
        $dados = $energia->valorMedioKWH();
        print_r($dados);
    }

}
