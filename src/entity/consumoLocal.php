<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace SCM\entity;

/**
 * Description of consumoLocal
 *
 * @author henrique.guedes
 */
class consumoLocal {
    private $idSCMConsumoEnergia;
    private $idSCMLocalEnergia;
    private $idCriador;
    private $consumoTotal;
    private $consumoMedio;
    private $dataInicio;
    private $dataTermino;
    private $totalDias;
    private $dataCriacao;
    private $status;
    function getIdSCMConsumoEnergia() {
        return $this->idSCMConsumoEnergia;
    }

    function getIdSCMLocalEnergia() {
        return $this->idSCMLocalEnergia;
    }

    function getIdCriador() {
        return $this->idCriador;
    }

    function getConsumoTotal() {
        return $this->consumoTotal;
    }

    function getConsumoMedio() {
        return $this->consumoMedio;
    }

    function getDataInicio() {
        return $this->dataInicio;
    }

    function getDataTermino() {
        return $this->dataTermino;
    }

    function getTotalDias() {
        return $this->totalDias;
    }

    function getDataCriacao() {
        return $this->dataCriacao;
    }

    function getStatus() {
        return $this->status;
    }

    function setIdSCMConsumoEnergia($idSCMConsumoEnergia) {
        $this->idSCMConsumoEnergia = $idSCMConsumoEnergia;
    }

    function setIdSCMLocalEnergia($idSCMLocalEnergia) {
        $this->idSCMLocalEnergia = $idSCMLocalEnergia;
    }

    function setIdCriador($idCriador) {
        $this->idCriador = $idCriador;
    }

    function setConsumoTotal($consumoTotal) {
        $this->consumoTotal = $consumoTotal;
    }

    function setConsumoMedio($consumoMedio) {
        $this->consumoMedio = $consumoMedio;
    }

    function setDataInicio($dataInicio) {
        $this->dataInicio = $dataInicio;
    }

    function setDataTermino($dataTermino) {
        $this->dataTermino = $dataTermino;
    }

    function setTotalDias($totalDias) {
        $this->totalDias = $totalDias;
    }

    function setDataCriacao($dataCriacao) {
        $this->dataCriacao = $dataCriacao;
    }

    function setStatus($status) {
        $this->status = $status;
    }


          
}
