<?php

namespace SCM\entity;

class secador {

    private $idSCMSecador;
    private $local;
    private $descricao;
    private $dataCriacao;
    private $status;

    function __construct() {
        
    }

    function getIdSCMSecador() {
        return $this->idSCMSecador;
    }

    function getLocal() {
        return $this->local;
    }

    function getDescricao() {
        return $this->descricao;
    }

    function getDataCriacao() {
        return $this->dataCriacao;
    }

    function getStatus() {
        return $this->status;
    }

    function setIdSCMSecador($idSCMSecador) {
        $this->idSCMSecador = $idSCMSecador;
    }

    function setLocal($local) {
        $this->local = $local;
    }

    function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    function setDataCriacao($dataCriacao) {
        $this->dataCriacao = $dataCriacao;
    }

    function setStatus($status) {
        $this->status = $status;
    }

}
