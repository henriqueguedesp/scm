<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace SCM\entity;

/**
 * Description of producao
 *
 * @author henrique.guedes
 */
class producao {
    //put your code here
    private $idSCMProducao;
    private $idCriador;
    private $dataApontamento;
    private $dataInicial;
    private $dataFinal;
    private $totalProduzido;
    private $status;
    private $diasApontamento;
    private $mediaProducao; 
    private $local;
    function getLocal() {
        return $this->local;
    }

    function setLocal($local) {
        $this->local = $local;
    }

                
            
    
    function getIdSCMProducao() {
        return $this->idSCMProducao;
    }

    function getIdCriador() {
        return $this->idCriador;
    }

    function getDataApontamento() {
        return $this->dataApontamento;
    }

    function getDataInicial() {
        return $this->dataInicial;
    }

    function getDataFinal() {
        return $this->dataFinal;
    }

    function getTotalProduzido() {
        return $this->totalProduzido;
    }

    function getStatus() {
        return $this->status;
    }

    function getDiasApontamento() {
        return $this->diasApontamento;
    }

    function getMediaProducao() {
        return $this->mediaProducao;
    }

    function setIdSCMProducao($idSCMProducao) {
        $this->idSCMProducao = $idSCMProducao;
    }

    function setIdCriador($idCriador) {
        $this->idCriador = $idCriador;
    }

    function setDataApontamento($dataApontamento) {
        $this->dataApontamento = $dataApontamento;
    }

    function setDataInicial($dataInicial) {
        $this->dataInicial = $dataInicial;
    }

    function setDataFinal($dataFinal) {
        $this->dataFinal = $dataFinal;
    }

    function setTotalProduzido($totalProduzido) {
        $this->totalProduzido = $totalProduzido;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function setDiasApontamento($diasApontamento) {
        $this->diasApontamento = $diasApontamento;
    }

    function setMediaProducao($mediaProducao) {
        $this->mediaProducao = $mediaProducao;
    }


}
