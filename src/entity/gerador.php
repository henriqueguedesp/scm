<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace SCM\entity;

/**
 * Description of gerador
 *
 * @author henrique.guedes
 */
class gerador {

    //put your code here
    private $idSCMGerador;
    private $local;
    private $descricao;
    private $dataCriacao;
    private $status;

    function __construct() {
        
    }

    function getIdSCMGerador() {
        return $this->idSCMGerador;
    }

    function getLocal() {
        return $this->local;
    }

    function getDescricao() {
        return $this->descricao;
    }

    function getDataCriacao() {
        return $this->dataCriacao;
    }

    function setIdSCMGerador($idSCMGerador) {
        $this->idSCMGerador = $idSCMGerador;
    }

    function setLocal($local) {
        $this->local = $local;
    }

    function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    function setDataCriacao($dataCriacao) {
        $this->dataCriacao = $dataCriacao;
    }

    function getStatus() {
        return $this->status;
    }

    function setStatus($status) {
        $this->status = $status;
    }


    
}
