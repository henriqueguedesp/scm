<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace SCM\entity;

/**
 * Description of localEnergia
 *
 * @author henrique.guedes
 */
class localEnergia {
    //put your code here
    
    private $idLocalEnergia;
    private $idCriador;
    private $local;
    private $descricao;
    private $dataCriacao;
    private $status;
    
    function getIdLocalEnergia() {
        return $this->idLocalEnergia;
    }

    function getIdCriador() {
        return $this->idCriador;
    }

    function getLocal() {
        return $this->local;
    }

    function getDescricao() {
        return $this->descricao;
    }

    function getDataCriacao() {
        return $this->dataCriacao;
    }

    function getStatus() {
        return $this->status;
    }

    function setIdLocalEnergia($idLocalEnergia) {
        $this->idLocalEnergia = $idLocalEnergia;
    }

    function setIdCriador($idCriador) {
        $this->idCriador = $idCriador;
    }

    function setLocal($local) {
        $this->local = $local;
    }

    function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    function setDataCriacao($dataCriacao) {
        $this->dataCriacao = $dataCriacao;
    }

    function setStatus($status) {
        $this->status = $status;
    }


}
