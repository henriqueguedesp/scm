<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace SCM\entity;

/**
 * Description of geracaoEnergia
 *
 * @author henrique.guedes
 */
class geracaoEnergia {

    private $idSCMGEnergia;
    private $idSCMGerador;
    private $idLeitor;
    private $dataLeitura;
    private $valorProduzidoInicial;
    private $valorProduzidoFinal;
    private $status;
    private $dataInicial;
    private $dataFinal;
    private $diasApontamento;
    private $mediaProduzido;
    

    function getDataInicial() {
        return $this->dataInicial;
    }

    function getDataFinal() {
        return $this->dataFinal;
    }

    function setDataInicial($dataInicial) {
        $this->dataInicial = $dataInicial;
    }

    function setDataFinal($dataFinal) {
        $this->dataFinal = $dataFinal;
    }

    
    function getDiasApontamento() {
        return $this->diasApontamento;
    }

    function getMediaProduzido() {
        return $this->mediaProduzido;
    }

    function setDiasApontamento($diasApontamento) {
        $this->diasApontamento = $diasApontamento;
    }

    function setMediaProduzido($mediaProduzido) {
        $this->mediaProduzido = $mediaProduzido;
    }

        function __construct() {
        
    }

    function getIdSCMGEnergia() {
        return $this->idSCMGEnergia;
    }

    function getIdSCMGerador() {
        return $this->idSCMGerador;
    }

    function getIdLeitor() {
        return $this->idLeitor;
    }

    function getDataLeitura() {
        return $this->dataLeitura;
    }

    function getValorProduzidoInicial() {
        return $this->valorProduzidoInicial;
    }

    function getValorProduzidoFinal() {
        return $this->valorProduzidoFinal;
    }

    function getStatus() {
        return $this->status;
    }

    function setIdSCMGEnergia($idSCMGEnergia) {
        $this->idSCMGEnergia = $idSCMGEnergia;
    }

    function setIdSCMGerador($idSCMGerador) {
        $this->idSCMGerador = $idSCMGerador;
    }

    function setIdLeitor($idLeitor) {
        $this->idLeitor = $idLeitor;
    }

    function setDataLeitura($dataLeitura) {
        $this->dataLeitura = $dataLeitura;
    }

    function setValorProduzidoInicial($valorProduzidoInicial) {
        $this->valorProduzidoInicial = $valorProduzidoInicial;
    }

    function setValorProduzidoFinal($valorProduzidoFinal) {
        $this->valorProduzidoFinal = $valorProduzidoFinal;
    }

    function setStatus($status) {
        $this->status = $status;
    }

}
