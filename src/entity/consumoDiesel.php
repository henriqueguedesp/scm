<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace SCM\entity;

/**
 * Description of consumoDiesel
 *
 * @author henrique.guedes
 */
class consumoDiesel {

    private $idSCMGConsumo;
    private $idSCMGerador;
    private $idLeitor;
    private $dataLeitura;
    private $dataInicial;
    private $dataFinal;
    private $valorConsumoInicial;
    private $valorConsumoFinal;
    private $status;
    private $diasApontamento;
    private $mediaConsumo;

    function __construct() {
        
    }
    function getDiasApontamento() {
        return $this->diasApontamento;
    }

    function getMediaConsumo() {
        return $this->mediaConsumo;
    }

    function setDiasApontamento($diasApontamento) {
        $this->diasApontamento = $diasApontamento;
    }

    function setMediaConsumo($mediaConsumo) {
        $this->mediaConsumo = $mediaConsumo;
    }

        
    
    function getIdSCMGConsumo() {
        return $this->idSCMGConsumo;
    }

    function getIdSCMGerador() {
        return $this->idSCMGerador;
    }

    function getIdLeitor() {
        return $this->idLeitor;
    }

    function getDataLeitura() {
        return $this->dataLeitura;
    }

    function getDataInicial() {
        return $this->dataInicial;
    }

    function getDataFinal() {
        return $this->dataFinal;
    }

    function setDataInicial($dataInicial) {
        $this->dataInicial = $dataInicial;
    }

    function setDataFinal($dataFinal) {
        $this->dataFinal = $dataFinal;
    }

    
    function getValorConsumoInicial() {
        return $this->valorConsumoInicial;
    }

    function getValorConsumoFinal() {
        return $this->valorConsumoFinal;
    }

    function getStatus() {
        return $this->status;
    }

    function setIdSCMGConsumo($idSCMGConsumo) {
        $this->idSCMGConsumo = $idSCMGConsumo;
    }

    function setIdSCMGerador($idSCMGerador) {
        $this->idSCMGerador = $idSCMGerador;
    }

    function setIdLeitor($idLeitor) {
        $this->idLeitor = $idLeitor;
    }

    function setDataLeitura($dataLeitura) {
        $this->dataLeitura = $dataLeitura;
    }



    function setValorConsumoInicial($valorConsumoInicial) {
        $this->valorConsumoInicial = $valorConsumoInicial;
    }

    function setValorConsumoFinal($valorConsumoFinal) {
        $this->valorConsumoFinal = $valorConsumoFinal;
    }

    function setStatus($status) {
        $this->status = $status;
    }

}
