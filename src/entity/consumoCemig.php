<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace SCM\entity;

/**
 * Description of consumoCemig
 *
 * @author henrique.guedes
 */
class consumoCemig {
    //put your code here
    
    private $idSCMEnergia;
    private $idLeitor;
    private $consumo;
    private $valorPago;
    private $mesReferencia;
    private $status;
    
    function __construct() {
        
    }
    
    function getIdSCMEnergia() {
        return $this->idSCMEnergia;
    }

    function getIdLeitor() {
        return $this->idLeitor;
    }

    function getConsumo() {
        return $this->consumo;
    }

    function getValorPago() {
        return $this->valorPago;
    }

    function getMesReferencia() {
        return $this->mesReferencia;
    }

    function getStatus() {
        return $this->status;
    }

    function setIdSCMEnergia($idSCMEnergia) {
        $this->idSCMEnergia = $idSCMEnergia;
    }

    function setIdLeitor($idLeitor) {
        $this->idLeitor = $idLeitor;
    }

    function setConsumo($consumo) {
        $this->consumo = $consumo;
    }

    function setValorPago($valorPago) {
        $this->valorPago = $valorPago;
    }

    function setMesReferencia($mesReferencia) {
        $this->mesReferencia = $mesReferencia;
    }

    function setStatus($status) {
        $this->status = $status;
    }



    
}
