<?php

namespace SCM\entity;

/**
 * Description of consumoGLP
 *
 * @author henrique.guedes
 */
class consumoGLP {

    private $idSCMSConsumo;
    private $idSCMSecador;
    private $idLeitor;
    private $dataLeitura;
    private $dataInicial;
    private $dataFinal;
    private $valorConsumoInicial;
    private $valoreConsumoFinal;
    private $status;
    private $diasApontamento;
    private $mediaConsumo;

    function __construct() {
        
    }
    
    function getDiasApontamento() {
        return $this->diasApontamento;
    }

    function getMediaConsumo() {
        return $this->mediaConsumo;
    }

    function setDiasApontamento($diasApontamento) {
        $this->diasApontamento = $diasApontamento;
    }

    function setMediaConsumo($mediaConsumo) {
        $this->mediaConsumo = $mediaConsumo;
    }

        
    function getDataInicial() {
        return $this->dataInicial;
    }

    function getDataFinal() {
        return $this->dataFinal;
    }

    function setDataInicial($dataInicial) {
        $this->dataInicial = $dataInicial;
    }

    function setDataFinal($dataFinal) {
        $this->dataFinal = $dataFinal;
    }

    
    function getIdSCMSConsumo() {
        return $this->idSCMSConsumo;
    }

    function getIdSCMSecador() {
        return $this->idSCMSecador;
    }

    function getIdLeitor() {
        return $this->idLeitor;
    }

    function getDataLeitura() {
        return $this->dataLeitura;
    }

    

    function getValorConsumoInicial() {
        return $this->valorConsumoInicial;
    }

    function getValoreConsumoFinal() {
        return $this->valoreConsumoFinal;
    }

    function getStatus() {
        return $this->status;
    }

    function setIdSCMSConsumo($idSCMSConsumo) {
        $this->idSCMSConsumo = $idSCMSConsumo;
    }

    function setIdSCMSecador($idSCMSecador) {
        $this->idSCMSecador = $idSCMSecador;
    }

    function setIdLeitor($idLeitor) {
        $this->idLeitor = $idLeitor;
    }

    function setDataLeitura($dataLeitura) {
        $this->dataLeitura = $dataLeitura;
    }

    

    function setValorConsumoInicial($valorConsumoInicial) {
        $this->valorConsumoInicial = $valorConsumoInicial;
    }

    function setValoreConsumoFinal($valoreConsumoFinal) {
        $this->valoreConsumoFinal = $valoreConsumoFinal;
    }

    function setStatus($status) {
        $this->status = $status;
    }

}
