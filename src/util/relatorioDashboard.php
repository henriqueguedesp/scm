<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace SCM\util;

/**
 * Description of relatorioDashboard
 *
 * @author henrique.guedes
 */
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use SCM\util\sessao;
use SCM\models\modeloProtheus;
use SCM\models\modeloConfiguracoes;
use SCM\models\modeloApontamentoConsumoCemig;
use SCM\models\modeloSecador;
use SCM\models\modeloApontamentoGLP;
use SCM\models\modeloApontamentoDiesel;
use SCM\util\energia;
use SCM\models\modeloApontamentoConsumoEnergiaLocal;
use SCM\models\modeloApontamentoProducao;

class relatorioDashboard {

    //put your code here

    public function custoTotalLinha($idLinha) {
        $modelo = new modeloConfiguracoes();
        $dados = $modelo->safraAtual();
        $dataInicioSafraDB = $dados->dataInicioSafra;
        $dataTerminoSafraDB = $dados->dataTerminoSafra;
        $dados->dataInicioSafra = str_replace("-", "", $dados->dataInicioSafra);
        $dados->dataTerminoSafra = str_replace("-", "", $dados->dataTerminoSafra);

        $modelo = new modeloProtheus();

        $valorLinhaRecebimento = $modelo->itensGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '111010' . $idLinha, '111010' . $idLinha);
        $valorDespalha = $modelo->itensGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '111020' . $idLinha, '111020' . $idLinha);
        $valorSecador = $modelo->itensGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '111040' . $idLinha, '111040' . $idLinha);
        $materialRecebido = $modelo->totalMatRecPorLinha($dados->dataInicioSafra, $dados->dataTerminoSafra, $idLinha);
        /*
          print_r($valorLinhaRecebimento);
          print_r("<br>");
          print_r("<br>");
          print_r($valorDespalha);
          print_r("<br>");
          print_r("<br>");
          print_r($valorSecador);
          print_r("<br>"); */
        $total = 0;
        header('Cache-Control: no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Content-Type: application/x-msexcel');
        header("Content-Disposition: attachment; filename = Linha " . $idLinha . ".xls");
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo "<table border='1'>";
        echo "<tr>";
        echo "<td><center><b>Produto</b></center></td>";
        echo "<td><center><b>Valor R$</b></center></td>";
        echo "<td><center><b>Setor</b></center></td>";
        echo "</tr>";
        foreach ($valorLinhaRecebimento as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->NOME_PRODUTO . "</td>";
            echo "<td>" . number_format($dado->VALORTOTAL, 2, ",", ".") . "</td>";
            echo "<td> Linha " . $idLinha . "</td>";
            echo "</tr>";
            $total = $total + $dado->VALORTOTAL;
        }
        foreach ($valorDespalha as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->NOME_PRODUTO . "</td>";
            echo "<td>" . number_format($dado->VALORTOTAL, 2, ",", ".") . "</td>";
            echo "<td> Despalha " . $idLinha . "</td>";
            echo "</tr>";
            $total = $total + $dado->VALORTOTAL;
        }
        foreach ($valorSecador as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->NOME_PRODUTO . "</td>";
            echo "<td>" . number_format($dado->VALORTOTAL, 2, ",", ".") . "</td>";
            echo "<td> Secador " . $idLinha . "</td>";
            echo "</tr>";
            $total = $total + $dado->VALORTOTAL;
        }
        echo "<tr>";
        echo "<td><b>Custo total</b></td>";
        echo "<td colspan='2'><b>" . number_format($total, 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td><b>Total material recebido (linha " . $idLinha . ")</b></td>";
        echo "<td colspan='2'><b>" . number_format($materialRecebido->MATERIAL, 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td><b>Custo por tonelada recebida (linha " . $idLinha . ")</b></td>";
        echo "<td colspan='2'><b>" . number_format(($total / $materialRecebido->MATERIAL), 2, ",", ".") . "</b></td>";
        echo "</tr>";


        echo "</table>";
    }

    public function custoTotalLinhas() {
        $modelo = new modeloConfiguracoes();
        $dados = $modelo->safraAtual();
        $dataInicioSafraDB = $dados->dataInicioSafra;
        $dataTerminoSafraDB = $dados->dataTerminoSafra;
        $dados->dataInicioSafra = str_replace("-", "", $dados->dataInicioSafra);
        $dados->dataTerminoSafra = str_replace("-", "", $dados->dataTerminoSafra);

        $modelo = new modeloProtheus();

        $valorLinhaRecebimento = $modelo->itensGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1110101', '1110104');
        $valorDespalha = $modelo->itensGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1110201', '1110204');
        $valorSecador = $modelo->itensGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1110401', '1110404');
        /*
          print_r($valorLinhaRecebimento);
          print_r("<br>");
          print_r("<br>");
          print_r($valorDespalha);
          print_r("<br>");
          print_r("<br>");
          print_r($valorSecador);
          print_r("<br>"); */
        $total = 0;
        header('Cache-Control: no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Content-Type: application/x-msexcel');
        header("Content-Disposition: attachment; filename = Linha Absoluto.xls");
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo "<table border='1'>";
        echo "<tr>";
        echo "<td><center><b>Produto</b></center></td>";
        echo "<td><center><b>Valor R$</b></center></td>";
        //  echo "<td><center><b>Setor</b></center></td>";
        echo "</tr>";
        foreach ($valorLinhaRecebimento as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->NOME_PRODUTO . "</td>";
            echo "<td>" . number_format($dado->VALORTOTAL, 2, ",", ".") . "</td>";
            //  echo "<td> Linha " . $idLinha . "</td>";
            echo "</tr>";
            $total = $total + $dado->VALORTOTAL;
        }
        foreach ($valorDespalha as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->NOME_PRODUTO . "</td>";
            echo "<td>" . number_format($dado->VALORTOTAL, 2, ",", ".") . "</td>";
            //echo "<td> Despalha " . $idLinha . "</td>";
            echo "</tr>";
            $total = $total + $dado->VALORTOTAL;
        }
        foreach ($valorSecador as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->NOME_PRODUTO . "</td>";
            echo "<td>" . number_format($dado->VALORTOTAL, 2, ",", ".") . "</td>";
            //  echo "<td> Secador " . $idLinha . "</td>";
            echo "</tr>";
            $total = $total + $dado->VALORTOTAL;
        }
        echo "<tr>";
        echo "<td><b>Custo total</b></td>";
        echo "<td ><b>" . number_format($total, 2, ",", ".") . "</b></td>";
        echo "</tr>";



        echo "</table>";
    }

    public function custoSilosArmazenagemPorMaterial() {
        $modelo = new modeloConfiguracoes();
        $dados = $modelo->safraAtual();
        $dataInicioSafraDB = $dados->dataInicioSafra;
        $dataTerminoSafraDB = $dados->dataTerminoSafra;
        $dados->dataInicioSafra = str_replace("-", "", $dados->dataInicioSafra);
        $dados->dataTerminoSafra = str_replace("-", "", $dados->dataTerminoSafra);

        $modelo = new modeloProtheus();

        $valorSilosArmazenagem = $modelo->itensGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1110601', '1110601');
        $materialRecebido = $modelo->totalMatRecTotal($dados->dataInicioSafra, $dados->dataTerminoSafra);

        $total = 0;
        header('Cache-Control: no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Content-Type: application/x-msexcel');
        header("Content-Disposition: attachment; filename = Silos de Armazenagem por material.xls");
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo "<table border='1'>";
        echo "<tr>";
        echo "<td><center><b>Produto</b></center></td>";
        echo "<td><center><b>Valor R$</b></center></td>";
        //  echo "<td><center><b>Setor</b></center></td>";
        echo "</tr>";
        foreach ($valorSilosArmazenagem as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->NOME_PRODUTO . "</td>";
            echo "<td>" . number_format($dado->VALORTOTAL, 2, ",", ".") . "</td>";
            //  echo "<td> Linha " . $idLinha . "</td>";
            echo "</tr>";
            $total = $total + $dado->VALORTOTAL;
        }

        echo "<tr>";
        echo "<td><b>Custo total</b></td>";
        echo "<td ><b>" . number_format($total, 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td><b>Total material recebido</b></td>";
        echo "<td ><b>" . number_format($materialRecebido->MATERIAL, 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td><b>Custo por tonelada recebida </b></td>";
        echo "<td ><b>" . number_format(($total / $materialRecebido->MATERIAL), 2, ",", ".") . "</b></td>";
        echo "</tr>";


        echo "</table>";
    }

    public function custoSilosArmazenagem() {
        $modelo = new modeloConfiguracoes();
        $dados = $modelo->safraAtual();
        $dataInicioSafraDB = $dados->dataInicioSafra;
        $dataTerminoSafraDB = $dados->dataTerminoSafra;
        $dados->dataInicioSafra = str_replace("-", "", $dados->dataInicioSafra);
        $dados->dataTerminoSafra = str_replace("-", "", $dados->dataTerminoSafra);

        $modelo = new modeloProtheus();

        $valorSilosArmazenagem = $modelo->itensGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1110601', '1110601');

        $total = 0;
        header('Cache-Control: no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Content-Type: application/x-msexcel');
        header("Content-Disposition: attachment; filename = Silos de Armazenagem.xls");
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo "<table border='1'>";
        echo "<tr>";
        echo "<td><center><b>Produto</b></center></td>";
        echo "<td><center><b>Valor R$</b></center></td>";
        //  echo "<td><center><b>Setor</b></center></td>";
        echo "</tr>";
        foreach ($valorSilosArmazenagem as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->NOME_PRODUTO . "</td>";
            echo "<td>" . number_format($dado->VALORTOTAL, 2, ",", ".") . "</td>";
            //  echo "<td> Linha " . $idLinha . "</td>";
            echo "</tr>";
            $total = $total + $dado->VALORTOTAL;
        }

        echo "<tr>";
        echo "<td><b>Custo total</b></td>";
        echo "<td ><b>" . number_format($total, 2, ",", ".") . "</b></td>";
        echo "</tr>";

        echo "</table>";
    }

    public function custoDescartePorMaterial() {
        $modelo = new modeloConfiguracoes();
        $dados = $modelo->safraAtual();
        $dataInicioSafraDB = $dados->dataInicioSafra;
        $dataTerminoSafraDB = $dados->dataTerminoSafra;
        $dados->dataInicioSafra = str_replace("-", "", $dados->dataInicioSafra);
        $dados->dataTerminoSafra = str_replace("-", "", $dados->dataTerminoSafra);

        $modelo = new modeloProtheus();

        $valorDescarte = $modelo->itensGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1110301', '1110302');
        $materialRecebido = $modelo->totalMatRecTotal($dados->dataInicioSafra, $dados->dataTerminoSafra);

        $total = 0;
        header('Cache-Control: no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Content-Type: application/x-msexcel');
        header("Content-Disposition: attachment; filename = Descarte por material.xls");
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo "<table border='1'>";
        echo "<tr>";
        echo "<td><center><b>Produto</b></center></td>";
        echo "<td><center><b>Valor R$</b></center></td>";
        //  echo "<td><center><b>Setor</b></center></td>";
        echo "</tr>";
        foreach ($valorDescarte as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->NOME_PRODUTO . "</td>";
            echo "<td>" . number_format($dado->VALORTOTAL, 2, ",", ".") . "</td>";
            //  echo "<td> Linha " . $idLinha . "</td>";
            echo "</tr>";
            $total = $total + $dado->VALORTOTAL;
        }

        echo "<tr>";
        echo "<td><b>Custo total</b></td>";
        echo "<td ><b>" . number_format($total, 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td><b>Total material recebido</b></td>";
        echo "<td ><b>" . number_format($materialRecebido->MATERIAL, 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td><b>Custo por tonelada recebida </b></td>";
        echo "<td ><b>" . number_format(($total / $materialRecebido->MATERIAL), 2, ",", ".") . "</b></td>";
        echo "</tr>";


        echo "</table>";
    }

    public function custoDescarte() {
        $modelo = new modeloConfiguracoes();
        $dados = $modelo->safraAtual();
        $dataInicioSafraDB = $dados->dataInicioSafra;
        $dataTerminoSafraDB = $dados->dataTerminoSafra;
        $dados->dataInicioSafra = str_replace("-", "", $dados->dataInicioSafra);
        $dados->dataTerminoSafra = str_replace("-", "", $dados->dataTerminoSafra);
        $modelo = new modeloProtheus();
        $valorDescarte = $modelo->itensGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1110301', '1110302');
        $total = 0;
        header('Cache-Control: no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Content-Type: application/x-msexcel');
        header("Content-Disposition: attachment; filename = Descarte.xls");
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo "<table border='1'>";
        echo "<tr>";
        echo "<td><center><b>Produto</b></center></td>";
        echo "<td><center><b>Valor R$</b></center></td>";
        //  echo "<td><center><b>Setor</b></center></td>";
        echo "</tr>";
        foreach ($valorDescarte as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->NOME_PRODUTO . "</td>";
            echo "<td>" . number_format($dado->VALORTOTAL, 2, ",", ".") . "</td>";
            //  echo "<td> Linha " . $idLinha . "</td>";
            echo "</tr>";
            $total = $total + $dado->VALORTOTAL;
        }

        echo "<tr>";
        echo "<td><b>Custo total</b></td>";
        echo "<td ><b>" . number_format($total, 2, ",", ".") . "</b></td>";
        echo "</tr>";



        echo "</table>";
    }

    public function custoEmpilhadeiras() {
        $modelo = new modeloConfiguracoes();
        $dados = $modelo->safraAtual();
        $dataInicioSafraDB = $dados->dataInicioSafra;
        $dataTerminoSafraDB = $dados->dataTerminoSafra;
        $dados->dataInicioSafra = str_replace("-", "", $dados->dataInicioSafra);
        $dados->dataTerminoSafra = str_replace("-", "", $dados->dataTerminoSafra);
        $modelo = new modeloProtheus();
        $valorEmpilhadeiras = $modelo->itensGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1130201', '1130201');
        $total = 0;
        header('Cache-Control: no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Content-Type: application/x-msexcel');
        header("Content-Disposition: attachment; filename = Empilhadeiras.xls");
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo "<table border='1'>";
        echo "<tr>";
        echo "<td><center><b>Produto</b></center></td>";
        echo "<td><center><b>Valor R$</b></center></td>";
        //  echo "<td><center><b>Setor</b></center></td>";
        echo "</tr>";
        foreach ($valorEmpilhadeiras as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->NOME_PRODUTO . "</td>";
            echo "<td>" . number_format($dado->VALORTOTAL, 2, ",", ".") . "</td>";
            //  echo "<td> Linha " . $idLinha . "</td>";
            echo "</tr>";
            $total = $total + $dado->VALORTOTAL;
        }

        echo "<tr>";
        echo "<td><b>Custo total</b></td>";
        echo "<td ><b>" . number_format($total, 2, ",", ".") . "</b></td>";
        echo "</tr>";



        echo "</table>";
    }

    public function custoDespalha() {
        $modelo = new modeloConfiguracoes();
        $dados = $modelo->safraAtual();
        $dataInicioSafraDB = $dados->dataInicioSafra;
        $dataTerminoSafraDB = $dados->dataTerminoSafra;
        $dados->dataInicioSafra = str_replace("-", "", $dados->dataInicioSafra);
        $dados->dataTerminoSafra = str_replace("-", "", $dados->dataTerminoSafra);
        $modelo = new modeloProtheus();
        $valorEmpilhadeiras = $modelo->itensGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1110501', '1110501');
        $total = 0;
        header('Cache-Control: no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Content-Type: application/x-msexcel');
        header("Content-Disposition: attachment; filename = Debulha.xls");
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo "<table border='1'>";
        echo "<tr>";
        echo "<td><center><b>Produto</b></center></td>";
        echo "<td><center><b>Valor R$</b></center></td>";
        //  echo "<td><center><b>Setor</b></center></td>";
        echo "</tr>";
        foreach ($valorEmpilhadeiras as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->NOME_PRODUTO . "</td>";
            echo "<td>" . number_format($dado->VALORTOTAL, 2, ",", ".") . "</td>";
            //  echo "<td> Linha " . $idLinha . "</td>";
            echo "</tr>";
            $total = $total + $dado->VALORTOTAL;
        }
        echo "<tr>";
        echo "<td><b>Custo total</b></td>";
        echo "<td ><b>" . number_format($total, 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "</table>";
    }

    public function custoSistemaAR() {
        $modelo = new modeloConfiguracoes();
        $dados = $modelo->safraAtual();
        $dataInicioSafraDB = $dados->dataInicioSafra;
        $dataTerminoSafraDB = $dados->dataTerminoSafra;
        $dados->dataInicioSafra = str_replace("-", "", $dados->dataInicioSafra);
        $dados->dataTerminoSafra = str_replace("-", "", $dados->dataTerminoSafra);
        $modelo = new modeloProtheus();
        $custoCCCasaCompressorTorre = $modelo->itensGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1120701', '1120701');
        $custoCCSistemaArComprimidoTorre = $modelo->itensGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1129901', '1129901');
        $custoCCCasaCompressor = $modelo->itensGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1111001', '1111002');
        $total = 0;
        header('Cache-Control: no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Content-Type: application/x-msexcel');
        header("Content-Disposition: attachment; filename = Sistema Ar.xls");
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo "<table border='1'>";
        echo "<tr>";
        echo "<td><center><b>Produto</b></center></td>";
        echo "<td><center><b>Valor R$</b></center></td>";
        //  echo "<td><center><b>Setor</b></center></td>";
        echo "</tr>";
        foreach ($custoCCCasaCompressorTorre as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->NOME_PRODUTO . "</td>";
            echo "<td>" . number_format($dado->VALORTOTAL, 2, ",", ".") . "</td>";
            //  echo "<td> Linha " . $idLinha . "</td>";
            echo "</tr>";
            $total = $total + $dado->VALORTOTAL;
        }
        foreach ($custoCCSistemaArComprimidoTorre as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->NOME_PRODUTO . "</td>";
            echo "<td>" . number_format($dado->VALORTOTAL, 2, ",", ".") . "</td>";
            //  echo "<td> Linha " . $idLinha . "</td>";
            echo "</tr>";
            $total = $total + $dado->VALORTOTAL;
        }
        foreach ($custoCCCasaCompressor as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->NOME_PRODUTO . "</td>";
            echo "<td>" . number_format($dado->VALORTOTAL, 2, ",", ".") . "</td>";
            //  echo "<td> Linha " . $idLinha . "</td>";
            echo "</tr>";
            $total = $total + $dado->VALORTOTAL;
        }
        echo "<tr>";
        echo "<td><b>Custo total</b></td>";
        echo "<td ><b>" . number_format($total, 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "</table>";
    }

    public function custoArmazens() {
        $modelo = new modeloConfiguracoes();
        $dados = $modelo->safraAtual();
        $dataInicioSafraDB = $dados->dataInicioSafra;
        $dataTerminoSafraDB = $dados->dataTerminoSafra;
        $dados->dataInicioSafra = str_replace("-", "", $dados->dataInicioSafra);
        $dados->dataTerminoSafra = str_replace("-", "", $dados->dataTerminoSafra);
        $modelo = new modeloProtheus();
        $custoCCArmazens = $modelo->itensGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1130101', '1130108');
        $custoCCUnidadeCondensadoras = $modelo->itensGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1130202', '1130202');
        $total = 0;
        header('Cache-Control: no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Content-Type: application/x-msexcel');
        header("Content-Disposition: attachment; filename = Armazéns.xls");
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo "<table border='1'>";
        echo "<tr>";
        echo "<td><center><b>Produto</b></center></td>";
        echo "<td><center><b>Valor R$</b></center></td>";
        //  echo "<td><center><b>Setor</b></center></td>";
        echo "</tr>";
        foreach ($custoCCArmazens as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->NOME_PRODUTO . "</td>";
            echo "<td>" . number_format($dado->VALORTOTAL, 2, ",", ".") . "</td>";
            //  echo "<td> Linha " . $idLinha . "</td>";
            echo "</tr>";
            $total = $total + $dado->VALORTOTAL;
        }
        foreach ($custoCCUnidadeCondensadoras as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->NOME_PRODUTO . "</td>";
            echo "<td>" . number_format($dado->VALORTOTAL, 2, ",", ".") . "</td>";
            //  echo "<td> Linha " . $idLinha . "</td>";
            echo "</tr>";
            $total = $total + $dado->VALORTOTAL;
        }
        echo "<tr>";
        echo "<td><b>Custo total</b></td>";
        echo "<td ><b>" . number_format($total, 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "</table>";
    }

    public function custoTotal() {
        $modelo = new modeloConfiguracoes();
        $dados = $modelo->safraAtual();
        $dataInicioSafraDB = $dados->dataInicioSafra;
        $dataTerminoSafraDB = $dados->dataTerminoSafra;
        $dados->dataInicioSafra = str_replace("-", "", $dados->dataInicioSafra);
        $dados->dataTerminoSafra = str_replace("-", "", $dados->dataTerminoSafra);
        $modelo = new modeloProtheus();
        #RECEBIMENTO
        $valorLinhaRecebimento = $modelo->itensGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1110101', '1110104');
        $valorDespalha = $modelo->itensGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1110201', '1110204');
        $valorSecador = $modelo->itensGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1110401', '1110404');
        #ARMAZENS
        $custoCCArmazens = $modelo->itensGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1130101', '1130108');
        $custoCCUnidadeCondensadoras = $modelo->itensGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1130202', '1130202');
        #SISTEMA AR
        $custoCCCasaCompressorTorre = $modelo->itensGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1120701', '1120701');
        $custoCCSistemaArComprimidoTorre = $modelo->itensGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1129901', '1129901');
        $custoCCCasaCompressor = $modelo->itensGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1111001', '1111002');
        #EMPILHADEIRAS
        $valorEmpilhadeiras = $modelo->itensGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1130201', '1130201');
        #DESPALHA
        $valorDebulha = $modelo->itensGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1110501', '1110501');
        #SILOS
        $valorSilosArmazenagem = $modelo->itensGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1110601', '1110601');
        #DESCARTE
        $valorDescarte = $modelo->itensGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1110301', '1110302');
        $total = 0;
        header('Cache-Control: no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Content-Type: application/x-msexcel');
        header("Content-Disposition: attachment; filename = Custo Total.xls");
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo "<table border='1'>";
        echo "<tr>";
        echo "<td><center><b>Produto</b></center></td>";
        echo "<td><center><b>Valor R$</b></center></td>";
        echo "<td><center><b>Setor</b></center></td>";
        echo "</tr>";
        foreach ($valorLinhaRecebimento as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->NOME_PRODUTO . "</td>";
            echo "<td>" . number_format($dado->VALORTOTAL, 2, ",", ".") . "</td>";
            echo "<td> Linhas de recebimento</td>";
            echo "</tr>";
            $total = $total + $dado->VALORTOTAL;
        }
        foreach ($valorDebulha as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->NOME_PRODUTO . "</td>";
            echo "<td>" . number_format($dado->VALORTOTAL, 2, ",", ".") . "</td>";
            echo "<td> Debulha</td>";
            echo "</tr>";
            $total = $total + $dado->VALORTOTAL;
        }
        foreach ($valorSecador as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->NOME_PRODUTO . "</td>";
            echo "<td>" . number_format($dado->VALORTOTAL, 2, ",", ".") . "</td>";
            echo "<td> Secador</td>";
            echo "</tr>";
            $total = $total + $dado->VALORTOTAL;
        }
        foreach ($custoCCArmazens as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->NOME_PRODUTO . "</td>";
            echo "<td>" . number_format($dado->VALORTOTAL, 2, ",", ".") . "</td>";
            echo "<td> Armazéns</td>";
            echo "</tr>";
            $total = $total + $dado->VALORTOTAL;
        }
        foreach ($custoCCUnidadeCondensadoras as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->NOME_PRODUTO . "</td>";
            echo "<td>" . number_format($dado->VALORTOTAL, 2, ",", ".") . "</td>";
            echo "<td> Armazéns</td>";
            echo "</tr>";
            $total = $total + $dado->VALORTOTAL;
        }
        foreach ($custoCCCasaCompressorTorre as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->NOME_PRODUTO . "</td>";
            echo "<td>" . number_format($dado->VALORTOTAL, 2, ",", ".") . "</td>";
            echo "<td> Sistema AR</td>";
            echo "</tr>";
            $total = $total + $dado->VALORTOTAL;
        }
        foreach ($custoCCSistemaArComprimidoTorre as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->NOME_PRODUTO . "</td>";
            echo "<td>" . number_format($dado->VALORTOTAL, 2, ",", ".") . "</td>";
            echo "<td> Sistema AR</td>";
            echo "</tr>";
            $total = $total + $dado->VALORTOTAL;
        }
        foreach ($custoCCCasaCompressor as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->NOME_PRODUTO . "</td>";
            echo "<td>" . number_format($dado->VALORTOTAL, 2, ",", ".") . "</td>";
            echo "<td> Sistema AR</td>";
            echo "</tr>";
            $total = $total + $dado->VALORTOTAL;
        }
        foreach ($valorEmpilhadeiras as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->NOME_PRODUTO . "</td>";
            echo "<td>" . number_format($dado->VALORTOTAL, 2, ",", ".") . "</td>";
            echo "<td> Empilhadeiras</td>";
            echo "</tr>";
            $total = $total + $dado->VALORTOTAL;
        }
        foreach ($valorDespalha as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->NOME_PRODUTO . "</td>";
            echo "<td>" . number_format($dado->VALORTOTAL, 2, ",", ".") . "</td>";
            echo "<td> Despalha</td>";
            echo "</tr>";
            $total = $total + $dado->VALORTOTAL;
        }
        foreach ($valorSilosArmazenagem as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->NOME_PRODUTO . "</td>";
            echo "<td>" . number_format($dado->VALORTOTAL, 2, ",", ".") . "</td>";
            echo "<td> Silos de Armazenagem</td>";
            echo "</tr>";
            $total = $total + $dado->VALORTOTAL;
        }
        foreach ($valorDescarte as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->NOME_PRODUTO . "</td>";
            echo "<td>" . number_format($dado->VALORTOTAL, 2, ",", ".") . "</td>";
            echo "<td> Descarte</td>";
            echo "</tr>";
            $total = $total + $dado->VALORTOTAL;
        }
        echo "<tr>";
        echo "<td><b>Custo total</b></td>";
        echo "<td colspan='2' ><b>" . number_format($total, 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "</table>";
    }

    public function custoEnergiaLinha01() {
        $modelo = new modeloConfiguracoes();
        $dados = $modelo->safraAtual();
        $dataInicioSafraDB = $dados->dataInicioSafra;
        $dataTerminoSafraDB = $dados->dataTerminoSafra;
        $dados->dataInicioSafra = str_replace("-", "", $dados->dataInicioSafra);
        $dados->dataTerminoSafra = str_replace("-", "", $dados->dataTerminoSafra);

        $modelo = new modeloProtheus();

        // $valorDescarte = $modelo->itensGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1110301', '1110302');
        $materialRecebido = $modelo->totalMatRecPorLinha($dados->dataInicioSafra, $dados->dataTerminoSafra, 1);

        ##INTERNO
        $modelo = new modeloApontamentoConsumoEnergiaLocal();
        $informacoesKHW = new energia();
        $valorMedioKWH = $informacoesKHW->valorMedioKWH();
        $custoEnergiaLinha01 = $modelo->consumoEnergiaLocal(1, $dados->dataInicioSafra, $dados->dataTerminoSafra);
        $total = 0;
        header('Cache-Control: no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Content-Type: application/x-msexcel');
        header("Content-Disposition: attachment; filename = Custo energia linha 01s.xls");
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo "<table border='1'>";
        echo "<tr>";
        echo "<td><center><b>Consumo</b></center></td>";
        echo "<td><center><b>Valor R$</b></center></td>";
        //  echo "<td><center><b>Setor</b></center></td>";
        echo "</tr>";
        foreach ($custoEnergiaLinha01 as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->consumo . "</td>";
            echo "<td>" . number_format(($dado->consumo * $valorMedioKWH), 2, ",", ".") . "</td>";
            //  echo "<td> Linha " . $idLinha . "</td>";
            echo "</tr>";
            $total = $total + $dado->consumo;
        }

        echo "<tr>";
        echo "<td><b>Consumo total</b></td>";
        echo "<td ><b>" . number_format($total, 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td><b>Custo total R$</b></td>";
        echo "<td ><b>" . number_format(($total * $valorMedioKWH), 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "<td><b>Total material recebido</b></td>";
        echo "<td ><b>" . number_format($materialRecebido->MATERIAL, 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td><b>Custo por tonelada recebida </b></td>";
        echo "<td ><b>" . number_format((($total * $valorMedioKWH) / $materialRecebido->MATERIAL), 2, ",", ".") . "</b></td>";
        echo "</tr>";


        echo "</table>";
    }

    public function custoEnergiaLinha02() {
        $modelo = new modeloConfiguracoes();
        $dados = $modelo->safraAtual();
        $dataInicioSafraDB = $dados->dataInicioSafra;
        $dataTerminoSafraDB = $dados->dataTerminoSafra;
        $dados->dataInicioSafra = str_replace("-", "", $dados->dataInicioSafra);
        $dados->dataTerminoSafra = str_replace("-", "", $dados->dataTerminoSafra);

        $modelo = new modeloProtheus();

        // $valorDescarte = $modelo->itensGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1110301', '1110302');
        $materialRecebido = $modelo->totalMatRecPorLinha($dados->dataInicioSafra, $dados->dataTerminoSafra, 2);

        ##INTERNO
        $modelo = new modeloApontamentoConsumoEnergiaLocal();
        $informacoesKHW = new energia();
        $valorMedioKWH = $informacoesKHW->valorMedioKWH();
        $custoEnergiaLinha01 = $modelo->consumoEnergiaLocal(2, $dados->dataInicioSafra, $dados->dataTerminoSafra);
        $total = 0;
        header('Cache-Control: no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Content-Type: application/x-msexcel');
        header("Content-Disposition: attachment; filename = Custo energia linha 02.xls");
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo "<table border='1'>";
        echo "<tr>";
        echo "<td><center><b>Consumo</b></center></td>";
        echo "<td><center><b>Valor R$</b></center></td>";
        //  echo "<td><center><b>Setor</b></center></td>";
        echo "</tr>";
        foreach ($custoEnergiaLinha01 as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->consumo . "</td>";
            echo "<td>" . number_format(($dado->consumo * $valorMedioKWH), 2, ",", ".") . "</td>";
            //  echo "<td> Linha " . $idLinha . "</td>";
            echo "</tr>";
            $total = $total + $dado->consumo;
        }

        echo "<tr>";
        echo "<td><b>Consumo total</b></td>";
        echo "<td ><b>" . number_format($total, 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td><b>Custo total R$</b></td>";
        echo "<td ><b>" . number_format(($total * $valorMedioKWH), 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "<td><b>Total material recebido</b></td>";
        echo "<td ><b>" . number_format($materialRecebido->MATERIAL, 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td><b>Custo por tonelada recebida </b></td>";
        echo "<td ><b>" . number_format((($total * $valorMedioKWH) / $materialRecebido->MATERIAL), 2, ",", ".") . "</b></td>";
        echo "</tr>";


        echo "</table>";
    }

    public function custoEnergiaLinha03() {
        $modelo = new modeloConfiguracoes();
        $dados = $modelo->safraAtual();
        $dataInicioSafraDB = $dados->dataInicioSafra;
        $dataTerminoSafraDB = $dados->dataTerminoSafra;
        $dados->dataInicioSafra = str_replace("-", "", $dados->dataInicioSafra);
        $dados->dataTerminoSafra = str_replace("-", "", $dados->dataTerminoSafra);
        $modelo = new modeloProtheus();
        $materialRecebido = $modelo->totalMatRecPorLinha($dados->dataInicioSafra, $dados->dataTerminoSafra, 3);
        ##INTERNO
        $modelo = new modeloApontamentoConsumoEnergiaLocal();
        $informacoesKHW = new energia();
        $valorMedioKWH = $informacoesKHW->valorMedioKWH();
        $custoEnergiaLinha01 = $modelo->consumoEnergiaLocal(3, $dados->dataInicioSafra, $dados->dataTerminoSafra);
        $total = 0;
        header('Cache-Control: no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Content-Type: application/x-msexcel');
        header("Content-Disposition: attachment; filename = Custo energia linha 03.xls");
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo "<table border='1'>";
        echo "<tr>";
        echo "<td><center><b>Consumo</b></center></td>";
        echo "<td><center><b>Valor R$</b></center></td>";
        //  echo "<td><center><b>Setor</b></center></td>";
        echo "</tr>";
        foreach ($custoEnergiaLinha01 as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->consumo . "</td>";
            echo "<td>" . number_format(($dado->consumo * $valorMedioKWH), 2, ",", ".") . "</td>";
            //  echo "<td> Linha " . $idLinha . "</td>";
            echo "</tr>";
            $total = $total + $dado->consumo;
        }
        echo "<tr>";
        echo "<td><b>Consumo total</b></td>";
        echo "<td ><b>" . number_format($total, 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td><b>Custo total R$</b></td>";
        echo "<td ><b>" . number_format(($total * $valorMedioKWH), 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "<td><b>Total material recebido</b></td>";
        echo "<td ><b>" . number_format($materialRecebido->MATERIAL, 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td><b>Custo por tonelada recebida </b></td>";
        echo "<td ><b>" . number_format((($total * $valorMedioKWH) / $materialRecebido->MATERIAL), 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "</table>";
    }

    public function custoEnergiaLinha04() {
        $modelo = new modeloConfiguracoes();
        $dados = $modelo->safraAtual();
        $dataInicioSafraDB = $dados->dataInicioSafra;
        $dataTerminoSafraDB = $dados->dataTerminoSafra;
        $dados->dataInicioSafra = str_replace("-", "", $dados->dataInicioSafra);
        $dados->dataTerminoSafra = str_replace("-", "", $dados->dataTerminoSafra);
        $modelo = new modeloProtheus();
        $materialRecebido = $modelo->totalMatRecPorLinha($dados->dataInicioSafra, $dados->dataTerminoSafra, 4);
        ##INTERNO
        $modelo = new modeloApontamentoConsumoEnergiaLocal();
        $informacoesKHW = new energia();
        $valorMedioKWH = $informacoesKHW->valorMedioKWH();
        $custoEnergiaLinha01 = $modelo->consumoEnergiaLocal(4, $dados->dataInicioSafra, $dados->dataTerminoSafra);
        $total = 0;
        header('Cache-Control: no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Content-Type: application/x-msexcel');
        header("Content-Disposition: attachment; filename = Custo energia linha 04.xls");
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo "<table border='1'>";
        echo "<tr>";
        echo "<td><center><b>Consumo</b></center></td>";
        echo "<td><center><b>Valor R$</b></center></td>";
        //  echo "<td><center><b>Setor</b></center></td>";
        echo "</tr>";
        foreach ($custoEnergiaLinha01 as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->consumo . "</td>";
            echo "<td>" . number_format(($dado->consumo * $valorMedioKWH), 2, ",", ".") . "</td>";
            //  echo "<td> Linha " . $idLinha . "</td>";
            echo "</tr>";
            $total = $total + $dado->consumo;
        }
        echo "<tr>";
        echo "<td><b>Consumo total</b></td>";
        echo "<td ><b>" . number_format($total, 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td><b>Custo total R$</b></td>";
        echo "<td ><b>" . number_format(($total * $valorMedioKWH), 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "<td><b>Total material recebido</b></td>";
        echo "<td ><b>" . number_format($materialRecebido->MATERIAL, 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td><b>Custo por tonelada recebida </b></td>";
        echo "<td ><b>" . number_format((($total * $valorMedioKWH) / $materialRecebido->MATERIAL), 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "</table>";
    }

    public function custoEnergiaArmazen01() {
        $modelo = new modeloConfiguracoes();
        $dados = $modelo->safraAtual();
        $dataInicioSafraDB = $dados->dataInicioSafra;
        $dataTerminoSafraDB = $dados->dataTerminoSafra;
        $dados->dataInicioSafra = str_replace("-", "", $dados->dataInicioSafra);
        $dados->dataTerminoSafra = str_replace("-", "", $dados->dataTerminoSafra);
        ##INTERNO
        $modelo = new modeloApontamentoConsumoEnergiaLocal();
        $informacoesKHW = new energia();
        $valorMedioKWH = $informacoesKHW->valorMedioKWH();
        $custo = $modelo->consumoEnergiaLocal(10, $dados->dataInicioSafra, $dados->dataTerminoSafra);

        $total = 0;
        header('Cache-Control: no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Content-Type: application/x-msexcel');
        header("Content-Disposition: attachment; filename = Custo energia armazém 01.xls");
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo "<table border='1'>";
        echo "<tr>";
        echo "<td><center><b>Consumo</b></center></td>";
        echo "<td><center><b>Valor R$</b></center></td>";
        //  echo "<td><center><b>Setor</b></center></td>";
        echo "</tr>";
        foreach ($custo as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->consumo . "</td>";
            echo "<td>" . number_format(($dado->consumo * $valorMedioKWH), 2, ",", ".") . "</td>";
            //  echo "<td> Linha " . $idLinha . "</td>";
            echo "</tr>";
            $total = $total + $dado->consumo;
        }
        echo "<tr>";
        echo "<td><b>Consumo total</b></td>";
        echo "<td ><b>" . number_format($total, 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td><b>Custo total R$</b></td>";
        echo "<td ><b>" . number_format(($total * $valorMedioKWH), 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "</table>";
    }

    public function custoEnergiaArmazen02() {
        $modelo = new modeloConfiguracoes();
        $dados = $modelo->safraAtual();
        $dataInicioSafraDB = $dados->dataInicioSafra;
        $dataTerminoSafraDB = $dados->dataTerminoSafra;
        $dados->dataInicioSafra = str_replace("-", "", $dados->dataInicioSafra);
        $dados->dataTerminoSafra = str_replace("-", "", $dados->dataTerminoSafra);
        ##INTERNO
        $modelo = new modeloApontamentoConsumoEnergiaLocal();
        $informacoesKHW = new energia();
        $valorMedioKWH = $informacoesKHW->valorMedioKWH();
        $custo = $modelo->consumoEnergiaLocal(6, $dados->dataInicioSafra, $dados->dataTerminoSafra);

        $total = 0;
        header('Cache-Control: no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Content-Type: application/x-msexcel');
        header("Content-Disposition: attachment; filename = Custo energia armazém 02.xls");
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo "<table border='1'>";
        echo "<tr>";
        echo "<td><center><b>Consumo</b></center></td>";
        echo "<td><center><b>Valor R$</b></center></td>";
        //  echo "<td><center><b>Setor</b></center></td>";
        echo "</tr>";
        foreach ($custo as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->consumo . "</td>";
            echo "<td>" . number_format(($dado->consumo * $valorMedioKWH), 2, ",", ".") . "</td>";
            //  echo "<td> Linha " . $idLinha . "</td>";
            echo "</tr>";
            $total = $total + $dado->consumo;
        }
        echo "<tr>";
        echo "<td><b>Consumo total</b></td>";
        echo "<td ><b>" . number_format($total, 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td><b>Custo total R$</b></td>";
        echo "<td ><b>" . number_format(($total * $valorMedioKWH), 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "</table>";
    }

    public function custoEnergiaArmazen03() {
        $modelo = new modeloConfiguracoes();
        $dados = $modelo->safraAtual();
        $dataInicioSafraDB = $dados->dataInicioSafra;
        $dataTerminoSafraDB = $dados->dataTerminoSafra;
        $dados->dataInicioSafra = str_replace("-", "", $dados->dataInicioSafra);
        $dados->dataTerminoSafra = str_replace("-", "", $dados->dataTerminoSafra);
        ##INTERNO
        $modelo = new modeloApontamentoConsumoEnergiaLocal();
        $informacoesKHW = new energia();
        $valorMedioKWH = $informacoesKHW->valorMedioKWH();
        $custo = $modelo->consumoEnergiaLocal(7, $dados->dataInicioSafra, $dados->dataTerminoSafra);

        $total = 0;
        header('Cache-Control: no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Content-Type: application/x-msexcel');
        header("Content-Disposition: attachment; filename = Custo energia armazém 03.xls");
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo "<table border='1'>";
        echo "<tr>";
        echo "<td><center><b>Consumo</b></center></td>";
        echo "<td><center><b>Valor R$</b></center></td>";
        //  echo "<td><center><b>Setor</b></center></td>";
        echo "</tr>";
        foreach ($custo as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->consumo . "</td>";
            echo "<td>" . number_format(($dado->consumo * $valorMedioKWH), 2, ",", ".") . "</td>";
            //  echo "<td> Linha " . $idLinha . "</td>";
            echo "</tr>";
            $total = $total + $dado->consumo;
        }
        echo "<tr>";
        echo "<td><b>Consumo total</b></td>";
        echo "<td ><b>" . number_format($total, 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td><b>Custo total R$</b></td>";
        echo "<td ><b>" . number_format(($total * $valorMedioKWH), 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "</table>";
    }

    public function custoEnergiaArmazen04() {
        $modelo = new modeloConfiguracoes();
        $dados = $modelo->safraAtual();
        $dataInicioSafraDB = $dados->dataInicioSafra;
        $dataTerminoSafraDB = $dados->dataTerminoSafra;
        $dados->dataInicioSafra = str_replace("-", "", $dados->dataInicioSafra);
        $dados->dataTerminoSafra = str_replace("-", "", $dados->dataTerminoSafra);
        ##INTERNO
        $modelo = new modeloApontamentoConsumoEnergiaLocal();
        $informacoesKHW = new energia();
        $valorMedioKWH = $informacoesKHW->valorMedioKWH();
        $custo = $modelo->consumoEnergiaLocal(8, $dados->dataInicioSafra, $dados->dataTerminoSafra);

        $total = 0;
        header('Cache-Control: no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Content-Type: application/x-msexcel');
        header("Content-Disposition: attachment; filename = Custo energia armazém 04.xls");
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo "<table border='1'>";
        echo "<tr>";
        echo "<td><center><b>Consumo</b></center></td>";
        echo "<td><center><b>Valor R$</b></center></td>";
        //  echo "<td><center><b>Setor</b></center></td>";
        echo "</tr>";
        foreach ($custo as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->consumo . "</td>";
            echo "<td>" . number_format(($dado->consumo * $valorMedioKWH), 2, ",", ".") . "</td>";
            //  echo "<td> Linha " . $idLinha . "</td>";
            echo "</tr>";
            $total = $total + $dado->consumo;
        }
        echo "<tr>";
        echo "<td><b>Consumo total</b></td>";
        echo "<td ><b>" . number_format($total, 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td><b>Custo total R$</b></td>";
        echo "<td ><b>" . number_format(($total * $valorMedioKWH), 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "</table>";
    }

    public function custoEnergiaArmazen05() {
        $modelo = new modeloConfiguracoes();
        $dados = $modelo->safraAtual();
        $dataInicioSafraDB = $dados->dataInicioSafra;
        $dataTerminoSafraDB = $dados->dataTerminoSafra;
        $dados->dataInicioSafra = str_replace("-", "", $dados->dataInicioSafra);
        $dados->dataTerminoSafra = str_replace("-", "", $dados->dataTerminoSafra);
        ##INTERNO
        $modelo = new modeloApontamentoConsumoEnergiaLocal();
        $informacoesKHW = new energia();
        $valorMedioKWH = $informacoesKHW->valorMedioKWH();
        $custo = $modelo->consumoEnergiaLocal(9, $dados->dataInicioSafra, $dados->dataTerminoSafra);

        $total = 0;
        header('Cache-Control: no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Content-Type: application/x-msexcel');
        header("Content-Disposition: attachment; filename = Custo energia armazém 05.xls");
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo "<table border='1'>";
        echo "<tr>";
        echo "<td><center><b>Consumo</b></center></td>";
        echo "<td><center><b>Valor R$</b></center></td>";
        //  echo "<td><center><b>Setor</b></center></td>";
        echo "</tr>";
        foreach ($custo as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->consumo . "</td>";
            echo "<td>" . number_format(($dado->consumo * $valorMedioKWH), 2, ",", ".") . "</td>";
            //  echo "<td> Linha " . $idLinha . "</td>";
            echo "</tr>";
            $total = $total + $dado->consumo;
        }
        echo "<tr>";
        echo "<td><b>Consumo total</b></td>";
        echo "<td ><b>" . number_format($total, 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td><b>Custo total R$</b></td>";
        echo "<td ><b>" . number_format(($total * $valorMedioKWH), 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "</table>";
    }

    public function custoEnergiaTotalArmazens() {
        $modelo = new modeloConfiguracoes();
        $dados = $modelo->safraAtual();
        $dataInicioSafraDB = $dados->dataInicioSafra;
        $dataTerminoSafraDB = $dados->dataTerminoSafra;
        $dados->dataInicioSafra = str_replace("-", "", $dados->dataInicioSafra);
        $dados->dataTerminoSafra = str_replace("-", "", $dados->dataTerminoSafra);
        ##INTERNO
        $modelo = new modeloApontamentoConsumoEnergiaLocal();
        $informacoesKHW = new energia();
        $valorMedioKWH = $informacoesKHW->valorMedioKWH();
        $custo01 = $modelo->consumoEnergiaLocal(10, $dados->dataInicioSafra, $dados->dataTerminoSafra);
        $custo02 = $modelo->consumoEnergiaLocal(6, $dados->dataInicioSafra, $dados->dataTerminoSafra);
        $custo03 = $modelo->consumoEnergiaLocal(7, $dados->dataInicioSafra, $dados->dataTerminoSafra);
        $custo04 = $modelo->consumoEnergiaLocal(8, $dados->dataInicioSafra, $dados->dataTerminoSafra);
        $custo05 = $modelo->consumoEnergiaLocal(9, $dados->dataInicioSafra, $dados->dataTerminoSafra);

        $total = 0;
        header('Cache-Control: no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Content-Type: application/x-msexcel');
        header("Content-Disposition: attachment; filename = Custo total energia armazéns.xls");
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo "<table border='1'>";
        echo "<tr>";
        echo "<td><center><b>Consumo</b></center></td>";
        echo "<td><center><b>Valor R$</b></center></td>";
        echo "<td><center><b>Setor</b></center></td>";
        echo "</tr>";
        foreach ($custo01 as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->consumo . "</td>";
            echo "<td>" . number_format(($dado->consumo * $valorMedioKWH), 2, ",", ".") . "</td>";
            echo "<td> Armazén 01</td>";
            echo "</tr>";
            $total = $total + $dado->consumo;
        }
        foreach ($custo02 as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->consumo . "</td>";
            echo "<td>" . number_format(($dado->consumo * $valorMedioKWH), 2, ",", ".") . "</td>";
            echo "<td> Armazén 02</td>";
            echo "</tr>";
            $total = $total + $dado->consumo;
        }
        foreach ($custo03 as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->consumo . "</td>";
            echo "<td>" . number_format(($dado->consumo * $valorMedioKWH), 2, ",", ".") . "</td>";
            echo "<td> Armazén 03</td>";
            echo "</tr>";
            $total = $total + $dado->consumo;
        }
        foreach ($custo04 as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->consumo . "</td>";
            echo "<td>" . number_format(($dado->consumo * $valorMedioKWH), 2, ",", ".") . "</td>";
            echo "<td> Armazén 04</td>";
            echo "</tr>";
            $total = $total + $dado->consumo;
        }
        foreach ($custo05 as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->consumo . "</td>";
            echo "<td>" . number_format(($dado->consumo * $valorMedioKWH), 2, ",", ".") . "</td>";
            echo "<td> Armazén 05</td>";
            echo "</tr>";
            $total = $total + $dado->consumo;
        }
        echo "<tr>";
        echo "<td><b>Consumo total</b></td>";
        echo "<td colspan='2' ><b>" . number_format($total, 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td><b>Custo total R$</b></td>";
        echo "<td colspan='2' ><b>" . number_format(($total * $valorMedioKWH), 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "</table>";
    }

    public function custoEnergiaTotal() {
        $modelo = new modeloConfiguracoes();
        $dados = $modelo->safraAtual();
        $dataInicioSafraDB = $dados->dataInicioSafra;
        $dataTerminoSafraDB = $dados->dataTerminoSafra;
        $dados->dataInicioSafra = str_replace("-", "", $dados->dataInicioSafra);
        $dados->dataTerminoSafra = str_replace("-", "", $dados->dataTerminoSafra);
        ##INTERNO
        $energia = new energia();
        $valorMedioKWH = $energia->valorMedioKWH();

        $energiaGerada = $energia->energiaGerada();
        $energiaCemig = $energia->energiaCemig();

        $total = 0;
        header('Cache-Control: no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Content-Type: application/x-msexcel');
        header("Content-Disposition: attachment; filename = Custo total energia.xls");
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo "<table border='1'>";
        echo "<tr>";
        echo "<td><center><b>Consumo</b></center></td>";
        echo "<td><center><b>Valor R$</b></center></td>";
        echo "<td><center><b>Setor</b></center></td>";
        echo "</tr>";
        foreach ($energiaGerada as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->consumo . "</td>";
            echo "<td>" . number_format($dado->consumo, 2, ",", ".") . "</td>";
            echo "<td> Energia Gerada</td>";
            echo "</tr>";
            $total = $total + $dado->consumo;
        }
        foreach ($energiaCemig as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->consumo . "</td>";
            echo "<td>" . number_format($dado->consumo, 2, ",", ".") . "</td>";
            echo "<td> Energia CEMIG</td>";
            echo "</tr>";
            $total = $total + $dado->consumo;
        }

        echo "<tr>";
        echo "<td><b>Consumo total</b></td>";
        echo "<td colspan='2' ><b>" . number_format($total, 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td><b>Custo total R$</b></td>";
        echo "<td colspan='2' ><b>" . number_format(($total * $valorMedioKWH), 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "</table>";
    }

    public function custoSecador($secador) {
        $modelo = new modeloConfiguracoes();
        $dados = $modelo->safraAtual();
        $dataInicioSafraDB = $dados->dataInicioSafra;
        $dataTerminoSafraDB = $dados->dataTerminoSafra;
        $dados->dataInicioSafra = str_replace("-", "", $dados->dataInicioSafra);
        $dados->dataTerminoSafra = str_replace("-", "", $dados->dataTerminoSafra);
        ##INTERNO

        $modelo = new modeloApontamentoGLP();
        $dadosConsumo = $modelo->glpConsumoPeriodoTotalSecador($dataInicioSafraDB, $dataTerminoSafraDB, $secador);
        $valorMedioGLP = $this->valorMedioGLP($dataInicioSafraDB, $dataTerminoSafraDB, '111040' . $secador, '111040' . $secador);
        $modelo = new modeloProtheus();

        $materialRecebido = $modelo->totalMatRecPorLinha($dados->dataInicioSafra, $dados->dataTerminoSafra, $secador);
        /* print_r($dadosConsumo);
          print_r('<br>');
          print_r('<br>');
          print_r($valorMedioGLP); */

        $total = 0;
        header('Cache-Control: no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Content-Type: application/x-msexcel');
        header("Content-Disposition: attachment; filename = Secador 0" . $secador . ".xls");
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo "<table border='1'>";
        echo "<tr>";
        echo "<td><center><b>Dia</b></center></td>";
        echo "<td><center><b>Consumo</b></center></td>";
        echo "<td><center><b>Valor R$</b></center></td>";
        // echo "<td><center><b>Setor</b></center></td>";
        echo "</tr>";
        foreach ($dadosConsumo as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->dia . "</td>";
            echo "<td>" . str_replace('.', ',', $dado->consumo) . "</td>";
            echo "<td>" . number_format(($dado->consumo * $valorMedioGLP), 2, ",", ".") . "</td>";
            echo "</tr>";
            $total = $total + $dado->consumo;
        }
        echo "<tr>";
        echo "<td><b>Consumo total</b></td>";
        echo "<td colspan='2' ><b>" . number_format($total, 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td><b>Custo total R$</b></td>";
        echo "<td colspan='2' ><b>" . number_format(($total * $valorMedioGLP), 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "<td><b>Total material recebido (ton) R$</b></td>";
        echo "<td colspan='2' ><b>" . number_format(($materialRecebido->MATERIAL), 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "<td><b>Custo por material R$</b></td>";
        echo "<td colspan='2' ><b>" . number_format((($total * $valorMedioGLP) / $materialRecebido->MATERIAL), 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "</table>";
    }

    public function custoTotalSecador() {
        $modelo = new modeloConfiguracoes();
        $dados = $modelo->safraAtual();
        $dataInicioSafraDB = $dados->dataInicioSafra;
        $dataTerminoSafraDB = $dados->dataTerminoSafra;
        $dados->dataInicioSafra = str_replace("-", "", $dados->dataInicioSafra);
        $dados->dataTerminoSafra = str_replace("-", "", $dados->dataTerminoSafra);
        ##INTERNO

        $modelo = new modeloApontamentoGLP();
        $dadosConsumo = $modelo->glpConsumoPeriodoTotal($dataInicioSafraDB, $dataTerminoSafraDB);
        $valorMedioGLP = $this->valorMedioGLP($dataInicioSafraDB, $dataTerminoSafraDB, '1110401', '1110404');
        $modelo = new modeloProtheus();

        $materialRecebido = $modelo->totalMatRecTotal($dados->dataInicioSafra, $dados->dataTerminoSafra);
        /* print_r($dadosConsumo);
          print_r('<br>');
          print_r('<br>');
          print_r($valorMedioGLP); */

        $total = 0;
        header('Cache-Control: no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Content-Type: application/x-msexcel');
        header("Content-Disposition: attachment; filename = Custo absolutos secadores.xls");
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo "<table border='1'>";
        echo "<tr>";
        echo "<td><center><b>Dia</b></center></td>";
        echo "<td><center><b>Consumo</b></center></td>";
        echo "<td><center><b>Valor R$</b></center></td>";
        // echo "<td><center><b>Setor</b></center></td>";
        echo "</tr>";
        foreach ($dadosConsumo as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->dia . "</td>";
            echo "<td>" . str_replace('.', ',', $dado->consumo) . "</td>";
            echo "<td>" . number_format(($dado->consumo * $valorMedioGLP), 2, ",", ".") . "</td>";
            echo "</tr>";
            $total = $total + $dado->consumo;
        }
        echo "<tr>";
        echo "<td><b>Consumo total</b></td>";
        echo "<td colspan='2' ><b>" . number_format($total, 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td><b>Custo total R$</b></td>";
        echo "<td colspan='2' ><b>" . number_format(($total * $valorMedioGLP), 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "<td><b>Total material recebido (ton) R$</b></td>";
        echo "<td colspan='2' ><b>" . number_format(($materialRecebido->MATERIAL), 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "<td><b>Custo por material R$</b></td>";
        echo "<td colspan='2' ><b>" . number_format((($total * $valorMedioGLP) / $materialRecebido->MATERIAL), 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "</table>";
    }

    public function valorMedioGLP($dataInicial, $dataFinal, $ccInicial, $ccFinal) {

        //função retorna valor gasto com GLP por determinado perído e por determinado secador; 

        $dataIni = str_replace("-", "", $dataInicial);
        $dataFin = str_replace("-", "", $dataFinal);

        $modelo = new modeloProtheus();
        $retorno = $modelo->totalGastoPorProduto($dataIni, $dataFin, '00100142', $ccInicial, $ccFinal);
        $valorGLP = 0;
        if ($retorno->VALORTOTAL) {
            $valorGLP = $retorno->VALORTOTAL / $retorno->QUANTIDADE;
        } else {
            $retorno = $modelo->totalGastoPorProdutoHistorico('00100142', $ccInicial, $ccFinal);
            $valorGLP = $retorno->VALORTOTAL / $retorno->QUANTIDADE;
        }
        $valorGLP = round($valorGLP, 2);
        return $valorGLP;
    }

    public function custoTorreAbsoluto() {
        $modelo = new modeloConfiguracoes();
        $dados = $modelo->safraAtual();
        $dataInicioSafraDB = $dados->dataInicioSafra;
        $dataTerminoSafraDB = $dados->dataTerminoSafra;
        $dados->dataInicioSafra = str_replace("-", "", $dados->dataInicioSafra);
        $dados->dataTerminoSafra = str_replace("-", "", $dados->dataTerminoSafra);
        $modelo = new modeloProtheus();
        $custos = $modelo->itensGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1120201', '1120401');
        $total = 0;
        header('Cache-Control: no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Content-Type: application/x-msexcel');
        header("Content-Disposition: attachment; filename = Custo Torre Absoluto.xls");
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo "<table border='1'>";
        echo "<tr>";
        echo "<td><center><b>Produto</b></center></td>";
        echo "<td><center><b>Valor R$</b></center></td>";
        echo "</tr>";
        foreach ($custos as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->NOME_PRODUTO . "</td>";
            echo "<td>" . number_format($dado->VALORTOTAL, 2, ",", ".") . "</td>";
            echo "</tr>";
            $total = $total + $dado->VALORTOTAL;
        }
        echo "<tr>";
        echo "<td><b>Custo total</b></td>";
        echo "<td><b>" . number_format($total, 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "</table>";
    }

    public function custoTorre() {
        $modelo = new modeloConfiguracoes();
        $dados = $modelo->safraAtual();
        $dataInicioSafraDB = $dados->dataInicioSafra;
        $dataTerminoSafraDB = $dados->dataTerminoSafra;
        $dados->dataInicioSafra = str_replace("-", "", $dados->dataInicioSafra);
        $dados->dataTerminoSafra = str_replace("-", "", $dados->dataTerminoSafra);

        $modelo = new modeloProtheus();
        $custos = $modelo->itensGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1120201', '1120401');

        $modelo = new modeloApontamentoProducao();
        $producao = $modelo->apontamentosEspecificiosLinha(0, $dataInicioSafraDB, $dataTerminoSafraDB);

        $total = 0;
        $totalProduzido = 0;
        header('Cache-Control: no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Content-Type: application/x-msexcel');
        header("Content-Disposition: attachment; filename = Torre tratamento .xls");
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo "<table border='1'>";
        echo "<tr>";
        echo "<td><center><b>Dia</b></center></td>";
        echo "<td><center><b>Produção R$</b></center></td>";
        echo "</tr>";
        foreach ($producao as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->dia . "</td>";
            echo "<td>" . number_format($dado->total, 0, ",", ".") . "</td>";
            echo "</tr>";
            $totalProduzido = $totalProduzido + $dado->total;
        }
        foreach ($custos as $dado) {
            $total = $total + $dado->VALORTOTAL;
        }
        echo "<tr>";
        echo "<td><b>Total produzido</b></td>";
        echo "<td><b>" . number_format($totalProduzido, 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td><b>Custo total</b></td>";
        echo "<td><b>" . number_format($total, 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td><b>Custo TSI por saco</b></td>";
        echo "<td><b>" . number_format(($total / $totalProduzido), 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "</table>";
    }

    public function custoTSIAbsoluto() {
        $modelo = new modeloConfiguracoes();
        $dados = $modelo->safraAtual();
        $dataInicioSafraDB = $dados->dataInicioSafra;
        $dataTerminoSafraDB = $dados->dataTerminoSafra;
        $dados->dataInicioSafra = str_replace("-", "", $dados->dataInicioSafra);
        $dados->dataTerminoSafra = str_replace("-", "", $dados->dataTerminoSafra);
        $modelo = new modeloProtheus();
        $custos = $modelo->itensGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1121201', '1121201');
        $total = 0;
        header('Cache-Control: no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Content-Type: application/x-msexcel');
        header("Content-Disposition: attachment; filename = TSI Absoluto.xls");
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo "<table border='1'>";
        echo "<tr>";
        echo "<td><center><b>Produto</b></center></td>";
        echo "<td><center><b>Valor R$</b></center></td>";
        echo "</tr>";
        foreach ($custos as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->NOME_PRODUTO . "</td>";
            echo "<td>" . number_format($dado->VALORTOTAL, 2, ",", ".") . "</td>";
            echo "</tr>";
            $total = $total + $dado->VALORTOTAL;
        }
        echo "<tr>";
        echo "<td><b>Custo total</b></td>";
        echo "<td><b>" . number_format($total, 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "</table>";
    }

    public function custoTSI() {
        $modelo = new modeloConfiguracoes();
        $dados = $modelo->safraAtual();
        $dataInicioSafraDB = $dados->dataInicioSafra;
        $dataTerminoSafraDB = $dados->dataTerminoSafra;
        $dados->dataInicioSafra = str_replace("-", "", $dados->dataInicioSafra);
        $dados->dataTerminoSafra = str_replace("-", "", $dados->dataTerminoSafra);

        $modelo = new modeloProtheus();
        $custos = $modelo->itensGastoPorCC($dados->dataInicioSafra, $dados->dataTerminoSafra, '1121201', '1121201');

        $modelo = new modeloApontamentoProducao();
        $producao = $modelo->apontamentosEspecificiosLinha(1, $dataInicioSafraDB, $dataTerminoSafraDB);

        $total = 0;
        $totalProduzido = 0;
        header('Cache-Control: no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Content-Type: application/x-msexcel');
        header("Content-Disposition: attachment; filename = TSI .xls");
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo "<table border='1'>";
        echo "<tr>";
        echo "<td><center><b>Dia</b></center></td>";
        echo "<td><center><b>Produção R$</b></center></td>";
        echo "</tr>";
        foreach ($producao as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->dia . "</td>";
            echo "<td>" . number_format($dado->total, 0, ",", ".") . "</td>";
            echo "</tr>";
            $totalProduzido = $totalProduzido + $dado->total;
        }
        foreach ($custos as $dado) {
            $total = $total + $dado->VALORTOTAL;
        }
        echo "<tr>";
        echo "<td><b>Total produzido</b></td>";
        echo "<td><b>" . number_format($totalProduzido, 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td><b>Custo total</b></td>";
        echo "<td><b>" . number_format($total, 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td><b>Custo TSI por saco</b></td>";
        echo "<td><b>" . number_format(($total / $totalProduzido), 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "</table>";
    }

    public function custoEnergiaTorre() {
        $modelo = new modeloConfiguracoes();
        $dados = $modelo->safraAtual();
        $dataInicioSafraDB = $dados->dataInicioSafra;
        $dataTerminoSafraDB = $dados->dataTerminoSafra;
        $dados->dataInicioSafra = str_replace("-", "", $dados->dataInicioSafra);
        $dados->dataTerminoSafra = str_replace("-", "", $dados->dataTerminoSafra);
        ##INTERNO
        $modelo = new modeloApontamentoConsumoEnergiaLocal();
        $informacoesKHW = new energia();
        $valorMedioKWH = $informacoesKHW->valorMedioKWH();
        $consumo = $modelo->consumoEnergiaLocal(5, $dados->dataInicioSafra, $dados->dataTerminoSafra);

        $modelo = new modeloApontamentoProducao();
        $producao = $modelo->apontamentosEspecificiosLinha(0, $dataInicioSafraDB, $dataTerminoSafraDB);
        /*print_r($consumo);
        print_r('<br>');
        print_r('<br>');
        print_r($producao);
        */
        
        $total = 0;
        $totalProduzido = 0;
     
        
        header('Cache-Control: no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Content-Type: application/x-msexcel');
        header("Content-Disposition: attachment; filename = Custo energia torre classificação.xls");
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo "<table border='1'>";
        echo "<tr>";
        echo "<td><center><b>Consumo</b></center></td>";
        echo "<td><center><b>Valor R$</b></center></td>";
        echo "</tr>";
        foreach ($consumo as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->consumo . "</td>";
            echo "<td>" . number_format(($dado->consumo * $valorMedioKWH), 2, ",", ".") . "</td>";
            echo "</tr>";
            $total = $total + $dado->consumo;
        }
        foreach ($producao as $dado) {
          
            $totalProduzido = $totalProduzido + $dado->total;
        }
        echo "<tr>";
        echo "<td><b>Consumo total</b></td>";
        echo "<td ><b>" . number_format($total, 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td><b>Custo total R$</b></td>";
        echo "<td ><b>" . number_format(($total * $valorMedioKWH), 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "<td><b>Material total R$</b></td>";
        echo "<td ><b>" . number_format($totalProduzido, 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "<td><b>Custo energia por sacoR$</b></td>";
        echo "<td ><b>" . number_format(($total * $valorMedioKWH)/$totalProduzido, 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "</table>";
    }
    public function custoEnergiaTSI() {
        $modelo = new modeloConfiguracoes();
        $dados = $modelo->safraAtual();
        $dataInicioSafraDB = $dados->dataInicioSafra;
        $dataTerminoSafraDB = $dados->dataTerminoSafra;
        $dados->dataInicioSafra = str_replace("-", "", $dados->dataInicioSafra);
        $dados->dataTerminoSafra = str_replace("-", "", $dados->dataTerminoSafra);
        ##INTERNO
        $modelo = new modeloApontamentoConsumoEnergiaLocal();
        $informacoesKHW = new energia();
        $valorMedioKWH = $informacoesKHW->valorMedioKWH();
        $consumo = $modelo->consumoEnergiaLocal(11, $dados->dataInicioSafra, $dados->dataTerminoSafra);

        $modelo = new modeloApontamentoProducao();
        $producao = $modelo->apontamentosEspecificiosLinha(1, $dataInicioSafraDB, $dataTerminoSafraDB);
        /*print_r($consumo);
        print_r('<br>');
        print_r('<br>');
        print_r($producao);
        */
        
        $total = 0;
        $totalProduzido = 0;
     
        
        header('Cache-Control: no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Content-Type: application/x-msexcel');
        header("Content-Disposition: attachment; filename = Custo energia TSI.xls");
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo "<table border='1'>";
        echo "<tr>";
        echo "<td><center><b>Consumo</b></center></td>";
        echo "<td><center><b>Valor R$</b></center></td>";
        echo "</tr>";
        foreach ($consumo as $dado) {
            echo "<tr>";
            echo "<td>" . $dado->consumo . "</td>";
            echo "<td>" . number_format(($dado->consumo * $valorMedioKWH), 2, ",", ".") . "</td>";
            echo "</tr>";
            $total = $total + $dado->consumo;
        }
        foreach ($producao as $dado) {
          
            $totalProduzido = $totalProduzido + $dado->total;
        }
        echo "<tr>";
        echo "<td><b>Consumo total</b></td>";
        echo "<td ><b>" . number_format($total, 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td><b>Custo total R$</b></td>";
        echo "<td ><b>" . number_format(($total * $valorMedioKWH), 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "<td><b>Material total R$</b></td>";
        echo "<td ><b>" . number_format($totalProduzido, 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "<td><b>Custo energia por sacoR$</b></td>";
        echo "<td ><b>" . number_format(($total * $valorMedioKWH)/$totalProduzido, 2, ",", ".") . "</b></td>";
        echo "</tr>";
        echo "</table>";
    }

}
