<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace SCM\util;

use SCM\models\modeloConfiguracoes;
use SCM\models\modeloApontamentoDiesel;
use SCM\models\modeloProtheus;
use SCM\models\modeloGeracaoEnergia;
use SCM\models\modeloApontamentoConsumoCemig;

/**
 * Description of energia
 *
 * @author henrique.guedes
 */
class energia {

    function __construct() {
        
    }

    public function valorMedioKWH() {
        $modelo = new modeloConfiguracoes();
        $dadas = $modelo->safraAtual();


        $modelo = new modeloApontamentoDiesel();
        $dados = $modelo->dielselConsumoPeriodoTotal($dadas->dataInicioSafra, $dadas->dataTerminoSafra);

        $dataIni = str_replace("-", "", $dadas->dataInicioSafra);
        $dataFin = str_replace("-", "", $dadas->dataTerminoSafra);

        $modelo = new modeloProtheus();
        $retorno = $modelo->totalGastoPorProduto($dataIni, $dataFin, '00100158', '1119501', '1119501');
        $valorDiesel = 0;
        if ($retorno->VALORTOTAL) {
            $valorDiesel = $retorno->VALORTOTAL / $retorno->QUANTIDADE;
        } else {
            $retorno = $modelo->totalGastoPorProdutoHistorico('00100158', '1119501', '1119501');
            $valorDiesel = $retorno->VALORTOTAL / $retorno->QUANTIDADE;
        }
        $valorDiesel = round($valorDiesel, 2);

        $total = 0;

        foreach ($dados as $dado) {

            $total = $total + $dado->consumo;
        }
        //valor gasto com diesel
        $valorDiesel = $valorDiesel * $total;
        #retorna Valor total do Diesel
        $modelo = new modeloGeracaoEnergia();
        $energiaGerada = $modelo->totalEnergiaGerada($dadas->dataInicioSafra, $dadas->dataTerminoSafra);
        $energiaGerada = $energiaGerada->consumo;
        $valorKWHInterno = $valorDiesel / $energiaGerada;
        $modelo = new modeloApontamentoConsumoCemig();
        $valorKWHCemig = $modelo->valorKWHCemig($dadas->dataInicioSafra, $dadas->dataTerminoSafra);
        $valorKWHCemig = $valorKWHCemig->valorKWHCemig;

        $valorKWHMedio = ($valorKWHCemig + $valorKWHInterno) / 2;
        return($valorKWHMedio);
    }

    public function totalEnergiaCemig() {
        $modelo = new modeloConfiguracoes();
        $dadas = $modelo->safraAtual();
        $modelo = new modeloApontamentoConsumoCemig();
        $consumoCemig = $modelo->consumoTotalCemig($dadas->dataInicioSafra, $dadas->dataTerminoSafra);
        $consumoCemig = $consumoCemig->consumo;


        return ($consumoCemig);
    }
    public function energiaCemig() {
        $modelo = new modeloConfiguracoes();
        $dadas = $modelo->safraAtual();
        $modelo = new modeloApontamentoConsumoCemig();
        $consumoCemig = $modelo->consumoCemig($dadas->dataInicioSafra, $dadas->dataTerminoSafra);


        return ($consumoCemig);
    }

    public function totalEnergiaGerada() {
        $modelo = new modeloConfiguracoes();
        $dadas = $modelo->safraAtual();

        $modelo = new modeloGeracaoEnergia();
        $energiaGerada = $modelo->totalEnergiaGerada($dadas->dataInicioSafra, $dadas->dataTerminoSafra);
        $energiaGerada = $energiaGerada->consumo;

        return($energiaGerada);
    }
    public function energiaGerada() {
        $modelo = new modeloConfiguracoes();
        $dadas = $modelo->safraAtual();

        $modelo = new modeloGeracaoEnergia();
        $energiaGerada = $modelo->energiaGerada($dadas->dataInicioSafra, $dadas->dataTerminoSafra);

        return($energiaGerada);
    }

}
