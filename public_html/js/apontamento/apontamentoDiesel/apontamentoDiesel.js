//função de cadastro


$(document).ready(function () {
    $('#formCadastro').submit(function () {
        var idGerador = $('#gerador').val();
        var dataInicial = $('#dataInicial').val();
        var dataFinal = $('#dataFinal').val();
        var consumoInicial = $('#consumoInicial').val();
        var consumoFinal = $('#consumoFinal').val();
        if (dataFinal < dataInicial) {
            $("#avisoFinalMaiorInicial").show();
            $("#avisoFinalMaiorInicial").removeClass(' alert alert-success');
            $("#avisoFinalMaiorInicial").addClass("alert alert-danger animated fadeInUp").html("<center>Data final não pode ser menor que a data inicial!</center>");
            document.getElementById('dataInicial').style.border = "2px solid #FF6347";
            document.getElementById('dataFinal').style.border = "2px solid #FF6347";
        } else {
            $.ajax({//Função AJAX
                url: "cadastrarApontamentoDiesel",
                type: "post",
                data: {idGerador: idGerador, dataInicial: dataInicial, dataFinal: dataFinal, consumoInicial: consumoInicial, consumoFinal: consumoFinal},
                success: function (result) {
                    if (result == 1) {
                        jQuery.noConflict();
                        $("#cadastro").hide();
                        $("#sucessoCadastro").modal();
                    } else {
                        if (result == 10) {
                            //MOSTRAR PARA O USUÁRIO QUE O NOME JÁ ESTÁ SENDO UTILIZADA 
                            $("#avisoFinalMaiorInicial").show();
                            $("#avisoFinalMaiorInicial").removeClass(' alert alert-success');
                            $("#avisoFinalMaiorInicial").addClass("alert alert-danger animated fadeInUp").html("<center>Consumo inicial não pode ser maior que o consumo final!</center>");
                            document.getElementById('consumoInicial').style.border = "2px solid #FF6347";
                            document.getElementById('consumoFinal').style.border = "2px solid #FF6347";
                        } else {

                            jQuery.noConflict();
                            $("#cadastro").hide();
                            $("#erroDiv").html(result);
                            $("#erro").modal();
                        }

                    }
                },
                error: function () {
                    jQuery.noConflict();
                    $("#cadastro").hide();
                    $("#erro").modal();
                    console.log(data);
                }
            });
        }
        return false;
    });
}
);
//função chamar modal de edição de gerador
function editar() {
    jQuery.noConflict();
    $("#infoApontamento").hide();
    var id = $('#idApontamentoInfo').val();
    //document.getElementById('dataInicialEditar').value = $("#dataInicialInfo").val();
    document.getElementById('dataInicialEditar').value = moment($("#dataInicialInfo").val()).format('YYYY-DD-MM');
    // document.getElementById('dataFinalEditar').value = moment($("#dataFinalInfo").val()).format('YYYY-DD-MM');
    document.getElementById('consumoInicialEditar').value = $("#consumoInicialInfo").val();
    document.getElementById('consumoFinalEditar').value = $("#consumoFinalInfo").val();
    document.getElementById('idApontamentoEditar').value = $("#idApontamentoInfo").val();
    var combo = document.getElementById("geradorEditar");
    combo.selectedIndex = ($("#idGeradorInfo").val() - 1);
    $("#editar").modal();
}

//função de salvar edição
$(document).ready(function () {
    $('#formEditar').submit(function () {
        var idGerador = $('#geradorEditar').val();
        var consumoInicial = $('#consumoInicialEditar').val();
        var consumoFinal = $('#consumoFinalEditar').val();
        var idApontamento = $('#idApontamentoEditar').val();
        var dataFinal = $('#dataFinalEditar').val();
        var dataInicial = $('#dataInicialEditar').val();
        if (dataFinal < dataInicial) {
            $("#avisoFinalMaiorInicialEditar").show();
            $("#avisoFinalMaiorInicialEditar").removeClass(' alert alert-success');
            $("#avisoFinalMaiorInicialEditar").addClass("alert alert-danger animated fadeInUp").html("<center>Data final não pode ser menor que a data inicial!</center>");
            document.getElementById('dataInicialEditar').style.border = "2px solid #FF6347";
            document.getElementById('dataFinalEditar').style.border = "2px solid #FF6347";
        } else {
            $.ajax({//Função AJAX
                url: "editarApontamentoDiesel",
                type: "post",
                data: {idGerador: idGerador, dataInicial: dataInicial, dataFinal: dataFinal, consumoInicial: consumoInicial, consumoFinal: consumoFinal, idApontamento: idApontamento},
                success: function (result) {
                    if (result == 1) {
                        jQuery.noConflict();
                        $("#editar").hide();
                        $("#sucessoEditar").modal();
                    } else {
                        if (result == 10) {
                            //MOSTRAR PARA O USUÁRIO QUE O NOME JÁ ESTÁ SENDO UTILIZADA 
                            $("#avisoFinalMaiorInicialEditar").show();
                            $("#avisoFinalMaiorInicialEditar").removeClass(' alert alert-success');
                            $("#avisoFinalMaiorInicialEditar").addClass("alert alert-danger animated fadeInUp").html("<center>Consumo inicial não pode ser maior que o consumo final!</center>");
                            document.getElementById('consumoInicialEditar').style.border = "2px solid #FF6347";
                            document.getElementById('consumoFinalEditar').style.border = "2px solid #FF6347";
                        }
                        if (result == 20) {
                            $("#editar").hide();
                            $("#erro").modal();
                        }

                    }
                },
                error: function () {
                    $("#editar").hide();
                    $("#erro").modal();
                }
            });
        }
        return false;
    });
});
//função que chama modal de desativar gerador
function desativar() {
    jQuery.noConflict();
    document.getElementById('idDesativar').value = $('#idApontamentoInfo').val();
    $("#infoApontamento").hide();
    $("#desativar").modal();
}

$(document).ready(function () {
    $('#formDesativar').submit(function () {
        var id = $('#idDesativar').val();
        $.ajax({//Função AJAX
            url: "desativarApontamentoDiesel",
            type: "post",
            data: {id: id},
            success: function (result) {
                if (result == 1) {
                    jQuery.noConflict();
                    $("#desativar").hide();
                    $("#sucessoDesativar").modal();
                } else {
                    $("#desativar").hide();
                    $("#erro").modal();
                }
            },
            error: function () {
                $("#desativar").hide();
                $("#erro").modal();
            }
        });
        return false;
    });
});
function atualizarConsumoInicial() {
    var id = $('#gerador').val();
    $.ajax({//Função AJAX
        url: "atualizarConsumoInicialDiesel",
        type: "post",
        data: {id: id},
        success: function (result) {
            if (result != 'erro') {
                if (result == 'null') {
                    document.getElementById('consumoInicial').value = null;
                    document.getElementById('dataInicial').value = null;
                    document.getElementById('dataFinal').value = null;
                } else {
                    var data = JSON.parse(result);
                    document.getElementById('consumoInicial').value = data.valorConsumoFinal;
                    document.getElementById('dataInicial').value = moment(data.dataInicial).format('YYYY-MM-DD');
                    document.getElementById('dataFinal').value = moment(data.dataFinal).format('YYYY-MM-DD');
                }
            } else {
                alert('eroo');
            }
        },
        error: function () {
            alert('vish');
        }
    });
}

/*
 //função que chama modal de ativação de gerador
 function ativar(id) {
 jQuery.noConflict();
 document.getElementById('idAtivar').value = id;
 $("#ativar").modal();
 }
 
 $(document).ready(function () {
 $('#formAtivar').submit(function () {
 var id = $('#idAtivar').val();
 $.ajax({//Função AJAX
 url: "ativarGerador",
 type: "post",
 data: {id: id},
 success: function (result) {
 if (result == 1) {
 jQuery.noConflict();
 $("#ativar").hide();
 $("#sucessoAtivar").modal();
 } else {
 $("#ativar").hide();
 $("#erro").modal();
 
 }
 },
 error: function () {
 $("#ativar").hide();
 $("#erro").modal();
 }
 });
 return false;
 });
 });
 
 
 /*
 
 
 
 
 function editar(descricao, id) {
 jQuery.noConflict();
 document.getElementById('descricaoEditar').value = descricao;
 document.getElementById('id').value = id;
 $("#editar").modal();
 }
 
 
 
 
 
 }*/