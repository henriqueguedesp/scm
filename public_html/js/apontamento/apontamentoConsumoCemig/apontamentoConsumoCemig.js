//função de cadastro
$(document).ready(function () {
    $('#formCadastro').submit(function () {
        var mesReferencia = $('#mesReferencia').val();
        var consumo = $('#consumo').val();
        var valorPago = $('#valorPago').val();
        $.ajax({//Função AJAX
            url: "cadastrarApontamentoConsumoCemig",
            type: "post",
            data: {mesReferencia: mesReferencia, consumo: consumo, valorPago: valorPago},
            success: function (result) {
                if (result == 1) {
                    jQuery.noConflict();
                    $("#cadastro").hide();
                    $("#sucessoCadastro").modal();
                } else {
                    if (result == 11) {
                        //MOSTRAR PARA O USUÁRIO QUE O NOME JÁ ESTÁ SENDO UTILIZADA 
                        $("#aviso").show();
                        $("#aviso").removeClass(' alert alert-success');
                        $("#aviso").addClass("alert alert-danger animated fadeInUp").html("<center>Já existe conta adicionada para o mês " + mesReferencia + "!</center>");
                        document.getElementById('mesReferencia').style.border = "2px solid #FF6347";
                    } else {

                        jQuery.noConflict();
                        $("#cadastro").hide();
                        $("#erroDiv").html(result);
                        $("#erro").modal();

                    }

                }
            },
            error: function () {
                jQuery.noConflict();
                $("#cadastro").hide();
                $("#erro").modal();
            }
        });
        return false;


    });

});


//função chamar modal de edição de gerador
function editar(idApontamento, mesReferencia, consumo, valorPago) {
    jQuery.noConflict();


    document.getElementById('idApontamentoEditar').value = idApontamento;
    document.getElementById('mesReferenciaEditar').value = mesReferencia;
    document.getElementById('consumoEditar').value = consumo;
    document.getElementById('valorPagoEditar').value = valorPago;
    $("#editar").modal();
}

//função de salvar edição
$(document).ready(function () {
    $('#formEditar').submit(function () {
        var mesReferencia = $('#mesReferenciaEditar').val();
        var consumo = $('#consumoEditar').val();
        var valorPago = $('#valorPagoEditar').val();
        var idApontamento = $('#idApontamentoEditar').val();

        $.ajax({//Função AJAX
            url: "editarApontamentoConsumoCemig",
            type: "post",
            data: {mesReferencia: mesReferencia, consumo: consumo, valorPago: valorPago, idApontamento: idApontamento},
            success: function (result) {
                if (result == 1) {
                    jQuery.noConflict();
                    $("#editar").hide();
                    $("#sucessoEditar").modal();
                } else {
                    if (result == 11) {
                        $("#avisoEditar").show();
                        $("#avisoEditar").removeClass(' alert alert-success');
                        $("#avisoEditar").addClass("alert alert-danger animated fadeInUp").html("<center>Já existe conta adicionada para o mês " + mesReferencia + "!</center>");
                        document.getElementById('mesReferenciaEditar').style.border = "2px solid #FF6347";
                    }
                    if (result == 20) {
                        $("#editar").hide();
                        $("#erro").modal();
                    }

                }
            },
            error: function () {
                $("#editar").hide();
                $("#erro").modal();
            }
        });
        return false;
    });
});

//função que chama modal de desativar gerador
function desativar(idApontamento) {
    jQuery.noConflict();
    document.getElementById('idDesativar').value = idApontamento;
    $("#desativar").modal();

}

$(document).ready(function () {
    $('#formDesativar').submit(function () {
        var id = $('#idDesativar').val();
        
        $.ajax({//Função AJAX
            url: "desativarApontamentoConsumoCemig",
            type: "post",
            data: {id: id},
            success: function (result) {
                if (result == 1) {
                    jQuery.noConflict();

                    $("#desativar").hide();
                    $("#sucessoDesativar").modal();
                } else {
                    $("#desativar").hide();
                    $("#erro").modal();

                }
            },
            error: function () {
                $("#desativar").hide();
                $("#erro").modal();
            }
        });
        return false;
    });
});

/*
 //função que chama modal de ativação de gerador
 function ativar(id) {
 jQuery.noConflict();
 document.getElementById('idAtivar').value = id;
 $("#ativar").modal();
 }
 
 $(document).ready(function () {
 $('#formAtivar').submit(function () {
 var id = $('#idAtivar').val();
 $.ajax({//Função AJAX
 url: "ativarGerador",
 type: "post",
 data: {id: id},
 success: function (result) {
 if (result == 1) {
 jQuery.noConflict();
 $("#ativar").hide();
 $("#sucessoAtivar").modal();
 } else {
 $("#ativar").hide();
 $("#erro").modal();
 
 }
 },
 error: function () {
 $("#ativar").hide();
 $("#erro").modal();
 }
 });
 return false;
 });
 });
 
 
 /*
 
 
 
 
 function editar(descricao, id) {
 jQuery.noConflict();
 document.getElementById('descricaoEditar').value = descricao;
 document.getElementById('id').value = id;
 $("#editar").modal();
 }
 
 
 
 
 
 }*/