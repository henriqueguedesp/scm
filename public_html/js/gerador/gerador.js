//função de cadastro
$(document).ready(function () {
    $('#formCadastro').submit(function () {
        var descricao = $('#descricao').val();
        var local = $('#local').val();
        $.ajax({//Função AJAX
            url: "cadastrarGerador",
            type: "post",
            data: {descricao: descricao, local: local},
            success: function (result) {
                if (result == 1) {
                    jQuery.noConflict();
                    $("#cadastro").hide();
                    $("#sucessoCadastro").modal();
                } else {
                    if (result == 10) {
                        //MOSTRAR PARA O USUÁRIO QUE O NOME JÁ ESTÁ SENDO UTILIZADA 
                        $("#avisoCadastro").show();
                        $("#avisoCadastro").removeClass(' alert alert-success');
                        $("#avisoCadastro").addClass("alert alert-danger animated fadeInUp").html("<center>Descrição de categoria já utilizado!</center>");
                        document.getElementById('#descricao').style.border = "2px solid #FF6347";
                    } else {

                        jQuery.noConflict();
                        $("#cadastro").hide();
                        $("#erroDiv").html(result);
                        $("#erro").modal();
                    }

                }
            },
            error: function () {
                jQuery.noConflict();
                $("#cadastro").hide();
                $("#erro").modal();
            }
        });
        return false;
    });
});

//função chamar modal de edição de gerador
function editar(descricao, local, id) {
    jQuery.noConflict();
    document.getElementById('descricaoEdicao').value = descricao;
    document.getElementById('localEdicao').value = local;
    document.getElementById('idEdicao').value = id;
    $("#editar").modal();
}

//função de salvar edição
$(document).ready(function () {
    $('#formEditar').submit(function () {
        var descricao = $('#descricaoEdicao').val();
        var local = $('#localEdicao').val();
        var id = $('#idEdicao').val();
        $.ajax({//Função AJAX
            url: "editarGerador",
            type: "post",
            data: {descricao: descricao, local: local, id: id},
            success: function (result) {
                if (result == 1) {
                    jQuery.noConflict();
                    $("#editar").hide();
                    $("#sucessoEditar").modal();
                } else {
                    if (result == 10) {
                        //MOSTRAR PARA O USUÁRIO QUE O NOME JÁ ESTÁ SENDO UTILIZADA 
                        $("#avisoEditar").show();
                        $("#avisoEditar").removeClass(' alert alert-success');
                        $("#avisoEditar").addClass("alert alert-danger animated fadeInUp").html("<center>Descrição já utilizada!</center>");
                        document.getElementById('descricaoEditar').style.border = "2px solid #FF6347";
                        //MOSTRAR PARA O USUÁRIO QUE  A SIGLA JÁ ESTÁ SENDO UTILIZADA 
                    }
                    if (result == 20) {
                        $("#editar").hide();
                        $("#erro").modal();
                    }

                }
            },
            error: function () {
                $("#editar").hide();
                $("#erro").modal();
            }
        });
        return false;
    });
});

//função que chama modal de desativar gerador
function desativar(id) {
    jQuery.noConflict();
    document.getElementById('idDesativar').value = id;
    $("#desativar").modal();

}

$(document).ready(function () {
    $('#formDesativar').submit(function () {
        var id = $('#idDesativar').val();
        $.ajax({//Função AJAX
            url: "desativarGerador",
            type: "post",
            data: {id: id},
            success: function (result) {
                if (result == 1) {
                    jQuery.noConflict();

                    $("#desativar").hide();
                    $("#sucessoDesativar").modal();
                } else {
                    $("#desativar").hide();
                    $("#erro").modal();

                }
            },
            error: function () {
                $("#desativar").hide();
                $("#erro").modal();
            }
        });
        return false;
    });
});

//função que chama modal de ativação de gerador
function ativar(id) {
    jQuery.noConflict();
    document.getElementById('idAtivar').value = id;
    $("#ativar").modal();
}

$(document).ready(function () {
    $('#formAtivar').submit(function () {
        var id = $('#idAtivar').val();
        $.ajax({//Função AJAX
            url: "ativarGerador",
            type: "post",
            data: {id: id},
            success: function (result) {
                if (result == 1) {
                    jQuery.noConflict();
                    $("#ativar").hide();
                    $("#sucessoAtivar").modal();
                } else {
                    $("#ativar").hide();
                    $("#erro").modal();

                }
            },
            error: function () {
                $("#ativar").hide();
                $("#erro").modal();
            }
        });
        return false;
    });
});


/*
 
 
 
 
 function editar(descricao, id) {
 jQuery.noConflict();
 document.getElementById('descricaoEditar').value = descricao;
 document.getElementById('id').value = id;
 $("#editar").modal();
 }
 
 
 
 
 
 }*/