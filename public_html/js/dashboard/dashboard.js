

$(document).ready(function () {
    $('#formAdicionar').submit(function () {
        var id = $('#titulo').val();
        $.ajax({//Função AJAX
            url: "adicionarDestaque",
            type: "post",
            data: {id : id},
            success: function (result) {
                if (result == 1) {
                    jQuery.noConflict();
                    $("#adicionar").hide();
                    $("#sucesso").modal();
                }else{
                    $("#adicionar").modal().hide();
                    $("#erro").modal();
                }
            },
            error: function () {
                jQuery.noConflict();
                $("#adicionar").hide();
                $("#erro").modal();
            }
        });

        return false;
    });
});


$(document).ready(function () {
    $('#formDesativar').submit(function () {
        var id = $('#idDesativar').val();
        $.ajax({//Função AJAX
            url: "removerDestaque",
            type: "post",
            data: {id: id},
            success: function (result) {
                if (result == 1) {
                    jQuery.noConflict();

                    $("#desativar").hide();
                    $("#sucessoDesativar").modal();
                } else {
                    $("#desativar").hide();
                    $("#erro").modal();

                }
            },
            error: function () {
                $("#desativar").hide();
                $("#erro").modal();
            }
        });
        return false;
    });
});


function desativar(id) {
    jQuery.noConflict();
    document.getElementById('idDesativar').value = id;
    $("#desativar").modal();

}
