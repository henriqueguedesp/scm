$(document).ready(function () {
    $('.login-form').submit(function () {
        var login = $('#email').val();
        var pass = $('#senha').val();
        $.ajax({//Função AJAX
            url: "validaLogin",
            type: "post",
            data: {usuario: login, senha: pass},
            success: function (result) {
                if (result == 1) {
                    location.href = "/sim/public_html/";
                } else {
                    if (result == 14) {
                        $("#output").removeClass(' alert alert-success');
                        $("#output").addClass("alert alert-warning animated fadeInUp").html("Usuário não tem acesso a este módulo!");
                        document.getElementById('email').style.border = "";
                        document.getElementById('senha').style.border = "";
                    }
                    if (result == 13) {
                        $("#output").removeClass(' alert alert-success');
                        $("#output").addClass("alert alert-warning animated fadeInUp").html("Módulo desativado!");
                        document.getElementById('email').style.border = "";
                        document.getElementById('senha').style.border = "";
                    }
                    if (result == 12) {
                        $("#output").removeClass(' alert alert-success');
                        $("#output").addClass("alert alert-warning animated fadeInUp").html("Senha incorreta!");
                        document.getElementById('email').style.border = "";
                        document.getElementById('senha').style.border = "2px solid #FF6347";
                    }
                    if (result == 11) {
                        $("#output").removeClass(' alert alert-success');
                        $("#output").addClass("alert alert-warning animated fadeInUp").html("Usuário desativado!");
                        document.getElementById('email').style.border = "2px solid #FF6347";
                        document.getElementById('senha').style.border = "";
                    }
                    if (result == 10) {
                        $("#output").removeClass(' alert alert-success');
                        $("#output").addClass("alert alert-warning animated fadeInUp").html("Usuário não cadastrado!");
                        document.getElementById('email').style.border = "2px solid #FF6347";
                        document.getElementById('senha').style.border = "2px solid #FF6347";
                    }/*
                    if (result == 0) {

                        //$('#aviso').html('Usuário ou senha errados!');
                        $("#output").removeClass(' alert alert-success');
                        $("#output").addClass("alert alert-danger animated fadeInUp").html("Usuário ou senha está incorreto!");
                        document.getElementById('email').style.border = "2px solid #FF6347";
                        document.getElementById('senha').style.border = "2px solid #FF6347";
                    }*/


                }
            },
            error: function () {
                alert('Erro 664!');
            }
        });
        return false;
    });
});
