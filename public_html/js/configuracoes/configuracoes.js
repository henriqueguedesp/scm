//função de cadastro
$(document).ready(function () {
    $('#form').submit(function () {
        var dataInicioSafra = $('#dataInicioSafra').val();
        var dataTerminoSafra = $('#dataTerminoSafra').val();
        if (dataTerminoSafra < dataInicioSafra) {
            $("#avisoDataSafra").removeClass(' alert alert-success');
            $("#avisoDataSafra").addClass("alert alert-danger animated fadeInUp").html("<center>Data final não pode ser menor que data inicial!</center>");
            document.getElementById('dataInicioSafra').style.border = "2px solid #FF6347";
            document.getElementById('dataTerminoSafra').style.border = "2px solid #FF6347";
        } else {
            $.ajax({//Função AJAX
                url: "atualizarConfiguracoes",
                type: "post",
                data: {dataInicioSafra: dataInicioSafra, dataTerminoSafra: dataTerminoSafra},
                success: function (result) {
                    if (result == 1) {
                        $("#sucesso").modal();
                    } else {
                        jQuery.noConflict();
                        $("#erroDiv").html(result);
                        $("#erro").modal();
                    }
                },
                error: function () {
                    jQuery.noConflict();
                    $("#cadastro").hide();
                    $("#erro").modal();
                }
            });
        }
        return false;
    });


});