$(document).ready(function () {
    $('.login-form').submit(function () {
        var usuario = $('#usuario').val();
        var senha = $('#senha').val();
        var modulo = $('#modulo').val();
        var url = "/"+modulo + "/public_html/validaLogin";
        if (usuario == "" || senha == "") {
            $("#output").removeClass(' alert alert-success');
            $("#output").addClass("alert alert-warning animated fadeInUp").html("Usuário e senha não digitados!");
            document.getElementById('usuario').style.border = "2px solid #f4a82e";
            document.getElementById('senha').style.border = "2px solid #f4a82e";
        } else {
            $.ajax({//Função AJAX
                url: url,
                type: "post",
                data: {usuario: usuario, senha: senha,modulo : modulo},

                success: function (result) {
                    if (result == 1) {
                        location.href = '/'+modulo+"/public_html/";
                    } else if (result == 10) {
                        $("#output").removeClass(' alert alert-success');
                        $("#output").addClass("alert alert-danger animated fadeInUp").html("Usuário inválido!");
                        document.getElementById('usuario').style.border = "2px solid #FF6347";
                        document.getElementById('senha').style.border = "2px solid #FF6347";

                    } else if (result == 11) {
                        $("#output").removeClass(' alert alert-success');
                        $("#output").addClass("alert alert-danger animated fadeInUp").html("Usuário está desativado!");
                        document.getElementById('usuario').style.border = "2px solid #FF6347";
                        document.getElementById('senha').style.border = "";
                    } else if (result == 12) {
                        $("#output").removeClass(' alert alert-success');
                        $("#output").addClass("alert alert-danger animated fadeInUp").html("Senha incorreta!");
                        document.getElementById('usuario').style.border = "";
                        document.getElementById('senha').style.border = "2px solid #FF6347";
                    } else if (result == 13) {
                        $("#output").removeClass(' alert alert-success');
                        $("#output").addClass("alert alert-danger animated fadeInUp").html("Módulo desativado. Não é possível fazer login!");
                        document.getElementById('usuario').style.border = "";
                        document.getElementById('senha').style.border = "";
                    } else if (result == 14) {
                        $("#output").removeClass(' alert alert-success');
                        $("#output").addClass("alert alert-danger animated fadeInUp").html("Usuário não tem permissão de acesso nesse módulo!");
                        document.getElementById('usuario').style.border = "2px solid #FF6347";
                        document.getElementById('senha').style.border = "";
                    }
                },
                error: function () {
                    alert('Erro 664!');
                }
            });
        }

        return false;
    });
});
